
<div class="subscribe_block" style="<?=$subscribe_block_styles?> ">
    <p><?php echo $sub_title_text;?></p>
    <form  method="POST" onsubmit="sendform(event); "  id="subscribe_form" class="form-inline">
        <div class="input-append">
            <div class="form-group">

                <input type="email" name="email" style="<?=$input_styles?>"  class="form-control " placeholder="<?php echo $sub_placeholder_text;?>">
                <input type="hidden"  name="latter_text"  class="form-control " value="<?php echo $latter_text?>">
                <button class="btn" style="<?=$btn_styles?>"   type="submit" ><?php echo $sub_button_text;?></button>
            </div>
        </div>
    </form>
    <div class="modal fade" id="subscribeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="<?=$modal_styles?>">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo $sub_thankth_title;?></h4>
                </div>
                <div class="modal-body">
                    <div id="subscribe_result2" style="<?=$subscribe_block_styles?>" class="subscribe_result_close2 text-center"><?php echo $sub_thankth_text;?>
                        <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">



    function sendform(event) {
        event.preventDefault();
        var msg;
        msg=$('#subscribe_form').serialize();




        if ($('#subscribe_form input').val()!='') {
            $.ajax({
                type: 'POST',
                url: '<?php echo $action;?>',
                data: msg,
                success: function (data) {
                    $('#subscribeModal').modal('show');
                    $('div #subscribe_result').addClass('subscribe_result_open');

                },
                error: function (xhr, str) {
                    alert('Error: ' + xhr.responseCode);
                }
            });

        }
        else
        {
            alert('Type in your email!');
        }
    }
    $('div #subscribe_result .close').click(function() {
        $('div #subscribe_result').removeClass('subscribe_result_open');
    });
</script> <style type="text/css">
    <?php echo $styles;?>
</style>