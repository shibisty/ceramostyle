<div class="modal_form">
        <div class="mfi-container mfp-with-anim" id="enterLink">
          <div class="mfi-container__inner">
            <div class="mfp-close">x</div>
            <div class="mfp-container__cover enter-reg-container">
              <div class="po-cover">
                <div class="mfp--title">Вход на сайт</div>
                <form class="bForm" action="?" role="form" data-focus="false" data-toggle="validator">
                  <div class="form-group">
                    <label><span>E-mail</span></label>
                    <input class="form-control" type="email" data-minlength="2" required data-error="Некорректный email" name="umail">
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <label><span>Пароль</span></label>
                    <input class="form-control" type="password" required data-error="Некорректный пароль (не менее 6 символов)" data-minlength="6" name="upaswd">
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="sub-enter-link">
                    <input class="btn-submit" type="submit" value="Войти">
                  </div>
                  <div class="form-etc"><a class="forg_pswd" href="#">Забыли свой пароль?</a><a class="forg_reg" href="#">Регистрация</a></div>
                </form>
              </div>
              <div class="forg-cover modal_form">
                <div class="mfp--title">Восстановление пароля</div>
                <p>Если вы забыли пароль, введите E-Mail. Контрольная строка для смены пароля, а также ваши регистрационные данные, будут высланы вам по E-Mail.</p>
                <form class="bForm" action="?" role="form" data-focus="false" data-toggle="validator">
                  <div class="form-group">
                    <label><span>E-mail*</span></label>
                    <input class="form-control restorePswd" type="email" data-minlength="2" required data-error="Некорректный email" name="umail2">
                    <div class="help-block with-errors"></div>
                  </div>
                  <input class="btn-submit" type="submit" value="выслать">
                  <div class="back__link_pswd">назад</div>
                </form>
              </div>
              <div class="reg-cover reg_styled_cover modal_form">
                <div class="mfp--title">Регистрация</div>
                <form class="bForm" action="?" role="form" data-focus="false" data-toggle="validator">
                  <div class="form-group">
                    <label><span>Ваше имя *</span></label>
                    <input class="form-control" type="text" data-minlength="2" required data-error="Не менее 2 символов" name="uname">
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <label><span>Телефон *</span></label>
                    <input class="form-control" type="tel" data-minlength="2" required data-error="Некорректный телефон" name="utel">
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <label><span>E-mail *</span></label>
                    <input class="form-control" type="email" data-minlength="2" required data-error="Некорректный email" name="umail">
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <label> <span class="form-group__helper">Пароль * </span>
                      <input class="form-control form-control" id="inputPassword" required name="u_pswd" type="password" data-minlength="6" data-error="Не менее 6 символов" data-match="#inputPasswordConfirm" data-match-error="Пароли не совпадают" placeholder="Пароль">
                    </label>
                    <div class="help-block with-errors"></div><br>
                    <label> <span class="form-group__helper">Подтверждение пароля: **</span>
                      <input class="form-control form-control" id="inputPasswordConfirm" required name="u_pswd2" type="password" data-minlength="6" data-error="Не менее 6 символов" data-match="#inputPassword" data-match-error="Пароли не совпадают" placeholder="Подтверждение">
                    </label>
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <div class="fg-checkbox">
                      <input class="form-control" type="checkbox" id="inp_checkbox_p" checked>
                      <label for="inp_checkbox_p"></label>
                    </div>
                    <div class="fg-checkbox--text">Для продолжения необходимо подтвердить своё согласие на обработку Ваших персональных данных, установив флажок у соответствующего пункта</div>
                  </div>
                  <div class="form-after--text">
                    <p>* - Поля, обязательные для заполнения</p>
                    <p>** - Пароль должен быть не менее 6 символов длиной.</p>
                  </div>
                  <input class="btn-submit" type="submit" value="Регистрация">
                </form>
                <div class="back__link_reg">назад</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal_form">
        <div class="mfi-container mfp-with-anim" id="regLink">
          <div class="mfi-container__inner reg_form_pp">
            <div class="mfp-close">x</div>
            <div class="mfp-container__cover reg-container__cover">
              <div class="mfp--title"> Регистрация</div>
              <form class="bForm" action="?" role="form" method="GET" data-focus="false" data-toggle="validator">
                <div class="form-group">
                  <label><span>Ваше имя *</span></label>
                  <input type="text" data-minlength="2" required data-error="Не менее 2 символов" name="uname">
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <label><span>Телефон *</span></label>
                  <input type="tel" data-minlength="2" required data-error="Некорректный телефон" name="utel">
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <label><span>E-mail *</span></label>
                  <input type="email" data-minlength="2" required data-error="Некорректный email" name="umail">
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <label> <span class="form-group__helper">Пароль * </span>
                    <input class="form-control" id="inputPassword" required name="u_pswd" type="password" data-minlength="6" data-error="Не менее 6 символов" data-match="#inputPasswordConfirm" data-match-error="Пароли не совпадают">
                  </label>
                  <div class="help-block with-errors"></div><br>
                  <label> <span class="form-group__helper">Подтверждение пароля: **</span>
                    <input class="form-control" id="inputPasswordConfirm" required name="u_pswd2" type="password" data-minlength="6" data-error="Не менее 6 символов" data-match="#inputPassword" data-match-error="Пароли не совпадают">
                  </label>
                  <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                  <div class="fg-checkbox">
                    <input class="form-control" type="checkbox" id="inp_checkbox_p" checked>
                    <label for="inp_checkbox_p"></label>
                  </div>
                  <div class="fg-checkbox--text">Для продолжения необходимо подтвердить своё согласие на обработку Ваших персональных данных, установив флажок у соответствующего пункта</div>
                </div>
                <div class="form-after--text">
                  <p>* - Поля, обязательные для заполнения</p>
                  <p>** - Пароль должен быть не менее 6 символов длиной.</p>
                </div>
                <input type="submit" value="Регистрация">
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="modal_form">
        <div class="mfi-container mfp-with-anim" id="restorePswdBox">
          <div class="mfi-container__inner">
            <div class="mfp-close">x</div>
            <div class="mfp-container__cover call_back_form restore_pswwd_pp_form">
              <div class="mfp--title">Восстановление пароля</div>
              <form class="bForm" action="?" data-toggle="validator" role="form" data-focus="false">
                <div class="form-group">
                  <label> <span class="form-group__helper">Новый пароль: *</span>
                    <input class="form-control" id="inputPassword" required name="u_pswd" type="password" data-minlength="6" data-error="Не менее 6 символов" data-match="#inputPasswordConfirm" data-match-error="Пароли не совпадают">
                  </label>
                  <div class="help-block with-errors"></div><br>
                  <label> <span class="form-group__helper">Подтверждение пароля: **</span>
                    <input class="form-control" id="inputPasswordConfirm" required name="u_pswd2" type="password" data-minlength="6" data-error="Не менее 6 символов" data-match="#inputPassword" data-match-error="Пароли не совпадают">
                  </label>
                  <div class="help-block with-errors"></div>
                </div>
                <input type="submit" value="Сохранить">
              </form>
            </div>
          </div>
        </div>
      </div>
