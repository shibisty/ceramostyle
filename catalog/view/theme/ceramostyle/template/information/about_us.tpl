<?php echo $header; ?>
<div class="liders-b sliders-block">
	<div class="container">
		<div class="row">
			<div class="site_size">
				<ul class="breadcrumbs">
					<?php foreach ($breadcrumbs as $breadcrumb): ?>
    					  <li>
    						  <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
    							  <?php echo $breadcrumb['text']; ?>
    						  </a>
    					  </li>
    				  <?php endforeach; ?>
				</ul>
				<h1 class="mainH1">О компании</h1>
				<div class="about-c-cover"><img class="about-c-img" src="/catalog/view/theme/ceramostyle/img/we.jpg">
					<div class="about-c-text">все о<br> <span> ceramostyle</span></div>
				</div>
				<div class="oText">
				  <p style="text-align: justify;">ООО «ГК "КерамоСтиль» – динамично развивающаяся компания,
					  за время своей работы успела зарекомендовать себя надежным,
					  стабильным поставщиком и партнером. Основная сфера деятельности -
					  продажа строительных и отделочных материалов: керамогранита, керамической плитки,
					  строительных смесей, затирки, вентилируемого фасада, металлокассет.
					  Сотрудничаем с крупнейшими производителями Италии, Испании, России и Китая.</p>
				  <br>
				  <h2 style="text-transform:uppercase; font-size: 22px; margin-bottom: 46px; text-align:center;margin-top: 36px;">
					Главная задача компании – предоставить материал<br>высочайшего качества,
					в необходимые сроки и по оптимальной цене.</h2>
				  <p>Безупречное качество выпускаемой продукции,
					  высокая прочность и гарантия производителя позволяют нашим клиентам экономить
					  значительные средства и время, а также реализовывать за счет широкого ассортимента материала,
					  самые смелые и изысканные проекты архитекторов и дизайнеров.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
