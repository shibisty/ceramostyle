<?php echo $header; ?>
<div class="liders-b sliders-block">
  <div class="container">
    <div class="row">
      <div class="site_size">
        <ul class="breadcrumbs">
            <?php foreach ($breadcrumbs as $breadcrumb): ?>
				  <li>
					  <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
						  <?php echo $breadcrumb['text']; ?>
					  </a>
				  </li>
			  <?php endforeach; ?>
        </ul>
        <h1 class="mainH1">Подъем на этаж</h1>
        <div class="oText">
          <p style="text-align:justify">Сеть салонов керамической плитки и сантехники «CeramoStyle» освободит вас от забот по самостоятельной доставке товаров, приобретенных в наших салонах и интернет-магазине. Предоставьте это профессионалам и получайте удовольствие от результата. Доставка заказов осуществляется в максимально сжатые сроки в удобное для вас время. Мы приближаем радость от окончания ремонта, делая покупку любых отделочных материалов простой и комфортной!</p>
          <br>
          <br>
          <h2 class="h2T" style="margin-top:3px">Дополнительные сервисы:</h2>
          <br>
          <p><b>«Подъем/занос товара на этаж»</b></p>
          <p><b>«Разгрузка и пронос товара вручную»</b></p>
          <br>
          <br>
          <p style="margin-top:-11px;"><span style="text-transform: uppercase;">ПОДЪЕМ/ЗАНОС ТОВАРА В ПОМЕЩЕНИЕ НА ЭТАЖ:</span> Если у вас в доме нет лифта и требуется подъем товара. Формула расчета - 1,4 руб. х номер категории х количество этажей = стоимость подъема товара в доме с лифтом. Пример расчета:</p>
          <table class="table-teh">
            <tr>
              <th style="width: 260px">ФИКСИРОВАННАЯ СТОИМОСТЬ ЗА 1КГ ПОДЪЕМА</th>
              <th style="width: 242px; text-align:center;">ВЕС ГРУЗА</th>
              <th style="width: 290px; text-align:center;">КОЛИЧЕСТВО ЭТАЖЕЙ</th>
              <th style="width: 289px; text-align:right;">СТОИМОСТЬ УСЛУГИ ДЛЯ КЛИЕНТА</th>
            </tr>
            <tr>
              <td><span style="position:relative; top: 20px;">1,4 руб.</td>
              <td style="width:242px;">
                <p style="margin-left:75px;white-space:nowrap;">1 категория — до 150 кг</p>
                <p style="margin-left:75px;white-space:nowrap;">2 категория — от 151 до 250 кг</p>
                <p style="margin-left:75px;white-space:nowrap;">3 категория — от 251 до 350 кг</p>
                <p style="margin-left:75px;white-space:nowrap;">4 категория — от 351 до 450 кг</p>
              </td>
              <td><span style="position:relative; top: 20px;">5</td>
              <td>
                <p>1050 руб.</p>
                <p>1050 руб.</p>
                <p>1050 руб.г</p>
                <p>1050 руб.</p>
              </td>
            </tr>
          </table>
          <p>Если у вас в доме есть лифт и требуется подъем товара. Формула расчета - 2,5 руб./кг х вес заказа = стоимость подъема товара в доме с лифтом. Пример расчета:</p>
          <br>
          <br>
          <p><span style="text-transform: uppercase;">РАЗГРУЗКА И ПРОНОС ТОВАРА ВРУЧНУЮ:</span> Донести товар с места разгрузки до места хранения по вашей просьбе возможно по дополнительному тарифу. Формула расчета: 7 руб. (1 метр переноса товара) х номер категории х расстояние = стоимость услуги. Пример расчета:</p>
          <table class="table-teh">
            <tr>
              <th style="width: 260px">ФИКСИРОВАННАЯ СТОИМОСТЬ ЗА 1КГ ПОДЪЕМА</th>
              <th style="width: 242px; text-align:center;">ВЕС ГРУЗА</th>
              <th style="width: 290px; text-align:center;">КОЛИЧЕСТВО ЭТАЖЕЙ</th>
              <th style="width: 289px; text-align:right;">СТОИМОСТЬ УСЛУГИ ДЛЯ КЛИЕНТА</th>
            </tr>
            <tr>
              <td><span style="position:relative; top: 20px;">1,4 руб.</td>
              <td style="width:242px;">
                <p style="margin-left:75px;white-space:nowrap;">1 категория — до 150 кг</p>
                <p style="margin-left:75px;white-space:nowrap;">2 категория — от 151 до 250 кг</p>
                <p style="margin-left:75px;white-space:nowrap;">3 категория — от 251 до 350 кг</p>
                <p style="margin-left:75px;white-space:nowrap;">4 категория — от 351 до 450 кг</p>
              </td>
              <td><span style="position:relative; top: 20px;">5</td>
              <td>
                <p>1050 руб.</p>
                <p>1050 руб.</p>
                <p>1050 руб.г</p>
                <p>1050 руб.</p>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
