<?php echo $header; ?>
<div class="liders-b sliders-block">
          <div class="container">
            <div class="row">
              <div class="site_size">
                <ul class="breadcrumbs">
                    <?php foreach ($breadcrumbs as $breadcrumb): ?>
    					  <li>
    						  <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
    							  <?php echo $breadcrumb['text']; ?>
    						  </a>
    					  </li>
    				  <?php endforeach; ?>
                </ul>
                <h1 class="mainH1">Оптовикам</h1>
                <div class="optovik-img">
                  <div class="opt-text-1 ab--title-right">
                    <span class="banner_second">выгодные условия</span>
                    <span class="banner_second2">совместной работы </span>
                    <span class="banner_second3">для оптовиков и строителей</span></div>
                    <img src="/catalog/view/theme/ceramostyle/img/optoviki.jpg">
                </div>
                <div class="arc-adv-block arc-optovik-block">
                  <h3 class="h3T">ПОЧЕМУ МЫ?</h3>
                  <div class="adv-cover-container">
                    <div class="adv-item">
                      <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/ww/vv1.png"></div>
                      <div class="adv-cover-part">
                        <div class="adv-title">СОБСТВЕННЫЙ СКЛАД КОМПАНИИ В МОСКВЕ</div>
                        <div class="adv-text">В наличии на собственном складе компании всегда представлено более 1.200.000 кв. м. керамической плитки и гранита.</div>
                      </div>
                    </div>
                    <div class="adv-item">
                      <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/ww/vv2.png"></div>
                      <div class="adv-cover-part">
                        <div class="adv-title">ИНДИВИДУАЛЬНЫЙ ПОДХОД</div>
                        <div class="adv-text">Уникальные условия сотрудничества для оптовиков и строителей. Наличие услуги «Персональный менеджер» позволяет получить высокий уровень обслуживания с учетом всех</div>
                      </div>
                    </div>
                    <div class="adv-item">
                      <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/ww/vv3.png"></div>
                      <div class="adv-cover-part">
                        <div class="adv-title">ОПЕРАТИВНОСТЬ</div>
                        <div class="adv-text">Доставка по Москве и МО в течение одного дня. Отгрузка со склада осуществляется в течение часа с момента поступления заявки.</div>
                      </div>
                    </div>
                    <div class="adv-item">
                      <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/ww/vv4.png"></div>
                      <div class="adv-cover-part">
                        <div class="adv-title">ОГРОМНЫЙ ВЫБОР</div>
                        <div class="adv-text">Широкий ассортимент товаров постоянно пополняется новинками ведущих российских и зарубежных производителей.</div>
                      </div>
                    </div>
                    <div class="adv-item">
                      <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/ww/vv5.png"></div>
                      <div class="adv-cover-part">
                        <div class="adv-title">КАЧЕСТВО</div>
                        <div class="adv-text">В ассортименте – только товары первого сорта, высочайшего качества, имеющие все необходимые сертификаты.</div>
                      </div>
                    </div>
                    <div class="adv-item">
                      <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/ww/vv6.png"></div>
                      <div class="adv-cover-part">
                        <div class="adv-title">ГАРАНТИЯ И ЛУЧШИЕ ЦЕНЫ</div>
                        <div class="adv-text">Компания предлагает на весь ассортимент цены заводов-изготовителей, выгодные скидки на оптовые заказы.</div>
                      </div>
                    </div>
                    <div class="adv-item">
                      <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/ww/vv7.png"></div>
                      <div class="adv-cover-part">
                        <div class="adv-title">МАРКЕТИНГОВАЯ ПОДДЕРЖКА</div>
                        <div class="adv-text">Нашим партнерам и клиентам мы обеспечиваем маркетинговую и рекламную поддержку.</div>
                      </div>
                    </div>
                    <div class="adv-item">
                      <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/ww/vv8.png"></div>
                      <div class="adv-cover-part">
                        <div class="adv-title">РАБОТАЕМ С 2001 ГОДА</div>
                        <div class="adv-text">Компания «Ceramostyle» основана командой профессионалов, имеющих многолетний опыт работы в сфере строительства.</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="optovik-img">
                  <div class="opt-text-1 ab--title-right">услуга<span>персональный менеджер</span></div><img src="/catalog/view/theme/ceramostyle/img/manager-img.jpg">
                </div>
                <div class="arc-cover-text">
                  <div class="h3T">МЫ ХОТИМ ВИДЕТЬ ВАС В ЧИСЛЕ НАШИХ КЛИЕНТОВ И ПАРТНЕРОВ</div><span class="join_btn mfi" data-url="#callBack">Заказать звонок</span>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $footer; ?>
