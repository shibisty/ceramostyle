<?php echo $header; ?>
<div class="liders-b sliders-block">
  <div class="container">
    <div class="row">
      <div class="site_size">
        <ul class="breadcrumbs">
            <?php foreach ($breadcrumbs as $breadcrumb): ?>
                <li>
                    <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
                        <?php echo $breadcrumb['text']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <h1 class="mainH1">ГАРАНТИИ ВОЗВРАТА В КОМПАНИИ «ceramostyle»</h1>
        <div class="voz-container">
          <div class="voz-top-container">
            <div class="container">
              <div class="gar-row">
                <div class="gar-item">
                  <div class="voz-block vt-1"><img class="voz-icon" src="/catalog/view/theme/ceramostyle/img/vt/v1.png">
                    <div class="voz-text">100% прозрачная<br> схема возврата<br> денежных средств</div>
                  </div>
                </div>
                <div class="gar-item">
                  <div class="voz-block vt-2"><img class="voz-icon" src="/catalog/view/theme/ceramostyle/img/vt/v2.png">
                    <div class="voz-text">
                      <p>Оперативное решение любых вопросов, связанных с возвратом и обменом товара.</p><br>
                      <p style="margin-top: 16px">Отдел качества и сервиса работает ежедневно с 9:00 до 18:00 часов.</p><br>
                      <p style="margin-top: 28px;">тел.: +7 (495) 223-34-96</p>
                      <p>e-mail: sale@ceramostyle.ru</p>
                    </div>
                  </div>
                </div>
                <div class="gar-item">
                  <div class="voz-block vt-3"><img class="voz-icon" src="/catalog/view/theme/ceramostyle/img/vt/v3.png">
                    <div class="voz-text">
                      <p style="margin-bottom: 24px;">Возврат/обмен товара осуществляется на складе компании строго по адресу:</p><br>
                      <p>г. Москва, 1-ый Котляковский переулок, д. 1, стр. 34.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="oText vtOT2">
            <h2>ИНФОРМАЦИЯ ДЛЯ ПОКУПАТЕЛЕЙ</h2>
            <p>Плитка, керамогранит, мозаика в разных партиях отличается по тону и калибру, это обусловлено свойствами материала и технологией производства и не является недостатком товара. Плитка, изготовленная индустриальным способом, имеет допуски, технические характеристики, нормативная величина которых, указывается в каталогах и иных информационных документах. Дополнительная информация о товаре предоставляется по требованию покупателя.</p>
            <br>
            <br>
            <p>Расчет необходимого количества плитки, производимого консультантом-дизайнером, носит рекомендательный характер. Принятие решения по заказу остается за покупателем. Консультант-дизайнер не несёт ответственности за расчет необходимого количества плитки и размеры помещений, предоставленные покупателем.</p>
          </div>
          <h2 class="h2T h2TOT">Обмен товара</h2>
          <br>
          <br>
          <div class="container">
            <div class="gar-row gar-row-2">
              <div class="gar-item">
                <div class="voz-block vt-1"><img class="voz-icon" src="/catalog/view/theme/ceramostyle/img/vt/v4.png">
                  <div class="voz-text">Комплектность и соответствие заказу проверяется в момент передачи товара от продавца к покупателю.</div>
                </div>
              </div>
              <div class="gar-item">
                <div class="voz-block vt-2"><img class="voz-icon" src="/catalog/view/theme/ceramostyle/img/vt/v5.png">
                  <div class="voz-text">
                    <p>При обнаружении боя, сколов на<br> плитке/декоративных элементах или несоответствия поступившего товара необходимо сообщить об этом менеджерам <br>Отдела качества и сервиса:<br> тел.: +7 (495) 223-34-96 e-mail:<br> sale@ceramostyle.ru или внести<br> соответствующую запись в сопроводительные документы (при доставке).</p>
                  </div>
                </div>
              </div>
              <div class="gar-item">
                <div class="voz-block vt-3"><img class="voz-icon" src="/catalog/view/theme/ceramostyle/img/vt/v6.png">
                  <div class="voz-text">
                    <p>Предоставить менеджеру отдела качества фотографии битой<br> плитки/декоративных элементов и ярлыка с упаковки в течение трех дней с момента получения товара на e-mail: sale@ceramostyle.ru<br> с указанием номера счета/заказа, количества битых<br> плиток/декоративных элементов.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="come-b-block">
              <h2 class="h2T h2TOT">УСЛОВИЯ ВОЗВРАТА ТОВАРА НАДЛЕЖАЩЕГО КАЧЕСТВА</h2>
              <ul class="cbb-list">
                <li>
                  <div class="cbb-text">Вы имеете право в течение 60 дней с момента передачи товара вернуть купленную у нас плитку, и в течение 14 дней с момента передачи товара вы можете вернуть купленную у нас сантехнику, напольные покрытия и сопутствующие товары. Товар, приобретенный из уцененного ассортимента или в дисконт-магазине возврату не подлежит.</div></li>
                  <li>
                    <div class="cbb-text">Возврат товара производится согласно кратности покупки (пример: при приобретении кратно упаковкам, товар возвращается упаковками).</div></li>

                    <li>
                      <div class="cbb-text">Все претензии по товару принимаются до момента его укладки.
                      Возврат денежных средств осуществляется в кассе любого салона сети «CeramoStyle» при предъявлении чека и документа, удостоверяющего личность. В случае утери чека необходимо обращаться в кассу салона, где был совершен платеж. Денежные средства покупатель может получить во время работы салона (понедельник – суббота: 9:00–21:00, воскресенье: 9:00–19:00), без ограничений по срокам</div></li>
                        <li>
                          <div class="cbb-text">Возврат денежных средств, оплаченных с помощью банковской карты, производится только на карту, с которой была произведена.</div></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php echo $footer; ?>
