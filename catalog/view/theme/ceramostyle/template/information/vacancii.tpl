<?php echo $header; ?>
<div class="liders-b sliders-block">
          <div class="container">
            <div class="row">
              <div class="site_size">
                <ul class="breadcrumbs">
                    <?php foreach ($breadcrumbs as $breadcrumb): ?>
    					  <li>
    						  <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
    							  <?php echo $breadcrumb['text']; ?>
    						  </a>
    					  </li>
    				  <?php endforeach; ?>
                </ul>
                <h1 class="mainH1">Вакансии</h1>
                <div class="vac-img"><img src="/catalog/view/theme/ceramostyle/img/vac-br.jpg">
                  <div class="vac-text">Вакансии</div>
                </div>
                <div class="oText">
                  <p>Наша компания проводит конкурсный отбор кандидатов на следующие вакансии:</p>
                </div>
                <div class="vac-cover">
                  <div class="vac-item">
                    <div class="oText"><br>
                      <h1>Менеджер по продажам</h1>
                      <p>График работы: Полный день</p>
                      <p>Требуемый опыт работы: Более 3-х лет</p>
                      <p style="margin-top: 14px;">Требования:</p><br>

                      <p style="line-height: 1em">Опыт в сфере продаж B2B.</p>
                      <p style="line-height: 1em">Опыт работы в сфере продаж строительных и отделочных материалов приветствуется.</p>
                      <p style="line-height: 1em">Опыт продаж керамогранита приветствуется.</p>
                      <p style="line-height: 1em">Наличие личного авто желательно.</p>
                      <p style="line-height: 1em">Высшее образование.</p>
                      <p style="line-height: 1em">Коммуникабельность, грамотная речь.</p>
                      <p style="line-height: 1em">Обязанности:</p><br>

                      <p style="line-height: 1.2em">Поиск и привлечение клиентов (строительные, строительно-монтажные компании, застройщики, архитекторы, дизайнеры).</p>
                      <p style="line-height: 1em">Проведение презентаций для клиентов.</p>
                      <p style="line-height: 1em">Проведение переговоров и подготовка коммерческих предложений.</p>
                      <p style="line-height: 1em">Выезд к клиентам.</p>
                      <p style="line-height: 1em">Работа с существующей базой.</p>
                      <p style="line-height: 1em">Условия:</p><br>

                      <p style="line-height: 1.2em">Мы предлагаем интересную работу для активных и амбициозных людей в лидирующей Компании.</p>
                      <p style="line-height: 1em">Профессиональный рост в продажах.</p>
                      <p style="line-height: 1.2em">Возможность зарабатывать от 80 000 руб. и выше (фиксированный оклад + бонусная система).</p>
                      <p style="line-height: 1em">Оформление по ТК.</p>
                      <p style="line-height: 1em">График работы: понедельник-четверг с 9.00 — 18.00, пятница с 9.00 до 17.00.</p>
                      <p style="line-height: 1em">Офис - 15 минут пешком от метро Каширская.</p>
                    </div>
                  </div>
                  <div class="vac-item">
                    <div class="oText"><br>
                      <h1>Менеджер по продажам</h1>
                      <p>График работы: Полный день</p>
                      <p>Требуемый опыт работы: Более 3-х лет</p>
                      <p style="margin-top: 14px;">Требования:</p><br>

                      <p style="line-height: 1em">Опыт в сфере продаж B2B.</p>
                      <p style="line-height: 1em">Опыт работы в сфере продаж строительных и отделочных материалов приветствуется.</p>
                      <p style="line-height: 1em">Опыт продаж керамогранита приветствуется.</p>
                      <p style="line-height: 1em">Наличие личного авто желательно.</p>
                      <p style="line-height: 1em">Высшее образование.</p>
                      <p style="line-height: 1em">Коммуникабельность, грамотная речь.</p>
                      <p style="line-height: 1em">Обязанности:</p><br>

                      <p style="line-height: 1.2em">Поиск и привлечение клиентов (строительные, строительно-монтажные компании, застройщики, архитекторы, дизайнеры).</p>
                      <p style="line-height: 1em">Проведение презентаций для клиентов.</p>
                      <p style="line-height: 1em">Проведение переговоров и подготовка коммерческих предложений.</p>
                      <p style="line-height: 1em">Выезд к клиентам.</p>
                      <p style="line-height: 1em">Работа с существующей базой.</p>
                      <p style="line-height: 1em">Условия:</p><br>

                      <p style="line-height: 1.2em">Мы предлагаем интересную работу для активных и амбициозных людей в лидирующей Компании.</p>
                      <p style="line-height: 1em">Профессиональный рост в продажах.</p>
                      <p style="line-height: 1.2em">Возможность зарабатывать от 80 000 руб. и выше (фиксированный оклад + бонусная система).</p>
                      <p style="line-height: 1em">Оформление по ТК.</p>
                      <p style="line-height: 1em">График работы: понедельник-четверг с 9.00 — 18.00, пятница с 9.00 до 17.00.</p>
                      <p style="line-height: 1em">Офис - 15 минут пешком от метро Каширская.</p>
                    </div>
                  </div>
                </div><b>Звоните, либо направляйте ваше резюме с указанием желаемой должности по адресу: info@ceramostyle.ru.</b>
                <div class="art-soc-block">
                  <ul>
                    <li><a class="vk_soc" href="#vk"><i class="ico_soc"></i><span>240</span></a></li>
                    <li><a class="fb_soc" href="#fb"><i class="ico_soc"></i><span>240</span></a></li>
                    <li><a class="tw_soc" href="#tw"><i class="ico_soc"></i><span>240</span></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $footer; ?>
