<?php echo $header; ?>
<div class="liders-b sliders-block">
  <div class="container">
    <div class="row">
      <div class="site_size">
        <ul class="breadcrumbs">
            <?php foreach ($breadcrumbs as $breadcrumb): ?>
                <li>
                    <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
                        <?php echo $breadcrumb['text']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <h1 class="mainH1">Контакты компании ГК Керамостиль</h1>
        <div class="contact-cover">
          <div class="dost-cover">
            <div class="dost-item">
              <div class="oText">
                <h2>Наш центральный офис и выставочный зал находится по адресу:</h2>
                <p style="margin-top:10px">115201, Москва, 1-ый Котляковский переулок, д. 1, стр. 34</p>
                <p style="margin-top:10px">Бизнес-центр "ЕРМАК", 2-х этажное здание из красного кирпича, 2 этаж. От м. Каширская 15 мин. пешком</p>
                <p>На личном автотранспорте ориентироваться на адрес: 1-ый Котляковский пер, д. 3, напротив - Бизнес-центр "ЕРМАК".</p>
                <p style="margin-bottom:15px"></p>
                <h3 style="margin-bottom:20px">Телефоны:</h3>
                <p style="font-size:18px; line-height: 1em"><b>+7 495 223-34-96</b></p>
                <p style="font-size:18px; line-height: 1em"><b>+7 495 255-07-07 </b></p>
                <p style="font-size:18px; line-height: 1em"><b>+7 495 933-35-87 </b></p>
                <p style="font-size:18px; line-height: 1em"><b>+7 916 603-41-08</b></p>
                <p style="margin-top:16px">Все ваши вопросы, замечания, комментарии и заявки вы также можете направить нам по адресу:</p>
                <p>E-mail:info@ceramostyle.ru</p>
                <h3 style="margin-top:14px">График работы: </h3>
                <p style="margin-top:13px">понедельник — четверг: 9:00 - 18:00</p>
                <p style="margin-top:-10px">пятница: 9:00 - 17:00</p>
                <p style="margin-top:-8px">суббота: 10.00 - 16.00</p><br>
                <h3 style="margin-top:-12px">Юридический адрес:</h3>
                <p style="margin-top:11px">107078, г. Москва, проезд Мясницкий, д.4, стр.1</p>
                <p style="margin-top:-7px">ООО «ГК «КерамоСтиль»</p>
                <p style="margin-top:-7px">ОГРН 5147746441805</p>
                <p style="margin-top:-7px">ИНН 7701417010</p>
              </div>
            </div>
            <div class="dost-item">
            <div class="frame-cover">
            <iframe class="frame-contact__" src="https://api-maps.yandex.ua/frame/v1/-/CVt7MWil" width="100%" height="600" frameborder="0"></iframe></div></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
