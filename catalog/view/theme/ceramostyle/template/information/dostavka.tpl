<?php echo $header; ?>
<div class="liders-b sliders-block">
          <div class="container">
            <div class="row">
              <div class="site_size">
                <ul class="breadcrumbs">
                    <?php foreach ($breadcrumbs as $breadcrumb): ?>
    					  <li>
    						  <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
    							  <?php echo $breadcrumb['text']; ?>
    						  </a>
    					  </li>
    				  <?php endforeach; ?>
                </ul>
                <h1 class="mainH1">Доставка</h1>
                <div class="dost-cover">
                  <div class="dost-item">
                    <div class="oText">
                      <p>У нас есть собственный автопарк для доставки товаров по всей России. Мы всегда доставляем груз оперативно и четко и берем оформление документов на себя.</p>
                      <h2>Минимальная стоимость доставки по Москве - от 2 500 руб.</h2>
                      <p>За эти деньги вы получаете целую Газель, которая доставит весь ваш заказ по указанному адресу.</p>
                      <h2>Доставка крупных партий по Москве и/или регионам</h2>
                      <p>При заказе крупных партий товара или при необходимости доставки по регионам стоимость доставки рассчитывается индивидуально. Чтобы узнать предварительную стоимость доставки вашего заказа, вы можете заполнить форму обратной связи, указав следующую информацию:</p>
                      <ul>
                        <li>материал,</li>
                        <li>количество,</li>
                        <li>пункт назначения.</li>
                      </ul>
                      <h2>При заказе продукции Kerama Marazzi "Белое Солнце" от 500 м2 - доставка бесплатна!</h2>
                      <p>Также любой интересующий вопрос о доставке можно задать по телефону +7 (495) 223-34-96</p>
                    </div>
                  </div>
                  <div class="dost-item"><img src="/catalog/view/theme/ceramostyle/img/dostavka-car.jpg"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $footer; ?>
