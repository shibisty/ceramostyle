<?php echo $header; ?>
<div class="liders-b sliders-block">
  <div class="container">
	<div class="row">
	  <div class="site_size">
		<ul class="breadcrumbs">
            <?php foreach ($breadcrumbs as $breadcrumb): ?>
                <li>
                    <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
                        <?php echo $breadcrumb['text']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
		</ul>
		<h1 class="mainH1">Наши работы</h1>
		<div class="our-w-cover">
			<?php foreach ($banners as $banner): ?>
				<div class="ow-item"><a href="<?php echo $banner['link']; ?>">
					<div class="ow-img"><img src="<?php echo $banner['thumb']; ?>"></div>
					<div class="ow-bg"><img src="/catalog/view/theme/ceramostyle/img/our-w-bg.png">
					  <div class="ow--text-cover">
						  <?php if (isset($banner['title'][0])): ?>
							  <div class="ow-text"><?php echo $banner['title'][0]; ?></div>
						  <?php endif; ?>
						  <?php if (isset($banner['title'][1])): ?>
							  <div class="ow2-text"><?php echo $banner['title'][1]; ?></div>
						  <?php endif; ?>
					  </div>
					</div></a></div>
			<?php endforeach; ?>
		</div>
	  </div>
	</div>
  </div>
</div>
<?php echo $footer; ?>
