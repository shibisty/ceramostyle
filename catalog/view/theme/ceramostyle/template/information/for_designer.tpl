<?php echo $header; ?>
<div class="liders-b sliders-block">
  <div class="container">
    <div class="row">
      <div class="site_size">
        <ul class="breadcrumbs">
            <?php foreach ($breadcrumbs as $breadcrumb): ?>
                <li>
                    <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
                        <?php echo $breadcrumb['text']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <h1 class="mainH1">Дизайнерам и архитекторам</h1>
        <div class="arc-img"><img src="/catalog/view/theme/ceramostyle/img/arc-img.jpg">
          <div class="arc-text">вы - дизайнер или архитектор?</div>
        </div>
        <div class="arc-cover-text">
          <div class="oText">
            <h2>СОТРУДНИЧЕСТВО С «Ceramostyle» – ЭТО НОВЫЕ ВОЗМОЖНОСТИ ДЛЯ ВАШЕЙ ПРОФЕССИОНАЛЬНОЙ ДЕЯТЕЛЬНОСТИ</h2>
            <p>Специальное предложение для дизайнеров интерьера, архитекторов и декораторов! Мы предлагаем максимально удобные и стабильные условия работы, выгодную партнерскую программу, долгосрочное и доверительное сотрудничество с успешной компанией, которая имеет большой опыт работы на рынке строительных материалов и услуг.</p>
          </div>
          <h3 class="h3T">МЫ ПРИГЛАШАЕМ ВАС К ВЗАИМОВЫГОДНОМУ И ДОВЕРИТЕЛЬНОМУ СОТРУДНИЧЕСТВУ!</h3><a class="join_btn" href="#">Присоединиться</a>
        </div>
        <div class="arc-adv-block">
          <h3 class="h3T">ПРЕИМУЩЕСТВА СОТРУДНИЧЕСТВА</h3>
          <div class="adv-cover-container">
            <div class="adv-item">
              <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/des_arc/img-1.png"></div>
              <div class="adv-text">Широкий ассортимент товара и складская программа. Для Вас строительные материалы на любой вкус и уровень достатка</div>
            </div>
            <div class="adv-item">
              <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/des_arc/img-2.png"></div>
              <div class="adv-text">Предоставление каталогов с продукцией компании, ее подробным описанием</div>
            </div>
            <div class="adv-item">
              <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/des_arc/img-3.png"></div>
              <div class="adv-text">Бесплатное обучение работе в программе компьютерного 3D моделирования</div>
            </div>
            <div class="adv-item">
              <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/des_arc/img-4.png"></div>
              <div class="adv-text">Персональное обслуживание и индивидуальный подход к каждому партнеру</div>
            </div>
            <div class="adv-item">
              <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/des_arc/img-5.png"></div>
              <div class="adv-text">Коворкинг ceramostyle</div>
            </div>
            <div class="adv-item">
              <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/des_arc/img-6.png"></div>
              <div class="adv-text">Бесплатная доставка материалов по Москве и Московской области в максимально сжатые сроки</div>
            </div>
            <div class="adv-item">
              <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/des_arc/img-7.png"></div>
              <div class="adv-text">Размещение информации о вас и ваших работах в разделе Партнеры</div>
            </div>
            <div class="adv-item">
              <div class="adv-img"><img src="/catalog/view/theme/ceramostyle/img/des_arc/img-8.png"></div>
              <div class="adv-text">Конкурсы, призы, семинары и мастер-классы от нашей компании</div>
            </div>
          </div>
        </div>
        <div class="arc-after-text">
          <p>Специалисты компании «CeramoStyle» всегда готовы поддержать ваши творческие начинания, оказать всевозможное содействие, а также принять участие в реализации дизайн-проектов любого уровня сложности. Вы можете обратиться к нам за любой профессиональной помощью и рассчитывать на максимально оперативное разрешение любого вопроса.</p>
          <div class="h2T">СОТРУДНИЧАТЬ С «ceramostyle» НАДЕЖНО - МЫ ВСЕГДА ПОМОЖЕМ РЕШИТЬ ВСЕ ВОЗНИКАЮЩИЕ ВОПРОСЫ И ВОПЛОТИТЬ В ЖИЗНЬ ЗАДУМАННЫЙ ВАМИ ПРОЕКТ.</div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
