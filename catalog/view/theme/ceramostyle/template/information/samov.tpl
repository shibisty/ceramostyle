<?php echo $header; ?>
<div class="liders-b sliders-block">
  <div class="container">
    <div class="row">
      <div class="site_size">
        <ul class="breadcrumbs">
            <?php foreach ($breadcrumbs as $breadcrumb): ?>
                <li>
                    <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
                        <?php echo $breadcrumb['text']; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <h1 class="mainH1">Самовывоз</h1>
        <div class="oText">
          <p>Сеть салонов строительных материалов «Ceramostyle» освободит вас от забот по самостоятельной доставке товаров, приобретенных в наших салонах и интернет-магазине. Предоставьте это профессионалам и получайте удовольствие от результата. Доставка заказов осуществляется в максимально сжатые сроки в удобное для вас время. Мы приближаем радость от окончания ремонта, делая покупку любых отделочных материалов простой и комфортной!</p>
          <br>
          <div class="h2T">ВЫ МОЖЕТЕ ЗАБРАТЬ СВОЙ ЗАКАЗ САМОСТОЯТЕЛЬНО ИЗ ОДНОГО ИЗ НАШИХ ПУНКТОВ САМОВЫВОЗА:</div>
          <p style="margin-top: -10px">Также любой интересующий вопрос о доставке можно задать по телефону +7 (495) 223-34-96</p>
        </div>
        <div class="samov-contact">
          <div class="sc-block sc-addr-block">
            <div class="sc--title">Адрес склада:</div>
            <div class="sc--text">Москва, 1-ый Котляковский переулок, д.1, стр.. 34.</div>
          </div>
          <div class="sc-block sc-tel-block">
            <div class="sc--title">Контактный центр:</div>
            <div class="sc--text">8 (495) 223-34-96</div>
          </div>
          <div class="sc-block sc-email-block">
            <div class="sc--title">e-mail:</div>
            <div class="sc--text">info@ceramostyle.ru</div>
          </div>
        </div>
        <div class="samov-map">

          <!-- <iframe src="https://api-maps.yandex.ru/frame/v1/-/CVt7MWil?lang=ru_RU"" width="100%" height="330" frameborder="0"></iframe> -->
          <script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
          <div id="YMapID" style="width: 100%; height: 335px;"></div>
          <script type="text/javascript">
            jQuery(document).ready(function($) {
              $(window).on('load', function(event) {
                ymaps.ready(init);

                function init(){
                  var myMap = new ymaps.Map("YMapID", {
                    center: [55.6513, 37.63595987053599],
                    zoom: 17
                  });
                  var myPlacemark = new ymaps.Placemark([55.6513, 37.63595987053599], {}, {
                    preset: 'twirl#redIcon'
                  });

                  myMap.geoObjects.add(myPlacemark);
                }

              });
            });
          </script>

        </div>
        <div class="samov-rej">
          <div class="sc--title">Режим работы:</div>
          <p>Пн-Сб: с 9.00 до 18.00, без перерыва</p>
          <p>Вск: с 9.00 до 16.00, без перерыва</p>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
