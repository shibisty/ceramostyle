<div class="modal_form">
  <div class="mfi-container mfp-with-anim" id="storeLinkInner">
	<div class="mfi-container__inner">
	  <div class="mfp-close">x</div>
	  <div class="mfp-container__cover">
		<div class="mfp--title">В вашей корзине:</div>
		<div class="tov-p">
			<?php if ($products || $vouchers) { ?>
				<?php foreach ($products as $product) { ?>
				  <div class="tov--item-shoped">
					<div class="tp-block">
					  <div class="tovp-img-sm">
                           <a href="<?php echo $product['href']; ?>">
						        <img src="<?php echo $product['thumb']; ?>">
                          </a>
					  </div>
					  <div class="tovp-title">
                          <a href="<?php echo $product['href']; ?>">
                              <?php echo $product['name']; ?> <?php echo $product['option_str'] ? '('.$product['option_str'].')' : ''; ?>
                          </a>
                      </div>
					</div>
					<div class="tp-block tp__line">
					  <div class="tov-p-cost"><?php echo $product['price']; ?> руб</div>
					  <!-- <div class="tov-p-select">
						<select name="tp_select" style="width: 100%">
						  <option>100</option>
						  <option>200</option>
						  <option>300</option>
						</select>
					  </div> -->
					</div>
				  </div>
                  <div class="tov--item-shoped">
    	  			<div class="tp-block tp__line">
    	  			  <div class="tov-p-cost"><?php echo $text_items; ?></div>
    	  			  <div class="tov-p-remove" onclick="cart.remove(<?php echo $product['cart_id']; ?>)">Удалить</div>
    	  			</div>
    	  		  </div>
				<?php } ?>
                <a href="/index.php?route=checkout/cart"> <div class="tp-get-z">Оформить заказ</div></a>
			<?php } else { ?>
			  <p class="text-center"><?php echo $text_empty; ?></p>
			<?php } ?>
		</div>
	  </div>
	</div>
  </div>
</div>
