<div class="top-search">
    <div class="ts--text">Оптовые поставки стройматериалов</div>
    <div class="header-search" data-toggle="validator" role="form" data-focus="false" data-delay="1000">
        <form class="form-group"  id="search" action="/search" >
            <label>
                <input class="form-control form-search"
                value="<?php echo $search; ?>"
                id="inputSearch" name="search" type="text" data-minlength="2"
                placeholder="Искать товар по артикулу или названию..." required="" data-error="Не менее 2 символов">
                <input class="form-search-sub" type="submit">
            </label>
            <div class="help-block with-errors"></div>
        </form>
    </div>
</div>
