<!DOCTYPE html>
<html class="no-js" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>
	</title>
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?>
	<meta name="description"
	content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
	<?php } ?>
	<?php if ($keywords) { ?>
	<meta name="keywords" content= "<?php echo $keywords; ?>" />
	<?php } ?>

	<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo $og_url; ?>" />
	<?php if ($og_image) { ?>
	<meta property="og:image" content="<?php echo $og_image; ?>" />
	<?php } else { ?>
	<meta property="og:image" content="<?php echo $logo; ?>" />
	<?php } ?>
	<meta property="og:site_name" content="<?php echo $name; ?>" />

	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $theme_path; ?>/favicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $theme_path; ?>/favicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $theme_path; ?>/favicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $theme_path; ?>/favicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $theme_path; ?>/favicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $theme_path; ?>/favicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $theme_path; ?>/favicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $theme_path; ?>/favicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $theme_path; ?>/favicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="<?php echo $theme_path; ?>/favicons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $theme_path; ?>/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $theme_path; ?>/favicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $theme_path; ?>/favicons/favicon-16x16.png">
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="manifest" href="<?php echo $theme_path; ?>/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo $theme_path; ?>/favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!--Responsive-->
	<meta name="MobileOptimized" content="320">
	<meta name="HandheldFriendly" content="True">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!--Touch-->
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
		<!--[if IE]>
		<meta http-equiv="imagetoolbar" content="no"><![endif]-->

		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

		<link rel="stylesheet" href="<?php echo $theme_path; ?>/css/fonts.css">
		<link rel="stylesheet" href="<?php echo $theme_path; ?>/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo $theme_path; ?>/css/header.css">
		<link rel="stylesheet" href="<?php echo $theme_path; ?>/css/plugins.css">
		<link rel="stylesheet" href="<?php echo $theme_path; ?>/css/main.css">
		<script src="<?php echo $theme_path; ?>/js/modernizr.js"></script>
		<script src="<?php echo $theme_path; ?>/js/jquery-2.2.4.min.js"></script>
		<script src="<?php echo $theme_path; ?>/js/plugins.js"></script>
		<script src="<?php echo $theme_path; ?>/css/owl.carousel.min.js"></script>
		<link rel="stylesheet" href="<?php echo $theme_path; ?>/css/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo $theme_path; ?>/css/jquery.mmenu.all.css">
		<script src="<?php echo $theme_path; ?>/js/core.js"></script>
		<script src="<?php echo $theme_path; ?>/js/common.js"></script>
		<script src="<?php echo $theme_path; ?>/js/app.js"></script>
		<script src="/catalog/view/theme/ceramostyle/js/jquery-ui.js"></script>
		<script src="/catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="/catalog/view/javascript/common.js" type="text/javascript"></script>
		<script src="/catalog/view/theme/ceramostyle/js/noty/js/noty/packaged/jquery.noty.packaged.min.js"></script>
		<?php foreach ($links as $link) { ?>
		<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />

		<script src="/media/newsletter/js/jquery.subscribe-better.js" type="text/javascript"></script>
		<script src="/media/newsletter/js/main.js" type="text/javascript"></script>
		<?php } ?>
		<!-- script load -->
		<?php #foreach ($scripts as $script) { ?>
		<!-- <script src="<?php echo $script; ?>" type="text/javascript"></script> -->
		<?php #} ?>
		<!-- .scripts load -->
		<?php foreach ($analytics as $analytic) { ?>
		<?php echo $analytic; ?>
		<?php } ?>
	</head>
	<body class="<?php echo $class; ?>">

		<div class="wrapper-cover">
			<div class="wrapper">
				<header class="header">
					<div class="header-area">
						<div class="container">
							<div class="desktop-row">
								<div class="row">
									<div class="header-left">
										<nav class="top-nav">
											<ul>
												<?php foreach ($mainmenu[0] as $menu): ?>
													<li><a href="<?php echo $menu['link']; ?>">
														<span><?php echo $menu['name']; ?></span></a>
													</li>
												<?php endforeach; ?>
											</ul>
										</nav>
									</div>
									<?php if (!$logged): ?>
										<div class="header-right">
											<div class="enter-area">
												<a
												class="mfi"
												data-url="#enterLink"
												data-effect="mfp-move-horizontal">
												<span>Войти</span>
											</a>
											<a class="mfi" data-url="#regLink" data-effect="mfp-move-horizontal">
												<span>Регистрация</span>
											</a></div>
										</div>
									<?php else: ?>
										<div class="header-right">
											<div class="enter-area">
											<a
												href="/index.php?route=account/account">
												<span>Личный кабинет</span>
											</a>
											<a
												href="/index.php?route=account/logout">
												<span>Выйти</span>
											</a>
										</div>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="mob-row">
							<div class="row"><a class="mob-sm-btn" href="#mmOrig"><span></span></a>
								<div class="mob-sm-menu-sm">
									<ul class="mobile-sm-menu2">
										<?php foreach ($categories as $category) { ?>
										<li <?php echo $category['children'] ? 'class="has-sub"' : ''; ?>><a
											href="<?php echo $category['href']; ?>">
											<?php echo $category['name']; ?></a>
											<?php if ($category['children']) { ?>
											<div class="second-menu--lvl2">
												<div class="sm-left">
													<div class="sm-left--block">
														<div class="lvl2--title"><?php echo $category['name']; ?>
															<ul class="lvl2--list">
																<?php foreach (
																array_chunk(
																$category['children'],
																ceil(count($category['children']) / $category['column'])
																)
																as $children
																) { ?>
																<?php foreach ($children as $child) { ?>
																<li>
																	<a href="<?php echo $child['href']; ?>">
																		<?php echo $child['name']; ?>
																	</a>
																</li>
																<?php } ?>
																<?php } ?>
															</ul>
														</div>
													</div>
													<div class="sm-left--block">
														<div class="pl-colors">плитка по цветам :</div>
														<ul class="pl-colors-list">
															<li class="pl-white">
																<a href="<?php echo $category['color_white']; ?>"></a><i style="background-color: #fff"></i>
																<span>белая</span>
															</li>
															<li class="pl-red">
																<a href="<?php echo $category['color_gray']; ?>"></a><i style="background-color: #ff0000"></i>
																<span>красная</span>
															</li>
															<li class="pl-yellow">
																<a href="<?php echo $category['color_black']; ?>"></a><i style="background-color: #ffea00"></i>
																<span>желтая</span>
															</li>
															<li class="pl-black">
																<a href="<?php echo $category['color_yalow']; ?>"></a><i style="background-color: #161616"></i>
																<span>черная</span>
															</li>
															<li class="pl-gray">
																<a href="<?php echo $category['color_blue']; ?>"></a><i style="background-color: #414141"></i>
																<span>серая</span>
															</li>
															<li class="pl-blue">
																<a href="<?php echo $category['color_red']; ?>"></a><i style="background-color: #1762c7"></i>
																<span>синяя</span>
															</li>
														</ul>
													</div>
													<div class="sm-pr-block">
														<div class="sm-pr--title">производители:</div>
														<ul>
															<?php foreach ($manufacturer as $m): ?>
																<li><a href="<?php echo $m['href']; ?>">
																	<img src="<?php echo $m['thumb']; ?>">
																</a></li>
															<?php endforeach; ?>
														</ul>
													</div>
												</div>
												<div class="sm-right">
													<div class="sm-right--title">новинка</div>
													<a href="<?php echo $category['last']['href']; ?>">
														<div class="sm-pr--special" style="background-image:url(<?php echo $category['last']['thumb']; ?>)">
															<div class="prsp-title">
																<?php echo $category['last']['name']; ?></div>
															<div class="prsp-subtitle">
																<?php echo $category['last']['category_name']; ?></div>
														</div>
													</a>
												</div>
											</div>
											<?php } ?>
										</li>
									<?php } ?>
									<li class="act-sub"><a href="<?php echo $action; ?>">Акции</a>
										<!-- <div class="second-menu--lvl2">
											<ul class="act-list">
												<li><a href="#">Акция-1</a></li>
												<li><a href="#">Акция-1</a></li>
												<li><a href="#">Акция-1</a></li>
											</ul>
										</div> -->
									</li>
															</ul>
					</div>
					<span class="mob-site--title">CERAMOSTYLE<a href="/"></a></span>
					<div class="mob-search"></div>
					<a class="mob-call_back" href="tel:<?php echo $telephone; ?>">
						<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 463.009 463.009" enable-background="new 0 0 463.009 463.009" width="20px" height="20px"><path d="m462.692 381.085c-1.472-11.126-7.895-20.719-17.62-26.318l-114.226-65.767c-13.99-8.055-31.738-5.71-43.157 5.708l-22.499 22.499c-5.987 5.988-15.459 6.518-22.028 1.231-17.737-14.272-35.201-29.979-51.906-46.685-16.705-16.705-32.412-34.168-46.685-51.906-5.287-6.57-4.758-16.041 1.231-22.029l22.498-22.499c11.418-11.417 13.766-29.163 5.709-43.156l-65.767-114.226c-5.6-9.726-15.192-16.148-26.318-17.62-11.127-1.475-22.06 2.236-29.996 10.172l-33.901 33.902c-23.661 23.662-24.041 66.944-1.07 121.875 22.088 52.818 63.308 110.962 116.065 163.721 52.759 52.758 110.903 93.978 163.722 116.066 27.039 11.307 51.253 16.957 71.697 16.956 21.088 0 38.163-6.013 50.178-18.027l33.901-33.902c7.935-7.936 11.643-18.869 10.172-29.995zm-139.33-79.086l114.226 65.767c5.649 3.252 9.379 8.824 10.233 15.286 0.718 5.423-0.691 10.763-3.885 15.066l-151.805-86.638 6.165-6.165c6.631-6.631 16.941-7.994 25.066-3.316zm-243.406-286.811c6.463 0.855 12.034 4.585 15.286 10.234l65.767 114.226c4.68 8.127 3.316 18.435-3.315 25.065l-5.663 5.663-87.114-151.303c3.561-2.637 7.82-4.069 12.26-4.069 0.921-1.77636e-15 1.85 0.061 2.779 0.184zm328.055 419.187c-18.798 18.798-57.244 18.01-105.48-2.162-51.06-21.352-107.491-61.424-158.901-112.833-51.41-51.41-91.482-107.842-112.834-158.901-20.173-48.237-20.96-86.683-2.162-105.482l25.167-25.168 87.245 151.532-5.851 5.851c-11.415 11.416-12.409 29.488-2.311 42.04 14.609 18.156 30.68 36.024 47.764 53.108 17.086 17.085 34.954 33.156 53.109 47.765 12.55 10.098 30.622 9.105 42.04-2.312l5.338-5.338 152.016 86.759-25.14 25.141z" fill="#006DF0"/></svg>
					</a>
					<div class="fb-store mfi" data-url="#storeLinkInner">
						<span><span class="t-counter"><?php echo $cart_count; ?></span></span>
					</div>
					<div class="mob-form">
						<form class="header-search" action="/search" data-toggle="validator" role="form" data-focus="false" data-delay="1000">
							<div class="form-group">
								<input class="form-control form-search" id="inputSearch"
								name="search" type="text" data-minlength="2" placeholder="Искать товар по артикулу или названию..." required="" data-error="Не менее 2 символов">
								<input class="form-search-sub" type="submit">
								<div class="help-block with-errors"></div>
							</div>
						</form>
					</div><span class="mob-contact-sm">
					Москва, 1-ый Котляковский переулок, д. 1, стр. 34.<br><span>тел: </span><span class="tel-contact-sm">8 (495) 223 34 96</span></span>
				</div>
			</div>
										</div>
									</div>
									<div class="header-second-area">
										<div class="site_size">

											<div class="top-logo"><a href="/">
												<?php if ($logo) { ?>
												<img src="<?php echo $logo; ?>">
												<?php }?>
											</a></div>
											<?php echo $search; ?>
											<div class="top-tels-cover">
												<div class="top-tels"><span><?php echo $telephone; ?></span></div>
												<span class="top-callback mfi" data-url="#callBack">Заказать обратный звонок</span>
											</div>
											<div class="top-addr">
												<div class="fb-addr"><?php echo $adress; ?></div>
												<div class="fb-store mfi" data-url="#storeLinkInner"><span><?php echo $cart_count; ?></span></div>
											</div>
											<div class="mob-search tablet-search"></div>
											<form class="table-search-form" action="/search" data-toggle="validator" role="form" data-focus="false">
												<div class="form-group">
													<input class="form-control form-search form-tablet-search"
													id="inputSearch2" name="search" type="text" data-minlength="2" placeholder="" required="" data-error="Не менее 2 символов">
													<input class="form-search-sub" type="submit">
												</div>
												<div class="help-block with-errors"></div>
											</form>
										</div>
									</div>
									<div class="header-menu-area">
										<div class="header-second-menu">
											<div class="site_size">
												<ul class="second-menu" id="mmOrig">
													<?php foreach ($categories as $category) { ?>
													<li <?php echo $category['children'] ? 'class="has-sub-menu"' : ''; ?>>
														<a href="<?php echo $category['href']; ?>">
															<span><?php echo $category['name']; ?></span>
														</a>
														<?php if ($category['children']) { ?>
														<div class="second-menu--lvl2">
															<div class="sm-left">
																<div class="sm-left--block">
																	<div class="lvl2--title"><?php echo $category['name']; ?>
																		<?php foreach (
																		array_chunk(
																		$category['children'],
																		ceil(count($category['children']) / $category['column'])
																		)
																		as $children
																		) { ?>
																		<ul class="lvl2--list">
																			<?php foreach ($children as $child) { ?>
																			<li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
																			<?php } ?>
																		</ul>
																		<?php } ?>
																	</div>
																</div>
																<div class="sm-left--block">
																	<div class="pl-colors">плитка по цветам :</div>
																	<ul class="pl-colors-list">
																		<li class="pl-white">
																			<a href="<?php echo $category['color_white']; ?>"></a><i style="background-color: #fff"></i>
																			<span>белая</span>
																		</li>
																		<li class="pl-red">
																			<a href="<?php echo $category['color_red']; ?>"></a><i style="background-color: #ff0000"></i>
																			<span>красная</span>
																		</li>
																		<li class="pl-yellow">
																			<a href="<?php echo $category['color_yalow']; ?>"></a><i style="background-color: #ffea00"></i>
																			<span>желтая</span>
																		</li>
																		<li class="pl-black">
																			<a href="<?php echo $category['color_black']; ?>"></a><i style="background-color: #161616"></i>
																			<span>черная</span>
																		</li>
																		<li class="pl-gray">
																			<a href="<?php echo $category['color_gray']; ?>"></a><i style="background-color: #414141"></i>
																			<span>серая</span>
																		</li>
																		<li class="pl-blue">
																			<a href="<?php echo $category['color_blue']; ?>"></a><i style="background-color: #1762c7"></i>
																			<span>синяя</span>
																		</li>
										</ul>
												</div>
												<div class="sm-pr-block">
													<div class="sm-pr--title">производители:</div>
													<ul>
														<?php foreach ($manufacturer as $m): ?>
															<li><a href="<?php echo $m['href']; ?>">
																<img src="<?php echo $m['thumb']; ?>">
															</a></li>
														<?php endforeach; ?>
													</ul>
												</div>
											</div>
												<?php if (count($category['last'])): ?>
													<div class="sm-right">
														<div class="sm-right--title">новинка</div>
														<div class="sm-pr--special" style="background-image:url(<?php echo $category['last']['thumb']; ?>)">
															<a href="<?php echo $category['last']['href']; ?>">
																<div class="prsp-title"><?php echo $category['last']['name']; ?></div>
															</a>
															<a href="<?php echo $category['last']['href']; ?>">
																<div class="prsp-subtitle"><?php echo $category['last']['category_name']; ?></div>
															</a>
														</div>
													</div>
												<?php endif; ?>
											</div>
																				<?php } ?>
																			</li>
																			<?php } ?>
																		</ul><span class="mob-sm-btn" role="button"><span></span>
																		<div class="menu-mob-single">
																			<ul></ul>
																		</div></span>
																		<div class="second-menu-act"><a href="<?php echo $action; ?>" style="padding: 10px;"><span>АКЦИИ</span></a></div>
																	</div>
																</div>
															</div>
														</header>
