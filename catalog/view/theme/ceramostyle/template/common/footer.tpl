</div>
<style>.select2-container :focus{outline:none}.select2-results__option{font:17px/1.2em 'HelveticaNeueCyr Bold'}.select2-container--default .select2-results__option--highlighted[aria-selected]{background-color:#ddd;color:#111} </style>
<footer class="footer">
  <div class="footer--bg">
	<div class="site_size">
	  <div class="footer-sub">
		<div class="fs-text">узнай о новостях первым</div>
		<div>
			<form data-toggle="validator">
			   <div class="form-group subscribe-me">
					<input class="fs-inp form-control" type="email" placeholder="Ваш E-mail"
						data-minlength="4" name="subscribe"  data-fv-emailaddress="true" required=""
                        data-error="Некорректный email">
					<input class="fs-sub enter-subscribe" type="submit" value="подписаться">
					<div class="help-block with-errors"></div>
				</div>
			</form>
		</div>
        <script type="text/javascript">
            new AdvancedNewsletter({
                container_id: '.subscribe-me',
                input_id: 'input[name=subscribe]',
                submit_id: '.enter-subscribe',
                display_as: true
            });
        </script>
	  </div>
	  <div class="footer-lists">
         <?php foreach ($footermenu[0] as $menu): ?>
             <div class="fl-inner">
               <div class="fl--title"><?php echo $menu['name']; ?></div>
               <?php if (isset($footermenu[$menu['menu_id']]) && count($footermenu[$menu['menu_id']])): ?>
                   <ul>
                       <?php foreach ($footermenu[$menu['menu_id']] as $child): ?>
                            <li><a href="<?php echo $child['link']; ?>"><?php echo $child['name']; ?></a></li>
                       <?php endforeach; ?>
                   </ul>
               <?php endif; ?>
             </div>
         <?php endforeach; ?>
		<div class="fl--contants">
		  <div class="fl-c--title">контакты:</div>
		  <div class="fl-c--tel"><?php echo $telephone; ?></div>
		  <div class="fl-c--text"><?php echo $email; ?></div>
		  <span class="mfi fl--mfi" data-url="#callBack">Заказать обратный звонок</span>
		</div>
	  </div>
	  <div class="footer-after-l">
		<div class="mob-only-contacts">
		  <div class="fl--contants">
			<div class="fl-c--title">контакты:</div>
			<div class="fl-c--tel"><?php echo $telephone; ?></div>
			<div class="fl-c--text"><?php echo $email; ?></div>
			<span class="mfi fl--mfi" data-url="#callBack">Заказать обратный звонок</span>
		  </div>
		</div>
		<div class="f-logo"><img src="/catalog/view/theme/ceramostyle/img/flogo.png"></div>
		<?php echo $socials; ?>
	  </div>
	  <div class="footer-last">
		<div class="fl--copy">
			© 2009-2016 ООО «КерамоСтиль» Керамогранит и плитка, оптимальные цены.
			Доставка по Москве и области.
		</div>
		<div class="fl--too"><a target="_blank" href="http://toogarin.ru/"></a></div>
	  </div>
	</div>
  </div>
</footer>
<!-- <script src="/catalog/view/theme/bactosfer/js/app.js"></script> -->
<?php if(isset($message) && isset($message_status)): ?>
	<script type="text/javascript">
		$(function(){
			var Message = noty({
				text: "<?php echo $message; ?>",
				type: "<?php echo isset($message_status) && $message_status ? $message_status : 'success'; ?>",
				layout: "center",
				timeout:1500,
				animation: {
					open: {
						height: 'toggle'
					},
					close: {
						height: 'toggle'
					},
					easing: 'swing',
					speed: 500
				}
			});
		});
	</script>
<?php endif; ?>
<!--Noscript-->

<noscript class="noscript-info">
  <p>В важем браузере отключен JavaSscript!. Содержимое сайта может отображаться неправильно.</p>
</noscript>

<!--[if lt IE 10]>
<div class="noscript-info">
  <p>Вы используете устаревшую версию браузера Internet Explorer. Содержимое сайта может отображаться неправильно. Пожалуйста обновитесь!</p>
  <noscript>
	<p>В важем браузере отключен JavaSscript!. Содержимое сайта может отображаться неправильно.</p>
  </noscript>
</div><![endif]-->

<!--Modal-->

<!--reg my data-->
<div class="modal_form">
  <div class="mfi-container mfp-with-anim" id="regLink2">
	<div class="mfi-container__inner reg_form_pp">
	  <div class="mfp-close">x</div>
	  <div class="mfp-container__cover reg-container__cover">
		<div class="mfp--title"> Редактирование данных</div>
		<form class="bForm" action="?" data-focus="false" data-toggle="validator" role="form">
		  <div class="form-group">
			<label><span>Ваше имя *</span></label>
			<input type="text" data-minlength="2" required data-error="Не менее 2 символов" name="uname">
			<div class="help-block with-errors"></div>
		  </div>
		  <div class="form-group">
			<label><span>Телефон *</span></label>
			<input type="tel" data-minlength="2" required data-error="Некорректный телефон" name="utel">
			<div class="help-block with-errors"></div>
		  </div>
		  <div class="form-group">
			<label><span>E-mail *</span></label>
			<input type="email" data-minlength="2" required data-error="Некорректный email" name="umail">
			<div class="help-block with-errors"></div>
		  </div>
		  <div class="form-group lfg--prety">
			<label> <span class="form-group__helper">Пароль * </span>
			  <input class="form-control" id="inputPassword2" required name="u_pswd" type="password" data-minlength="6" data-error="Не менее 6 символов" data-match="#inputPasswordConfirm2" data-match-error="Пароли не совпадают">
			</label>
			<div class="help-block with-errors"></div><br>
			<label> <span class="form-group__helper">Подтверждение пароля: **</span>
			  <input class="form-control" id="inputPasswordConfirm2" required name="u_pswd2" type="password" data-minlength="6" data-error="Не менее 6 символов" data-match="#inputPassword2" data-match-error="Пароли не совпадают">
			</label>
			<div class="help-block with-errors"></div>
			<br>
			<br>
		  </div>
		  <input class="reg_data_sbmt" type="submit" value="Сохранить">
		</form>
	  </div>
	</div>
  </div>
</div>
<div class="modal_form">
  <div class="mfi-container mfp-with-anim" id="storeLink">
	<div class="mfi-container__inner">
	  <div class="mfp-close">x</div>
	  <div class="mfp-container__cover">
		<div class="mfp--title">Добавить товар в корзину</div>
		<p>Ваш товар добавлен в корзину, спасибо!</p>
	  </div>
	</div>
  </div>
</div>
<div class="modal_form">
  <div class="mfi-container mfp-with-anim" id="callBack">
	<div class="mfi-container__inner">
	  <div class="mfp-close">x</div>
	  <div class="mfp-container__cover call_back_form">
		<div class="mfp--title">Заказать обратный звонок</div>
		<form class="bForm" action="/index.php?route=callback/callback/add"
        data-toggle="validator" role="form" data-focus="false" method="POST">
		  <div class="form-group">
			<label><span>Ваше имя</span></label>
			<input class="form-control" type="text" data-minlength="2" required data-error="Не менее 2 символов"
            name="name">
			<div class="help-block with-errors"></div>
		  </div>
		  <div class="form-group">
			<label><span>Ваш телефон</span></label>
			<input class="form-control" type="tel" data-minlength="6" required data-error="Некорректный телефон"
            name="telephone">
			<div class="help-block with-errors"></div>
		  </div>
		  <input type="submit" value="Заказать">
		</form>
	  </div>
	</div>
  </div>
</div>
<?php echo $cart; ?>
<!--Modal password restore form-->
<?php echo $login; ?>
<!--My order-->
<div class="modal_form">
  <div class="mfi-container mfp-with-anim" id="myOrder">
	<div class="mfi-container__inner">
	  <div class="mfp-close">x</div>
	  <div class="mfp-container__cover call_back_form">
		<div class="mfp--title">Мой заказа</div>
		<form class="bForm" action="?" data-toggle="validator" role="form" data-focus="false">
		  <div class="mz-item">
			<div class="mz-item--img"><img src="/catalog/view/theme/ceramostyle/img/catalog/c1.png"></div>
			<div class="mz-item--title">Altaj Lappato Beige</div>
			<div class="mz-item--price">4 990 <span>руб.</span></div>
			<div class="mz-item--sub-text"><span>Есть c доставкой</span><span>Есть в магазине</span></div>
		  </div>
		  <div class="mfp--title st--get">Выберите способ доставки</div>
		  <div class="tov-tabs dost-tabs">
			<ul class="tt-heading">
			  <li class="active" data-href="#dost_k">Доставка курьером</li>
			  <li data-href="#samov_k">Самовывоз из салона</li>
			</ul>
			<div class="tt-content">
			  <div class="t-content-item active" id="dost_k">
				<div class="form-group">
				  <label><span>Город*</span></label>
				  <input class="form-control" type="text" data-minlength="2" required data-error="Некорректный город">
				  <div class="help-block with-errors"></div>
				</div>
				<div class="form-group--inline">
				  <div class="form-group fg--street">
					<label><span>Улица *</span></label>
					<input type="text" required data-error="Это поле обязательно">
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group">
					<label><span>Дом *</span></label>
					<input class="form-control" type="text" required data-error="Это поле обязательно">
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group">
					<label><span>Корпус</span></label>
					<input class="form-control" type="text" required>
				  </div>
				  <div class="form-group">
					<label><span>Квартира</span></label>
					<input type="text" required>
				  </div>
				</div>
				<div class="fg-your-data">
				  <div class="mfp--title st--get">Ваши данные</div>
				  <div class="form-group">
					<label><span>Ваше имя *</span></label>
					<input class="form-control" type="text" data-minlength="2" required data-error="Не менее 2 символов" name="uname">
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group">
					<label><span>Телефон *</span></label>
					<input class="form-control" type="tel" data-minlength="2" required data-error="Некорректный телефон" name="utel">
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group">
					<label><span>E-mail *</span></label>
					<input class="form-control" type="email" data-minlength="2" required data-error="Некорректный email" name="umail">
					<div class="help-block with-errors"></div>
				  </div>
				</div>
				<div class="fg-block">
				  <div class="fg-inner-block">
					<div class="fg-first-tov">Стоимость товаров:
					  <div class="fg-price">6 990<span>руб.</span></div>
					</div>
					<div class="fg-first-dost">Стоимость доставки:
					  <div class="fg-price">0<span>руб.</span></div>
					</div>
				  </div>
				  <div class="fg-inner-block">
					<div class="fg-second"><span class="fgs--text">Итого:</span><span class="fgs--price">
						<div class="fgs-price--text">6 990</div><span>руб.</span></span></div>
				  </div>
				</div>
				<div class="fg-last-block">
				  <div class="fgl--inner">
					<div class="fg--text">Нажимая "Оформить заказ", я подтверждаю, что ознакомлен с информацией о товаре и принимаю условия договора купли-продажи, условия оказания услуг связи и даю согласие на обработку моих персональных данных.</div>
					<button class="fg-btn">Оформить заказ</button>
				  </div>
				</div>
				<button class="fg-go-next">Продолжить покупки								</button>
			  </div>
			  <div class="t-content-item" id="samov_k">
				<div class="form-group">
				  <label><span>Ваше имя *</span></label>
				  <input class="form-control" type="text" data-minlength="2" required data-error="Не менее 2 символов" name="uname">
				  <div class="help-block with-errors"></div>
				</div>
				<div class="form-group">
				  <label><span>Телефон *</span></label>
				  <input class="form-control" type="tel" data-minlength="2" required data-error="Некорректный телефон" name="utel">
				  <div class="help-block with-errors"></div>
				</div>
				<div class="form-group">
				  <label><span>E-mail *</span></label>
				  <input class="form-control" type="email" data-minlength="2" required data-error="Некорректный email" name="umail">
				  <div class="help-block with-errors"></div>
				</div>
				<div class="form-group">
				  <label><span>Город *</span></label>
				  <input class="form-control" type="text" data-minlength="2" required data-error="Некорректный город" name="ucity">
				  <div class="help-block with-errors"></div>
				</div>
				<div class="form-group ci--sub">
				  <input class="btn-submit" type="submit" value="Отправить">
				</div>
			  </div>
			</div>
		  </div>
		</form>
	  </div>
	</div>
  </div>
</div>
<div class="modal_form">
  <div class="mfi-container mfp-with-anim" id="myOrder2">
	<div class="mfi-container__inner">
	  <div class="mfp-close">x</div>
	  <div class="mfp-container__cover call_back_form restore_pswwd_pp_form">
		<div class="mfp--title">Мой заказ</div>
		<div class="mz-cover">
		  <form class="bForm" action="/index.php?route=product/product/write" method="POST"
          data-toggle="validator" role="form" data-focus="false">
			<div class="fg-top">
			  <div class="form-group">
				<label><span>Электронная почта *</span>
				  <input class="form-control"
                  type="email" data-minlength="2" required data-error="Некорректный email" name="email">
				  <div class="help-block with-errors"></div>
				</label>
			  </div>
			  <div class="form-group">
				<label><span>Ваше имя *</span>
				  <input class="form-control" type="text" data-minlength="2"
                  required data-error="Не менее 2 символов" name="name">
				  <div class="help-block with-errors"></div>
				</label>
			  </div>
			</div>
			<div class="form-group">
			  <label><span>Отзыв</span></label>
			  <textarea class="order_textarea" name="text"></textarea>
			</div>
			<div class="stars-rat--cover">
			  <div class="ms--title">Моя оценка *</div>
			  <div class="stars-rat">
				<input class="star star-1" id="star-1" type="radio" value="1" name="rating">
				<label for="star-1"></label>
				<input class="star star-2" id="star-2" type="radio" value="2" name="rating">
				<label for="star-2"></label>
				<input class="star star-3" id="star-3" type="radio" value="3" name="rating">
				<label for="star-3"></label>
				<input class="star star-4" id="star-4" type="radio" value="4" name="rating">
				<label for="star-4"></label>
				<input class="star star-5" id="star-5" type="radio" value="5" name="rating">
				<label for="star-5"></label>
			  </div>
			</div>
			<div class="rev-checkbox">
			  <input type="checkbox" id="rc" checked>
			  <label for="rc"></label><span>Я рекомендую этот товар другим</span>
			</div>
			<div class="rev-c--text">Керамическая плитка Trend, изящно подчеркнутая тонкими светлыми прожилками, займет достойное место в любой ванной комнате. Легкость в укладке обеспечивает удобный формат изделия (25 х 42,5 см). Цветовая палитра спокойных, благородных оттенков, декоры и мозаика способны создать уникальную атмосферу в Вашей ванной комнате. Благодаря данной коллекции, любой декоратор придаст ванной комнате завершенный и эстетичный вид. Специалисты тщательно проработали материал изделия, сделав его прочным и долговечным. Если Вы хотите создать новое настроение в ванной комнате, то стоит рассмотреть коллекцию керамической плитки Trend, потому что она отвечает последним веяниям архитектурной моды.</div>
			<div class="rev-bottom">
			  <div class="rev-c--text-after">* Поля, обязательные для заполнения</div>
              <input class="btn-submit rev-sbmt" type="hidden"
              value="<?php echo isset($product_id) ? $product_id : 0; ?>" name="product_id">
			  <input class="btn-submit rev-sbmt" type="submit" value="Отправить">
			</div>
		  </form>
		</div>
	  </div>
	</div>
  </div>
</div>
<div class="modal_form">
  <div class="mfi-container mfp-with-anim" id="desOrd">
	<div class="mfi-container__inner">
	  <div class="mfp-close">x</div>
	  <div class="mfp-container__cover enter-reg-container">
		<div class="po-cover des-reg--cover">
		  <div class="mfp--title">Запись к дизайнеру</div>
		  <form class="bForm" action="?" role="form" data-focus="false" data-toggle="validator">
			<div class="form-group">
			  <label><span>Сотрудник</span></label>
			  <select style="width:100%">
				<option disabled delected></option>
				<option>Сотрудник-1</option>
				<option>Сотрудник-2</option>
			  </select>
			</div>
			<div class="form-group">
			  <label><span>Услуга</span></label>
			  <select style="width:100%">
				<option disabled delected></option>
				<option>Услуга-1</option>
				<option>Услуга-2</option>
			  </select>
			</div>
			<div class="form-group">
			  <label><span>Дата</span>
				<input class="form-control" type="date">
				<div class="help-block with-errors"></div>
			  </label>
			</div>
			<div class="form-group">
			  <label><span>Время</span>
				<input class="form-control" type="text">
				<div class="help-block with-errors"></div>
			  </label>
			</div>
			<div class="des--text">Адрес: 117485, г. Москва, 1-ый Котляковский переулок, д. 1, стр. 34 +7(495) 223-34-96</div>
			<div class="sub-enter-link">
			  <input class="btn-submit" type="submit" value="оформить визит">
			</div>
		  </form>
		</div>
	  </div>
	</div>
  </div>
</div>
</div>
</body>
</html>
