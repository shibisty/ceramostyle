<?php echo $header; ?>
<div class="liders-b sliders-block">
	<div class="container">
		<div class="row">
			<div class="site_size">
				<ul class="breadcrumbs">
  				  <?php foreach ($breadcrumbs as $breadcrumb): ?>
  					  <li>
  						  <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
  							  <?php echo $breadcrumb['text']; ?>
  						  </a>
  					  </li>
  				  <?php endforeach; ?>
  	      	  </ul>
				<h1 class="mainH1">Акции и скидки на керамогранит и керамическую плитку</h1>
				<?php foreach ($banners as $banner): ?>
					<div class="act-block"><img src="<?php echo $banner['thumb']; ?>">
						<div class="act--title">
							<?php if (isset($banner['title'][0])): ?>
								<?php echo htmlspecialchars_decode($banner['title'][0]); ?>
							<?php endif; ?>
							<?php if (isset($banner['title'][1])): ?>
								<span><?php echo htmlspecialchars_decode($banner['title'][1]); ?></span>
							<?php endif; ?>
						</div>
						<?php if (isset($banner['title'][2])): ?>
							<div class="act--sub"><?php echo htmlspecialchars_decode($banner['title'][2]); ?></div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>

			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
