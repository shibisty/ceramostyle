<?php echo $header; ?>
<div class="liders-b sliders-block">
  <div class="container">
	<div class="row">
	  <div class="site_size">
		<div class="search-breadcrumbs">
			<ul class="breadcrumbs">
				<?php foreach ($breadcrumbs as $breadcrumb): ?>
					<li>
						<a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
							<?php echo $breadcrumb['text']; ?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<div class="header-search sfp-page-cover">
			<form class="form-group" action="/search">
			  <input class="form-search" value="<?php echo htmlspecialchars($_GET['search']); ?>"
			  type="text" name="search" data-minlength="2" placeholder="" data-error="Ошибки валидации" required="">
			  <div class="help-block with-errors"></div>
			  <input type="submit">
			</form>
		</div>
		<div class="sp--title">
		  <h1 class="mainH1">Результат поиска</h1>
		</div>
		<div class="sr-cover">
			<?php if (count($products)): ?>
				<?php foreach ($products as $product): ?>
					<div class="sr-item"><a class="sr--title" href="<?php echo $product['href']; ?>">
							<h2><?php echo $product['name']; ?></h2></a>
						<div class="sr--text"><?php echo $product['description']; ?></div>
						</div>
				<?php endforeach; ?>
			<?php else: ?>
				<p>
					По вашему запросу ничего не найдено. Перейти на <a href="/">главную страницу</a>
				</p>
			<?php endif; ?>
		</div>
		<div class="search-pagin">
			<?php echo $pagination; ?>
		</div>
	  </div>
	</div>
  </div>
</div>
<?php echo $footer; ?>
