<?php echo $header; ?>
<div class="liders-b sliders-block product-item">
	<div class="container">
		<div class="row">
			<div class="site_size">
				<ul class="breadcrumbs">
  				  <?php foreach ($breadcrumbs as $breadcrumb): ?>
  					  <li>
  						  <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
  							  <?php echo $breadcrumb['text']; ?>
  						  </a>
  					  </li>
  				  <?php endforeach; ?>
  		  	  </ul>
				<div class="tov-cover">
					<div class="tov-part">
						<div class="tov--slider">
							<div class="tov-sl-img" style="background-image:url(<?php echo $thumb; ?>)"></div>
							<div class="tov-sl-sm-img">
								<?php foreach ($images as $image): ?>
									<div class="sm-img" data-big-img="<?php echo $image['popup']; ?>">
										<img src="<?php echo $image['thumb']; ?>">
									</div>
								<?php endforeach; ?>
							</div>
						</div>
						<div class="tov-share-btns">
							<div class="social-shared-config">
								<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
								<script src="//yastatic.net/share2/share.js"></script>
								<div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,twitter" data-counter=""></div>
							</div>
						</div>
					</div>
					<div class="tov-part">
						<h1 class="mainH1"><?php echo $heading_title; ?></h1>
						<div class="tov-rat">
							<div class="sbsl--stars" data-ratio="<?php echo $rating; ?>"></div>
						</div>
						<div class="tov-price">
							<?php if ($special): ?>
								<?php echo $special; ?>
							<?php else: ?>
								<?php echo $price; ?>
							<?php endif; ?>

							<?php if ( $cost_type == 0): ?>
								<span>руб/м.кв</span>
						   <?php endif; ?>
						   <?php if ( $cost_type == 1): ?>
								<span>руб/шт.</span>
						   <?php endif; ?>
						</div>
						<div class="tov-selects">
							<input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
							<div class="tov-selects-cover">
								<?php //if ($cost_type): ?>
									<div class="tov-select-item">
										<div class="ts--title">В штуках</div>
										<select class="tov-select" name="tov_count" style="width: 100%">
											<?php if ($quantity > 0 && $quantity < 100): ?>
												<option selected value="<?php echo $quantity; ?>"><?php echo $quantity; ?></option>
												<?php $selected = 1; ?>
											<?php endif; ?>
											<option <?php empty($selected) ? 'selected' : ''; ?> value="100">100</option>
											<option value="200">200</option>
											<option value="300">300</option>
											<option value="400">400</option>
											<option value="500">500</option>
										</select>
									</div>
								<?php //endif; ?>

							<?php //if (!$cost_type): ?>
								<?php foreach ($options as $option): ?>
									<?php if ($option['type'] === 'select'): ?>
										<?// print_r($option); ?>
										<div class="tov-select-item">
											<div class="ts--title"><?php echo $option['name']; ?></div>
											<select class="tov-select" data-id="<?php echo $option['product_option_id']; ?>" name="option-<?php echo $option['product_option_id']; ?>" style="width: 100%">
												<?php foreach ($option['product_option_value'] as $value): ?>
													<option value="<?php echo $value['product_option_value_id']; ?>">
														<?php echo $value['name']; ?>
													</option>
												<?php endforeach; ?>
											</select>
										</div>
									<?php endif; ?>
								<?php endforeach; ?>
						</div>

							<?php //endif; ?>
							<div class="tov-select-btn">
								<button class="add-item-to mfi" data-url="#storeLink" data-effect="mfp-move-horizontal" >Добавить в корзину</button>
							</div>
						</div>
						<div class="tov-info">
							<ul>
								<li class="ti-nal">Наличие:<span><?php echo $quantity > 0 ? 'В наличии' : 'Нет в наличии' ?></span></li>
								<li class="ti-when">Самовывоз:<span>Сегодня</span></li>
								<li class="ti-dost">Доставка:<span>Завтра</span></li>
							</ul>
						</div>
						<div class="tov-tabs">
							<ul class="tt-heading">
								<li class="active" data-href="#t1">Описание</li>
								<li data-href="#t2">Характеристики</li>
								<li data-href="#t3">Отзывы</li>
							</ul>
							<div class="tt-content">
								<div class="t-content-item active" id="t1">
									<div class="oText">
										<h2>Описание <?php echo $heading_title; ?></h2>
										<?php echo $description; ?>
									</div>
								</div>
								<div class="t-content-item" id="t2">
									<div class="oText">
										<?php foreach ($attribute_groups as $attribute_group): ?>
											<table>
												<thead>
													<tr>
														<th><?php echo $attribute_group['name']; ?></th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($attribute_group['attribute'] as $attr): ?>
														<tr>
															<td><?php echo $attr['name']; ?></td>
															<td><?php echo $attr['text']; ?></td>
														</tr>
													<?php endforeach; ?>
												</tbody>
											</table>
										<?php endforeach; ?>

									</div>
								</div>
								<?php echo $review; ?>
							</div>
						</div>
					</div>
				</div>
				<?php echo $view; ?>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
