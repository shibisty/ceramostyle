<?php echo $header; ?>
<div class="liders-b sliders-block category-tpl">
	<div class="container">
		<div class="row">
			<div class="site_size">
				<ul class="breadcrumbs">
					<?php foreach ($breadcrumbs as $breadcrumb): ?>
						<li>
							<a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
								<?php echo $breadcrumb['text']; ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
				<?php echo $filter; ?>
				<h1 class="mainH1 catalog-main-h1"><?php echo $heading_title; ?></h1>
				<div class="catalog--text-filter">
				<select style="width:100%" name="sort">
						<?php foreach ($sorts as $sort): ?>
							<option data-href="<?php echo $sort['href']; ?>"
								<?php echo isset($_GET['sort']) && $_GET['order']
								&& $sort['value'] == $_GET['sort'].'-'.$_GET['order'] ? 'selected' : ''; ?>>
								<?php echo $sort['text']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="catalog-items--cover">
					<?php foreach ($products_top as $product) { ?>
					<div class="catalog-item">
						<div class="cat-item--inner">
							<a class="ca-link" href="<?php echo $product['href']; ?>">
								<div class="ca--img" style="background-image:url(<?php echo $product['thumb']; ?>)"></div>
							</a>
							<div class="sbsl--text1"><?php echo $product['category_name']; ?></div>
							<div class="ca--text" title="<?php echo $product['name']; ?>">
								<a href="<?php echo $product['href']; ?>">
									<?php echo $product['name']; ?>
								</a>
							</div>
							<div class="sbsl--stars" data-ratio="2"></div>
							<div class="sbsl--price">
								<div class="sl--price">
									<?php if ($product['special']): ?>
										<?php echo $product['special']; ?>
									<?php else: ?>
										<?php echo $product['price']; ?>
									<?php endif; ?>

									<?php if ( $product['cost_type'] == 0): ?>
			                             <span>руб/м.кв</span>
			                        <?php endif; ?>
			                        <?php if ( $product['cost_type'] == 1): ?>
			                             <span>руб/шт.</span>
			                        <?php endif; ?>
								</div>
								<div class="sl--buy mfi" data-url="#storeLink"
								onclick="cart.add('<?php echo $product['product_id']; ?>');" data-effect="mfp-move-horizontal"></div>
							</div>
						</div>
					</div>
					<?php } ?>
					<?php echo $bannerCatagory; ?>
					<?php foreach ($products_bottom as $product) { ?>
					<div class="catalog-item">
						<div class="cat-item--inner">
							<a class="ca-link" href="<?php echo $product['href']; ?>">
								<div class="ca--img" style="background-image:url(<?php echo $product['thumb']; ?>)"></div>
							</a>
							<div class="sbsl--text1"><?php echo $product['category_name']; ?></div>
							<div class="ca--text" title="<?php echo $product['name']; ?>">
								<a href="<?php echo $product['href']; ?>">
									<?php echo $product['name']; ?>
								</a>
							</div>
							<div class="sbsl--stars" data-ratio="2"></div>
							<div class="sbsl--price">
								<div class="sl--price">
									<?php if ($product['special']): ?>
										<?php echo $product['special']; ?>
									<?php else: ?>
										<?php echo $product['price']; ?>
									<?php endif; ?>
									<!-- <span>руб /м.кв</span> -->
								</div>
								<div class="sl--buy mfi" data-url="#storeLink" data-effect="mfp-move-horizontal"></div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<div class="catalog-pagin">
					<?php echo $pagination; ?>
				</div>
				<div class="catalog--text">
					<div class="oText">
						<?php echo htmlspecialchars_decode($category_description); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
