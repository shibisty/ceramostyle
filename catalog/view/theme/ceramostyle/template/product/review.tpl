<div class="t-content-item" id="t3">
	<div class="rev-comments">
		<ul>
			<?php foreach ($reviews as $review): ?>
				<li>
					<div class="comment-inner">
						<div class="comment-top">
							<div class="comment-top-rat">
								<div data-ratio="<?php echo $review['rating']; ?>" class="rateYo_comm"></div>
							</div>
							<div class="comment-top-date"><span><?php echo $review['date_added']; ?></span></div>
							<div class="comment-top-name"><span><?php echo $review['author']; ?>:</span></div>
						</div>
						<div class="comment-inner--text">
							<div class="oText">
								<p><?php echo $review['text']; ?></p>
							</div>
						</div>
					</div>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php echo $pagination; ?>
	</div>
	<a class="mfi leave-comm" data-url="#myOrder2" data-effect="mfp-move-horizontal">Оставить отзыв</a>
</div>
