<?php echo $header; ?>

<div class="liders-b sliders-block">
  <div class="container">
	<div class="row">
	  <div class="site_size">
		  <ul class="breadcrumbs">
			  <?php foreach ($breadcrumbs as $breadcrumb): ?>
				  <li>
					  <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
						  <?php echo $breadcrumb['text']; ?>
					  </a>
				  </li>
			  <?php endforeach; ?>
	  	  </ul>
		<h1 class="mainH1">Производители</h1>
		<div class="pr-block">
		  <div class="pr-text">Производители плитки</div>
		  <img src="/catalog/view/theme/ceramostyle/img/pr-img.jpg">
		</div>
		<div class="tov-tabs tov-pr-tabs">
		  <ul class="tt-heading">
			<li class="active" data-href="#t1">По брендам</li>
			<li data-href="#t2">По странам</li>
		  </ul>
		  <div class="tt-content">
			<div class="t-content-item active" id="t1">
			  <div class="oText">
				<ul class="pr-list">
				  <?php foreach ($manufacturer as $item): ?>
					  <li>
						  <a href="<?php echo $item['href']; ?>">
							  <?php echo $item['name']; ?>
						  </a>
					  </li>
				  <?php endforeach; ?>
				</ul>
			  </div>
			</div>
			<div class="t-content-item" id="t2">
			  <div class="oText">
				<ul class="pr-list">
					<?php foreach ($countries as $item): ?>
  					  <li>
  						  <a href="<?php echo $item['href']; ?>">
  							  <?php echo $item['country']; ?>
  						  </a>
  					  </li>
  				  <?php endforeach; ?>
				</ul>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>

<?php echo $footer; ?>
