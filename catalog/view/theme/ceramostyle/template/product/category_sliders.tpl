<?php echo $header; ?>
<div class="liders-b sliders-block">
	  <div class="container">
	      <div class="row">
	  	    <div class="site_size">
	      	  <ul class="breadcrumbs">
				  <?php foreach ($breadcrumbs as $breadcrumb): ?>
					  <li>
						  <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
							  <?php echo $breadcrumb['text']; ?>
						  </a>
					  </li>
				  <?php endforeach; ?>
	      	  </ul>
	          </div>
			  <?php echo $filter; ?>
	      </div>
	  </div>
	  <div class="site_size">
		  <?php foreach ($childs as $cat): ?>
			  <?php if (isset($products_top[$cat['category_id']]) && count($products_top[$cat['category_id']])): ?>
				  <div class="sliders-block catalog-sliders">
					    <h1 class="t-header"><?php echo $cat['name']; ?></h1>
					    <div class="t-sl-nav">
							<a href="<?php echo $cat['href']; ?>">посмотреть все</a>
					  	<span class="t-sl-prev-23 t-sl-prev" role="button"></span>
					  	<span class="t-sl-next-23 t-sl-next" role="button"></span>
					    </div>
					    <div class="sliders-block-sl--cover owl-carousel">
						    <?php foreach ($products_top[$cat['category_id']] as $product): ?>
								<div class="sliders-block-sl"><a class="ca-link" href="<?php echo $product['href']?>">
							  	    <div class="sbsl--img" style="background-image:url(<?php echo $product['thumb']?>)">
									</div></a>
								  	<div class="sbsl--text1"><?php echo $product['category_name']?></div>
								  	<div class="sbsl--text2"><?php echo $product['name']?></div>
								  	<div class="sbsl--stars" data-ratio="1"></div>
								  	<div class="sbsl--price">
								  	  <div class="sl--price">
										  	<?php if ($product['special']): ?>
				  								<?php echo $product['special']; ?>
				  							<?php else: ?>
				  								<?php echo $product['price']; ?>
				  							<?php endif; ?>

										  <?php if ( $product['cost_type'] == 0): ?>
				                               <span>руб/м.кв</span>
				                          <?php endif; ?>
				                          <?php if ( $product['cost_type'] == 1): ?>
				                               <span>руб/шт.</span>
				                          <?php endif; ?>
									  </div>
								  	  <div class="sl--buy mfi" data-url="#storeLink" data-effect="mfp-move-horizontal"></div>
								  	</div>
							    </div>
						    <?php endforeach; ?>
					    </div>
				  </div>
			  <?php endif; ?>
		  <?php endforeach; ?>
		<div class="catalog--text">
		  <div class="oText">
			<?php echo htmlspecialchars_decode($category_description); ?>
			</div>
		</div>
	  </div>
	</div>
<?php echo $footer; ?>
