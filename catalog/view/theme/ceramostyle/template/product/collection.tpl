<?php echo $header; ?>
<div class="liders-b sliders-block">
		  <div class="container">
			<div class="row">
			  <div class="site_size">
				<ul class="breadcrumbs">
					<?php foreach ($breadcrumbs as $breadcrumb): ?>
						  <li>
							  <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
								  <?php echo $breadcrumb['text']; ?>
							  </a>
						  </li>
					<?php endforeach; ?>
				</ul>
				<div class="tov-cover">
				  <div class="tov-part">
					<div class="tov--slider">
					  <div class="tov-sl-img" style="background-image:url(<?php echo $thumb; ?>)"></div>
					  <div class="tov-sl-sm-img">
						  <?php foreach ($images as $image): ?>
							  <div class="sm-img" data-big-img="<?php echo $image['popup']; ?>">
								  <img src="<?php echo $image['thumb']; ?>">
							  </div>
						  <?php endforeach; ?>
					  </div>
					</div>
					<div class="tov-share-btns">
						<div class="ts-vk"><img src="/catalog/view/theme/ceramostyle/img/social/vk_share.png"></div>
						<div class="ts-fb"><img src="/catalog/view/theme/ceramostyle/img/social/fb_share.png"></div>
					</div>
				  </div>
				  <div class="tov-part">
					  <?php if (isset($manufacturer['thumb'])): ?>
						<div class="tov-brand"><img src="<?php echo $manufacturer['thumb']; ?>"></div>
					  <?php endif; ?>
					<div class="_clearFix">
					  <div class="brand--title-cover">
						<h1 class="mainH1"><?php echo $heading_title; ?></h1>
					  </div>
					</div>
					<div class="tov-rat">
					  <div class="sbsl--stars" data-ratio="<?php echo $rating; ?>"></div>
					</div>
					<div class="tov-info tov-info--collection">
					  <ul>
						  <?php if (isset($manufacturer['name'])): ?>
							  <li>Производитель:<span><?php echo $manufacturer['name']; ?></span></li>
						  <?php endif; ?>
						<!-- <li>Страна:<span>Италия</span></li> -->
					  </ul>
					</div>
					<div class="tov-tabs">
					  <ul class="tt-heading">
						<li class="active" data-href="#t1">Описание</li>
						<li data-href="#t2">Характеристики</li>
						<li data-href="#t3">Отзывы</li>
					  </ul>
					  <div class="tt-content">
						<div class="t-content-item active" id="t1">
						  <div class="oText">
							  <h2>Описание <?php echo $heading_title; ?></h2>
							  <?php echo $description; ?>
						  </div>
						</div>
						<div class="t-content-item" id="t2">
						  <div class="oText">
							  <?php foreach ($attribute_groups as $attribute_group): ?>
								  <table>
									  <thead>
										  <tr>
											  <th><?php echo $attribute_group['name']; ?></th>
											  <th></th>
										  </tr>
									  </thead>
									  <tbody>
										  <?php foreach ($attribute_group['attribute'] as $attr): ?>
											  <tr>
												  <td><?php echo $attr['name']; ?></td>
												  <td><?php echo $attr['text']; ?></td>
											  </tr>
										  <?php endforeach; ?>
									  </tbody>
								  </table>
							  <?php endforeach; ?>
						  </div>
						</div>
						<!-- <div class="t-content-item" id="t3"> -->
							<?php echo $review; ?>
						<!-- </div> -->
					  </div>
					</div>
				  </div>
				</div>
				<div class="after-collection">
				  <div class="mainH1 catalog-main-h1">Образцы коллекции</div>
				  <div class="catalog--text-filter">по возрастанию цены</div>
				  <div class="catalog-items--cover">
					  <?php foreach ($collection_items as $product): ?>
						  <div class="sliders-block-sl">
						     <a class="ca-link" href="<?php echo $product['href']; ?>">
						  	   <div class="sbsl--img" style="background-image:url(<?php echo $product['thumb']; ?>)"></div>
						     </a>
						     <div class="sbsl--text1"><?php echo $product['category_name']; ?></div>
						     <div class="sbsl--text2"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
						     <div class="sbsl--stars" data-ratio="<?php echo $product['rating']; ?>"></div>
						     <div class="sbsl--price">
						  	   <div class="sl--price">
						  		   <?php echo $product['price']; ?>
								   <?php if ( $cost_type == 0): ?>
 		                               <span>руб/м.кв</span>
 		                          <?php endif; ?>
 		                          <?php if ( $cost_type == 1): ?>
 		                               <span>руб/шт.</span>
 		                          <?php endif; ?>
						  	   </div>
						  	   <div class="sl--buy mfi" data-url="#storeLink"
						  	   onclick="cart.add('<?php echo $product['product_id']; ?>');"></div>
						     </div>
						  </div>
					  <?php endforeach; ?>
				  </div>
				</div>
				<?php if (count($products)): ?>
					<div class="similiar-collection">
					  <div class="site_size">
						<div class="sliders-block">
						  <div class="t-header">похожие коллекции</div>
						  <div class="t-sl-nav">
							  <a href="<?php echo $more_link; ?>">посмотреть все</a>
							  <span class="t-sl-prev-23 t-sl-prev" role="button"></span>
							  <span class="t-sl-next-23 t-sl-next" role="button"></span>
						  </div>
						  <?php foreach ($products as $product): ?>
							  <div class="sliders-block-sl">
							 	 <a class="ca-link" href="<?php echo $product['href']; ?>">
							 		 <div class="sbsl--img" style="background-image:url(<?php echo $product['thumb']; ?>)"></div>
							 	 </a>
							 	 <div class="sbsl--text1"><?php echo $product['category_name']; ?></div>
							 	 <div class="sbsl--text2"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
							 	 <div class="sbsl--stars" data-ratio="<?php echo $product['rating']; ?>"></div>
							 	 <div class="sbsl--price">
							 		 <div class="sl--price">
							 			 <?php echo $product['price']; ?>
							 			 <!-- <span>руб /м.кв</span> -->
							 		 </div>
							 		 <div class="sl--buy mfi" data-url="#storeLink"
							 		 onclick="cart.add('<?php echo $product['product_id']; ?>');"></div>
							 	 </div>
							  </div>
						  <?php endforeach; ?>
						</div>
					  </div>
					</div>
				<?php endif; ?>
			  </div>
			</div>
		  </div>
		</div>
<?php echo $footer; ?>
