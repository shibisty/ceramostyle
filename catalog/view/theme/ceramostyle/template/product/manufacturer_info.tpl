<?php echo $header; ?>
<div class="container">
    <div class="row">
	  <div class="site_size">
          <ul class="breadcrumbs">
			  <?php foreach ($breadcrumbs as $breadcrumb): ?>
				  <li>
					  <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
						  <?php echo $breadcrumb['text']; ?>
					  </a>
				  </li>
			  <?php endforeach; ?>
	  	  </ul>
          <?php echo $filter; ?>
          <div class="row" style="max-width:none;">
            <div id="content" class="col-sm-12">
              <?php if ($description) { ?>
                  <div class="row" style="margin-top:15px;">
                     <div class="col-sm-2" style="float:left;">
                       <h1 class="mainH1 catalog-main-h1"><?php echo $heading_title; ?></h1>
                      </div>
                    <?php if ($thumb) { ?>
                        <div class="col-sm-2" style="float:right;">
                            <img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>"
                            title="<?php echo $heading_title; ?>" class="img-thumbnail" style="float:right;"/>
                        </div>
                    <?php } ?>
                  </div>
                  <hr>
              <?php } ?>
              <?php if ($products) { ?>
              <div class="row">
                  <div class="catalog-items--cover" style="margin-left: 16px;">
                <?php foreach ($products as $product) { ?>
                    <div class="catalog-item">
						<div class="cat-item--inner">
							<a class="ca-link" href="<?php echo $product['href']; ?>">
								<div class="ca--img" style="background-image:url(<?php echo $product['thumb']; ?>)"></div>
							</a>
							<div class="sbsl--text1"><?php echo $product['category_name']; ?></div>
							<div class="ca--text" title="<?php echo $product['name']; ?>">
								<a href="<?php echo $product['href']; ?>">
									<?php echo $product['name']; ?>
								</a>
							</div>
							<div class="sbsl--stars" data-ratio="2"></div>
							<div class="sbsl--price">
								<div class="sl--price">
									<?php if ($product['special']): ?>
										<?php echo $product['special']; ?>
									<?php else: ?>
										<?php echo $product['price']; ?>
									<?php endif; ?>

									<?php if ( $product['cost_type'] == 0): ?>
			                             <span>руб/м.кв</span>
			                        <?php endif; ?>
			                        <?php if ( $product['cost_type'] == 1): ?>
			                             <span>руб/шт.</span>
			                        <?php endif; ?>
								</div>
								<div class="sl--buy mfi" data-url="#storeLink"
								onclick="cart.add('<?php echo $product['product_id']; ?>');" data-effect="mfp-move-horizontal"></div>
							</div>
						</div>
					</div>
                <?php } ?>
              </div>
              <div class="row manufacturer-info-row">
                <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
              </div>
              <?php } ?>
          </div>
          <?php if ($description) { ?>
              <hr>
               <div class="row" style="max-width:none;">
              <div class="col-sm-12">
                  <div class="row" style="clear:both;">
                      <div class="col-sm-12" style="float:right;">
                      <?php echo $description; ?>
                      </div>
                  </div>
              </div>
              </div>
              <hr style="opacity:0;">
          <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
