<?php echo $header; ?>
<div class="liders-b sliders-block">
  <div class="container">
    <div class="row">
      <div class="site_size">
        <ul class="breadcrumbs">
          <?php foreach ($breadcrumbs as $breadcrumb): ?>
           <li>
            <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
             <?php echo $breadcrumb['text']; ?>
           </a>
         </li>
       <?php endforeach; ?>
     </ul>
     <div class="art-item-cover">
      <div class="art-part-block"><img src="<?php echo $article_des['image'] ?>"></div>
      <div class="art-part-block">
        <div class="art-part--date"><?php echo $article_des['added_date']; ?></div>
        <h1 class="mainH1"><?php echo $article_des['title'] ?></h1>
      </div>
    </div>
    <div class="oText">
     <?php echo $article_des['description'] ?>
   </div>
   <div class="art-inner-soc">
    <div class="art-soc-block">
<!--       <ul>
        <li><a class="vk_soc" href="#vk"><i class="ico_soc"></i><span>0</span></a></li>
        <li><a class="fb_soc" href="#fb"><i class="ico_soc"></i><span>0</span></a></li>
        <li><a class="tw_soc" href="#tw"><i class="ico_soc"></i></a></li>
      </ul> -->
    </div>
    <div class="social-shared-config">
      <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
      <script src="//yastatic.net/share2/share.js"></script>
      <div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,twitter" data-counter=""></div>
    </div>

  </div>
  <div class="similiar-art">
    <div class="clearHeader">
      <div class="t-header">Другие статьи</div>
    </div>
    <div class="art-cover">
      <?php foreach ($other_articles as $item): ?>
        <div class="art-item">
          <a href="<?php echo $item['href']; ?>">
            <div class="art-img" style="background-image:url(<?php echo $item['image']; ?>)"></div>
          </a>
          <div class="art-date"><?php echo $item['added_date']; ?></div><a class="art-href" href="<?php echo $item['href']; ?>">
          <h2 class="art-title"><?php echo $item['title']; ?></h2></a>
          <div class="art-text"><?php echo $item['meta_description']; ?></div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>
</div>
</div>
</div>
<?php echo $footer; ?>
