<div class="our-p-block">
  <div class="container">
	<div class="row">
	  <div class="t-header">НАШИ ПОСТАВЩИКИ		</div>
	  <div class="our-p-slider-cover">
		<div class="our-p-slider owl-carousel">
			<?php foreach ($manufactures as $item): ?>
				 <div class="op-item">
						 <img src="<?php echo $item['image'];?>">
				 </div>
			<?php endforeach; ?>
		</div>
		<span class="op-prev" role="button"></span>
		<span class="op-next" role="button"></span>
	  </div>
	</div>
  </div>
</div>
