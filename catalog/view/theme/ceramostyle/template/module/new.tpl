<div class="news-block sliders-block">
  <div class="container">
	<div class="row">
	  <div class="t-header">Новинки</div>
	  <div class="t-sl-nav"><a href="<?php echo $catalog_link; ?>">посмотреть все</a>
		  <span class="t-sl-prev t-sl-prev-23" role="button"></span>
		  <span class="t-sl-next t-sl-next-23" role="button"></span>
	  </div>
	  <div class="sliders-block-sl--cover owl-carousel">
		 <?php foreach ($products as $product): ?>
			 <div class="sliders-block-sl">
				 <a class="ca-link" href="<?php echo $product['href']; ?>">
					 <div class="sbsl--img" style="background-image:url(<?php echo $product['thumb']; ?>)"></div>
				 </a>
				 <div class="sbsl--text1"><?php echo $product['category_name']; ?></div>
				 <div class="sbsl--text2"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
				 <div class="sbsl--stars" data-ratio="<?php echo $product['rating']; ?>"></div>
				 <div class="sbsl--price">
					 <div class="sl--price">
						 <?php echo $product['price']; ?>
                        <?php if ( $product['cost_type'] == 0): ?>
                             <span>руб/м.кв</span>
                        <?php endif; ?>
                        <?php if ( $product['cost_type'] == 1): ?>
                             <span>руб/шт.</span>
                        <?php endif; ?>
					 </div>
					 <div class="sl--buy mfi" data-url="#storeLink"
					 onclick="cart.add('<?php echo $product['product_id']; ?>');"></div>
				 </div>
			 </div>
		 <?php endforeach; ?>
	  </div>
	</div>
  </div>
</div>
