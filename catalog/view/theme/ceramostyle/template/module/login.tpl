<div class="modal_form">
  <div class="mfi-container mfp-with-anim" id="enterLink">
    <div class="mfi-container__inner">
      <div class="mfp-close">x</div>
      <div class="mfp-container__cover enter-reg-container">
        <div class="po-cover">
          <div class="mfp--title">Вход на сайт</div>
          <form class="bForm" action="/index.php?route=account/login" method="POST" role="form" data-focus="false" data-toggle="validator">
            <div class="form-group">
              <label><span>E-mail</span></label>
              <input class="form-control" type="email" data-minlength="2" required data-error="Некорректный email" name="email">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
              <label><span>Пароль</span></label>
              <input class="form-control" type="password"
              required data-error="Некорректный пароль (не менее 6 символов)" data-minlength="6" name="password">
              <div class="help-block with-errors"></div>
            </div>
            <div class="sub-enter-link">
              <input class="btn-submit" type="submit" value="Войти">
            </div>
            <div class="form-etc"><a class="forg_pswd" href="#">Забыли свой пароль?</a>
              <a class="forg_reg" href="#">Регистрация</a></div>
            </form>
          </div>
          <div class="forg-cover modal_form">
            <div class="mfp--title">Восстановление пароля</div>
            <p>Если вы забыли пароль, введите E-Mail. Контрольная строка для смены пароля, а также ваши регистрационные данные, будут высланы вам по E-Mail.</p>
            <form class="bForm" action="/index.php?route=account/forgotten" method="POST" role="form" data-focus="false" data-toggle="validator">
              <div class="form-group">
                <label><span>E-mail*</span></label>
                <input class="form-control restorePswd" type="email"
                data-minlength="2" required data-error="Некорректный email" name="email">
                <div class="help-block with-errors"></div>
              </div>
              <input class="btn-submit" type="submit" value="выслать">
              <div class="back__link_pswd">назад</div>
            </form>
          </div>

          <div class="reg-cover reg_styled_cover modal_form">
            <div class="mfp--title">Регистрация</div>
            <form class="bForm" action="/index.php?route=account/register"
            method="POST" role="form" data-focus="false" data-toggle="validator">
            <div class="form-group">
              <label><span>Ваше имя *</span></label>
              <input class="form-control" type="text" data-minlength="2"
              required data-error="Не менее 2 символов" name="firstname">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
              <label><span>Телефон *</span></label>
              <input class="form-control" type="tel" data-minlength="2"
              required data-error="Некорректный телефон" name="telephone">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
              <label><span>E-mail *</span></label>
              <input class="form-control" type="email" data-minlength="2"
              required data-error="Некорректный email" name="email">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
              <label> <span class="form-group__helper">Пароль * </span>
                  <input type="password" name="password" data-minlength="6"
                      class="form-control" id="inputPassword2" placeholder="" required>
              </label>
              <div class="help-block with-errors"></div><br>
              <label> <span class="form-group__helper">Подтверждение пароля: **</span>
                  <input type="password" name="confirm"
                      class="form-control" id="inputPasswordConfirm2" data-match="#inputPassword2"
                      data-match-error="Пароли не совпадают" placeholder="" required>
              </label>
              <div class="help-block with-errors"></div>
              <input name="lastname" type="hidden">
              <input name="address_1" type="hidden">
              <input name="city" type="hidden">
              <input name="country_id" type="hidden">
            </div>
            <div class="form-group">
              <div class="fg-checkbox">
                <input class="form-control" type="checkbox" name="agree" id="inp_checkbox_p" checked>
                <label for="inp_checkbox_p"></label>
              </div>
              <div class="fg-checkbox--text">Я даю согласие на обработку персональных данных</div>
            </div>
            <div class="form-after--text">
              <p>* - Поля, обязательные для заполнения</p>
              <p>** - Пароль должен быть не менее 6 символов длиной.</p>
            </div>
            <input class="btn-submit" type="submit" value="Регистрация">
          </form>
          <div class="back__link_reg">назад</div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal_form">
  <div class="mfi-container mfp-with-anim" id="regLink">
    <div class="mfi-container__inner reg_form_pp">
      <div class="mfp-close">x</div>
      <div class="mfp-container__cover reg-container__cover reg_login-form__">
        <div class="mfp--title"> Регистрация</div>
        <form class="bForm" action="/index.php?route=account/register" method="POST" role="form" data-focus="false" data-toggle="validator">
          <div class="form-group">
            <label><span>Ваше имя *</span></label>
            <input class="form-control" type="text" data-minlength="2"
            required data-error="Не менее 2 символов" name="firstname">
            <div class="help-block with-errors"></div>
          </div>
          <div class="form-group">
            <label><span>Телефон *</span></label>
            <input class="form-control" type="tel" data-minlength="2"
            required data-error="Некорректный телефон" name="telephone">
            <div class="help-block with-errors"></div>
          </div>
          <div class="form-group">
            <label><span>E-mail *</span></label>
            <input name="email" class="form-control" type="email" data-error="Некорректный email"
            data-fv-emailaddress="true" required="">
            <div class="help-block with-errors"></div>
          </div>
            <div class="lfg--prety">
              <div class="form-group">
                <div class="form-inline">
                  <div class="form-group">
                    <label><span>Пароль *</span></label>
                    <input type="password" name="password" data-minlength="6"
                    class="form-control" id="inputPassword" data-match-error="Пароли не совпадают" placeholder="" required>
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                     <label><span>Повторите пароль *</span></label>
                    <input type="password" name="confirm"
                    class="form-control" id="inputPasswordConfirm" data-match="#inputPassword"
                    data-match-error="Пароли не совпадают" placeholder="" required>
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
                <div class="help-block with-errors"></div>
                <input name="lastname" type="hidden">
                <input name="address_1" type="hidden">
                <input name="city" type="hidden">
                <input name="country_id" type="hidden">
              </div>
              <div class="form-group">
                <div class="fg-checkbox">
                  <input class="form-control" type="checkbox" name="agree" id="inp_checkbox_p" checked>
                  <label for="inp_checkbox_p"></label>
                </div>
                <div class="fg-checkbox--text">Я даю согласие на обработку персональных данных</div>
              </div>
              <div class="form-after--text">
                <p>* - Поля, обязательные для заполнения</p>
                <p>** - Пароль должен быть не менее 6 символов длиной.</p>
              </div>
              <input class="btn-submit" type="submit" value="Регистрация">
            </form>
        </div>
      </div>
    </div>
    <div class="modal_form">
      <div class="mfi-container mfp-with-anim" id="restorePswdBox">
        <div class="mfi-container__inner">
          <div class="mfp-close">x</div>
          <div class="mfp-container__cover call_back_form restore_pswwd_pp_form">
            <div class="mfp--title">Восстановление пароля</div>
            <form class="bForm" action="/index.php?route=account/forgotten" method="POST" role="form" data-focus="false" data-toggle="validator">
              <div class="form-group">
                <label><span>E-mail*</span></label>
                <input class="form-control restorePswd" type="email"
                data-minlength="2" required data-error="Некорректный email" name="email">
                <div class="help-block with-errors"></div>
              </div>
              <input class="btn-submit" type="submit" value="выслать">
              <div class="back__link_pswd">назад</div>
            </form>
          </div>
        </div>
      </div>
    </div>
