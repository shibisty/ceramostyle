
  <div class="catalog-filter">
	<div class="catf--price">
	  <div class="catf--title">Цена <span>(руб.)</span></div>
	  <div class="catf--inputs"><span>от</span>
		<input type="text"
		class="range_1" id="range_from"
		value="<?php echo ceil(count($prices) == 2 && isset($prices[0]) ? $prices[0] : $price['min']);?>"><span>до</span>
		<input type="text"
		class="range_2" id="range_to"
		value="<?php echo ceil(isset($prices[1]) ? $prices[1] : (count($prices) != 2 && isset($prices[0]) ? $prices[0] : $price['max']));?>">
	  </div>
	  <div class="catf--range"
			data-min="<?php echo ceil($price['min']);?>"
			data-max="<?php echo ceil($price['max']);?>"
			data-from="<?php echo ceil(count($prices) == 2 && isset($prices[0]) ? $prices[0] : $price['min']);?>"
			data-to="<?php echo ceil(isset($prices[1]) ? $prices[1] : (count($prices) != 2 && isset($prices[0]) ? $prices[0] : $price['max']));?>"
		>
		<input type="text" id="range_cat_slider">
	  </div>
	</div>
	<div class="catf--nal">
	  <div class="catf--title">Наличие</div>
	  <a href="<?php echo $count_link_true; ?>">
		  <span
		  class="<?php echo $count ? 'nal-btn-all nal-btn-all-count' : 'nal-btn-single'; ?> btn--filter btn--filter-count"
		  data-value="true">В наличии</span>
	  </a>
	  <a href="<?php echo $count_link_false; ?>">
		  <span
		  class="<?php echo !$count ? 'nal-btn-all nal-btn-all-count' : 'nal-btn-single'; ?> btn--filter btn--filter-count"
		  data-value="false">Все</span>
	  </a>
	</div>
	<div class="catf--show">
	  <div class="catf--title">Показать</div>
	  <a href="<?php echo $show_link_false; ?>">
		  <span
		  class="<?php echo !$show ? 'el-btn-single col-btn-all-show' : 'col-btn-all'; ?> btn--filter btn--filter-show"
		  data-value="false">Элементы</span>
	  </a>
	  <a href="<?php echo $show_link_true; ?>">
		  <span
		  class="<?php echo $show ? 'el-btn-single col-btn-all-show' : 'col-btn-all'; ?> btn--filter btn--filter-show"
		  data-value="true">Коллекции</span>
	  </a>
	</div>
	<div class="catf--last">
		<span class="catf-last--lb btn--filter" id="button-filter">Показать</span>
		<a href="<?php echo $drop; ?>">
			<span class="catf-last--rb btn--filter">Сбросить фильтр</span>
		</a>
	</div>
  </div>
  <div class="catalog-sub-filter">
	  <?php $i = 0; ?>
	  <?php foreach ($filter_groups as $filter_group): ?>
		  <?php if ($i == 0 || ($i % 2) == 0): ?>
			  <div class="cat-sub-item">
		  <?php endif; ?>
			  <div class="sub-item--cover">
				<select name="filter-<?php echo $i; ?>" style="width: 100%"
					data-placeholder="<?php echo $filter_group['name']; ?>">
					<option value="0"><?php echo $filter_group['name']; ?></option>
				  <?php foreach ($filter_group['filter'] as $filter): ?>
					<option value="<?php echo $filter['filter_id']; ?>"
						<?php echo in_array($filter['filter_id'], $filter_category) ? 'selected':''; ?>>
						<?php echo $filter['name']; ?></option>
				  <?php endforeach; ?>
				</select>
			  </div>
		  <?php if ($i != 0 && ($i % 2) != 0 || $filter_group == end($filter_groups)): ?>
			</div>
		  <?php endif; ?>
		  <?php $i++; ?>
	  <?php endforeach; ?>
	<script type="text/javascript"><!--
	$('#button-filter').on('click', function() {
		filter = [];
		prices = [];
		count  = [];
		show   = [];

		$('select[name^=\'filter\'] option:selected').each(function(element) {
			if(this.value !== '0') filter.push(this.value);
		});

		$('input[class^=\'range\']').each(function(element) {
			//if(this.value !== '0') 
                prices.push(this.value);
		});

		location = '<?php echo $action; ?>&filter='
			+ filter.join(',')
			+ '&price=' + prices.join(',')
			+ '&show=' + String($('.col-btn-all-show').attr('data-value'))
			+ '&count=' + String($('.nal-btn-all-count').attr('data-value'));
	});
	//--></script>
  </div>
