<div class="home-slider" id="hs">
	<div class="hs-cover owl-carousel">
		<?php foreach ($banners as $banner) { ?>
			<div class="home--slide">
				<div class="hs-left">
					<img src="/catalog/view/theme/ceramostyle/img/hsLeft.png">
				</div>
				<div class="hs-right">
					<!-- <img src=""> -->
					<div class="hs--img" style="background-image:url(<?php echo $banner['image']; ?>)"></div>
				</div>
				<div class="site_size">
					<div class="hs--content">
						<?php if (isset($banner['title'][0])): ?>
							<div class="hs--text"><?php echo $banner['title'][0]; ?></div>
						<?php endif; ?>
						<?php if (isset($banner['title'][1])): ?>
							<div class="hs--text2"><?php echo $banner['title'][1]; ?></div>
						<?php endif; ?>
						<?php if (isset($banner['title'][2])): ?>
							<div class="hs--label"><?php echo $banner['title'][2]; ?></div>
						<?php endif; ?>
					</div>
						<a class="hs--more" href="<?php echo $banner['link']; ?>">Подробнее</a>
				</div>
			</div>
		<?php } ?>
	</div>
	<div class="hs-nav">
		<div class="site_size">
			<span class="hs-prev t-sl-prev-sl2" role="button"></span>
			<span class="hs-next t-sl-next-sl2" role="button"></span>
		</div>
	</div>
</div>
