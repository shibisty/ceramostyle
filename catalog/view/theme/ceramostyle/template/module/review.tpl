<div class="rev-block">
	<div class="container">
		<div class="row">
			<div class="t-header">отзывы наших клиентов</div>
			<div class="rev-slider-cover">
				<div class="rev-slider owl-carousel">
					<?php foreach ($reviews as $review): ?>
						<div class="rev-item">
							<div class="rev-a"><a href="<?php echo $review['href']; ?>"><?php echo $review['author']; ?></a></div>
							<div class="rev-text"><?php echo $review['text']; ?></div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>
