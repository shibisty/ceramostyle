<?php if (count($banners)): ?>
	<div class="f-soc">
	  <ul>
		  <?php foreach ($banners as $banner): ?>
			  <li><a class="s_vk" style="background-image: url(<?php echo $banner['image']; ?>)" href="<?php echo $banner['link']; ?>"></a></li>
		  <?php endforeach; ?>
	  </ul>
	</div>
<?php endif; ?>
