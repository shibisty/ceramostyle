<?php if (isset($banners[0])): ?>
	<div class="br-block">
	  <div class="container">
		  <?php foreach ($banners as $banner): ?>
			  <div class="br-cover">
				  <a href="<?php echo $banner['link']; ?>">
					  <img src="<?php echo $banner['image']; ?>">
					 <div class="br-content">
						 <?php if (isset($banner['title'][0])): ?>
							 <div class="br-text-left"><?php echo $banner['title'][0]; ?></div>
						 <?php endif; ?>
						 <?php if (isset($banner['title'][1])): ?>
							  <div class="br-text-right"><?php echo $banner['title'][1]; ?> <span>
								   <?php if (isset($banner['title'][2])): ?>
									   <?php echo $banner['title'][2]; ?>
									<?php endif; ?>
								  </span></div>
						 <?php endif; ?>
					 </div>
				  </a>
		  	</div>
		  <?php endforeach; ?>
	  </div>
	</div>
<?php endif; ?>
