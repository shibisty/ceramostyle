<div class="rec-slider">
	<div class="container">
		<div class="row">
			<div class="rec-sl-top">
				<div class="t-header">рекомендуем</div>
				<div class="t-sl-nav">
					<div class="t-sl-prev t-sl-prev-sl3" role="button"></div>
					<div class="t-sl-next t-sl-next-sl3" role="button"></div>
				</div>
			</div>
			<div class="rec-sl-cover owl-carousel">
				<?php foreach ($products as $product): ?>
					<div class="rs--item">
						<div class="rs-item-thumb" style="background-image:url(<?php echo $product['thumb']; ?>)">
						</div>
						<div class="rsi--content">
							<div class="rsi--title"><?php echo $product['name']; ?></div>
							<div class="rsi--text"><?php echo $product['description']; ?></div>
							<a class="rsi--link" href="<?php echo $product['href']; ?>">
								подробнее
							</a>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
