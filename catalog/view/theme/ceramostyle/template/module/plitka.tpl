<div class="bl--cover">
	<div class="container">
		<div class="brands-list">
			<?php foreach ($banners as $banner): ?>
				<div class="brand-item">
					<a href="<?php echo $banner['link']; ?>">
						<img class="bl--img" src="<?php echo $banner['image']; ?>">
						<?php if (isset($banner['title'][0])): ?>
							<div class="bl--title"><?php echo $banner['title'][0]; ?></div>
						<?php endif; ?>
						<?php if (isset($banner['title'][1])): ?>
							<div class="bl--sub"><?php echo $banner['title'][1]; ?></div>
						<?php endif; ?>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
