<div class="featureds-block">
	<div class="container">
		<div class="row">
			<div class="fb-cover">
				<?php $banner_counter = 0; ?>
				<?php foreach ($banners as $banner): ?>
					<?php $banner_counter++; ?>
					<div class="fb-inner">
						<div class="fb-custom-inner">
							<img class="fb-inner--img <?php echo 'fb-inner-num'.$banner_counter ?>" src="<?php echo $banner['image']; ?>">
							<div class="fbi--right">
								<?php if (isset($banner['title'][0])): ?>
									<?php echo $banner['title'][0]; ?>
								<?php endif; ?>
								<div class="fbi--bot">
									<span>
										<?php if (isset($banner['title'][1])): ?>
											<?php echo $banner['title'][1]; ?>
										<?php endif; ?>
									</span>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
