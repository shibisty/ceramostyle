<?php echo $header; ?>
<div class="container">
<!--   <ul class="breadcrumbs">
    <?php #foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php #} ?>
  </ul> -->

  <div id="content" class="<?php echo $class; ?> p404-cover">
    <div class="site_size">
      <div class="oText">
        <h1 class="h404"><?php echo $text_error; ?></h1>
        <div class="logo-404">
          <img src="/image/logo404.png" alt="">
        </div>

        <div class="p404-bot">
          <form class="header-search search-404" data-toggle="validator" role="form" data-focus="false" data-delay="1000" action="/search">
            <div class="form-group" id="search">
              <label>
              <input class="form-control form-search" value="" id="inputSearch2" name="search" type="text" data-minlength="2" placeholder="" required="" data-error="Не менее 2 символов">
                <input class="form-search-sub" type="submit">
              </label>
              <div class="help-block with-errors"></div>
            </div>
          </form>

          <p>А также вы можете перейти на главную или сообщить нам об этой ошибке.</p>
        </div>
<!--         <p>Попробуйте:</p>
        <ul>
          <li><a href="/">Перейти на главную</a></li>
          <li><a href="javascript:history.go(-1)">Вернуться туда, где вы были</a></li>
          <li>Воспользоваться поиском:</li>
        </ul>

        <div class="header-search search-404" data-toggle="validator" role="form" data-focus="false" data-delay="1000">
          <div class="form-group" id="search">
            <label>
              <input class="form-control form-search" value="" id="inputSearch2" name="search" type="text" data-minlength="2" placeholder="Искать товар по артикулу или названию..." required="" data-error="Не менее 2 символов">
              <input class="form-search-sub" type="submit">
            </label>
            <div class="help-block with-errors"></div>
          </div>
        </div> -->


      </div>
    </div>
  </div>

</div>
<?php echo $footer; ?>