<?php echo $header; ?>
<script src="/catalog/view/theme/ceramostyle/css/allbootstrap/bootstrap.min.js"></script>
<link rel="stylesheet" href="/catalog/view/theme/ceramostyle/css/allbootstrap/bootstrap.min.css">
<link rel="stylesheet" href="/catalog/view/theme/ceramostyle/css/fontawesomo/css/font-awesome.min.css">
<div class="container account-edit account-newsletter">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1 class="mainH1"><?php echo $heading_title; ?></h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal newsletter-form">
        <fieldset>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_newsletter; ?></label>
            <div class="col-sm-10 checkboxes-radio">
              <?php if ($newsletter) { ?>
              <label class="radio-inline">
                <input type="radio" name="newsletter" value="1" checked="checked" />
                <?php echo $text_yes; ?> </label>
                <label class="radio-inline">
                  <input type="radio" name="newsletter" value="0" />
                  <?php echo $text_no; ?></label>
                  <?php } else { ?>
                  
                  <div class="form-group radio">
                  <input id="v1" type="radio" name="newsletter" value="1" />
                  <label class="radio-inline" for="v1"> <?php echo $text_yes; ?> </label>
                  </div>

                  <div class="form-group radio">           
                  <input id="v2" type="radio" name="newsletter" value="0" checked="checked" />
                  <label class="radio-inline" for="v2"><?php echo $text_no; ?></label>
                  </div>
                    
                    <?php } ?>
                  </div>
                </div>
              </fieldset>
              <div class="buttons clearfix">
                <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                <div class="pull-right">
                  <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                </div>
              </div>
            </form>
            <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
          </div>
          <?php echo $footer; ?>