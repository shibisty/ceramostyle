$(document).ready(function() {

	'use strict';

	/**
	 * Cached vars
	 */
	var _w = $(window),
		_d = $(document);

	/*
	 * Common functions
	 */

	/**
	 * Main function
	 */
	var main = {
		construct : function() {
			/*
			 * Table responsive wrapper
			 */
			$('.oText').find('table').wrap('<div class="table-responsive"></div>');

			var _mfi = $('.mfi');

			/*
			 * Magnific popup initialize
			 */
			_mfi.each(function(index, el) {
				var _el = $(this);
				_el.magnificPopup({
					removalDelay: 500, //delay removal by X to allow out-animation
					showCloseBtn: false,
					callbacks: {
						beforeOpen: function() {
							var effect = this.st.el.attr('data-effect');
							if (effect) {
								this.st.mainClass = this.st.el.attr('data-effect');				
							}

							else{
								this.st.mainClass = 'mfp-with-fade';
							}

						}
					},
					items: {
						src: $(this).data('url'),
						type: 'inline'
					},
					midClick: true
				});

				_d.on('click', '.mfp-close', function(event) {
					$.magnificPopup.close();
				});				
			});
		}
	};

	/*
	 * Jquery 2+ load
	 */
	_w.on('load', function(event) {
		main.construct();
	});
	
});


