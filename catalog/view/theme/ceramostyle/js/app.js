$(function() {

	// if ($('select[data-name^="filter-"]').length) {
	// 	$(document).on('change', 'select[data-name^="filter-"]', function(event) {
	// 		var url = $(this).find('option:selected').attr('data-url');
	//
	// 		window.location.href = url;
	// 	});
	// }

	if($('select[name="sort"]').length) {
		$(document).on('change', 'select[name="sort"]', function(){
			var href = $(this).find('option:selected').attr('data-href');

			if(href) location.href = href;
		});
	}

	// if ($('select[name="tov_count"]').length) {
	// 	$(document).on('change', 'select[name="tov_count"]', function(){
	// 		var updateOrInsert = false
	//
	// 		$('.tovp-title').each(function(i, el){
	// 			var el = $(el);
	// 			var href = el.find('a').attr('href');
	// 			var id = parseInt($('input[name="product_id"]').val());
	//
	// 			console.log(href);
	// 			if((new RegExp(id + "$", "i")).test(href)) {
	// 				updateOrInsert = true;
	// 			}
	//
	// 		});
	//
	// 		if(!updateOrInsert) {
	// 			cart.add();
	// 		} else {
	// 			cart.update();
	// 		}
	// 	});
	// }
	//
	// if ($('select[name="tov_meters"]').length) {
	// 	$(document).on('change', 'select[name="tov_meters"]', function(){
	// 		var updateOrInsert = false
	//
	// 		$('.tovp-title').each(function(i, el){
	// 			var el = $(el);
	// 			var href = el.find('a').attr('href');
	// 			var id = parseInt($('input[name="product_id"]').val());
	//
	// 			if((new RegExp(id + "$", "i")).test(href)) {
	// 				updateOrInsert = true;
	// 			}
	// 		});
	//
	// 		if(!updateOrInsert) {
	// 			cart.add();
	// 		} else {
	// 			cart.update();
	// 		}
	// 	});
	// }

	$(document).on('click','button[data-url="#storeLink"]',function(){
		var val1 = $('select[name="tov_count"]').find('option:selected').val();
		var val2 = $('select[name="tov_meters"]').find('option:selected').val();
		var id = parseInt($('input[name="product_id"]').val());

		cart.add(id, val1);
	});

});
