$(document).ready(function() {

	'use strict';

	/**
	 * Cached vars
	 */
	var _w = $(window),
		_d = $(document);

	/*
	 * Common functions
	 */
	var mobileWrap = function() {
		var _img  = $('.fb-inner--img');
		if (_img.length) {
			if (_w.innerWidth() < 481) {
				if (_img.parent('.wrap-img-to-responsive').length) return;
				_img.wrap('<div class="wrap-img-to-responsive">');
			}

			else{
				if (_img.parent('.wrap-img-to-responsive').length) _img.unwrap();
			}
		}
	};


	/**
	 * Main function
	 */
	var main = {
		jsEffects: function() {
			var headerEff = function() {
				$('.header-area').addClass('is-visible');
			};

			setTimeout(headerEff, 300);
		},

		Sliders: function() {
			/*
			 * home slider
			 */
			var owl1 = $('#hs .hs-cover');
			owl1.owlCarousel({
				items: 1,
				navigation: true,
				loop: true
			});

			_d.on('click', '.hs-prev', function(event) {
				owl1.trigger('prev.owl.carousel');
			});

			_d.on('click', '.hs-next', function(event) {
				owl1.trigger('next.owl.carousel');
			});

			/*
			 * slider list
			 */
			var owl2 = $('.sliders-block-sl--cover');
			owl2.each(function(index, el) {

				var _this = $(this);
				if ($(this).children('.sliders-block-sl').length > 4) {
					$(this).owlCarousel({
						items: 4,
						loop: true,
						responsiveClass: true,
						responsive: {
							0: {
								items: 1,
								margin: 0
							},
							600: {
								items: 2,
								margin: 50
							},
							1000: {
								items: 3
							},
							1280: {
								items: 4,
								nav: true,
								margin: 40
							}
						}
					});
				}

				else{
					$(this).addClass('vis-slider');
				}


				_d.on('click', '.t-sl-prev-23', function(event) {
					var _this = $(this).closest('.t-sl-nav').next('.sliders-block-sl--cover');
					_this.trigger('prev.owl.carousel');
				});

				_d.on('click', '.t-sl-next-23', function(event) {
					var _this = $(this).closest('.t-sl-nav').next('.sliders-block-sl--cover');
					_this.trigger('next.owl.carousel');
				});

				/*
				 * rev slider
				 */
				var owlRev = $('.rev-slider');
				owlRev.owlCarousel({
					items: 3,
					loop: true,
					responsiveClass: true,
					responsive: {
						0: {
							items: 1
						},
						1024: {
							items: 2,
						},
						768: {
							items: 1,
							center: true,
							singleItem: true
						},
						1280: {
							items: 3,
							margin: 35
						}
					}
				});

				_d.on('click', '.t-sl-prev-23', function(event) {
					var _this = $(this).closest('.t-sl-nav').next('.sliders-block-sl--cover');
					_this.trigger('prev.owl.carousel');
				});

				_d.on('click', '.t-sl-next-23', function(event) {
					var _this = $(this).closest('.t-sl-nav').next('.sliders-block-sl--cover');
					_this.trigger('next.owl.carousel');
				});


				/*
				 * slider 2 items
				 */

				var owl3 = $('.rec-sl-cover');
				owl3.owlCarousel({
					items: 2,
					loop: true,
					nav: true,
					responsiveClass: true,
					responsive: {
						0: {
							items: 1
						},
						1024: {
							items: 1,
							center: true
						},
						1280: {
							items: 2
						}
					}
				});

				_d.on('click', '.rec-slider .t-sl-prev', function(event) {
					owl3.trigger('prev.owl.carousel');
				});

				_d.on('click', '.rec-slider .t-sl-next', function(event) {
					owl3.trigger('next.owl.carousel');
				});


				/*
				 * our post slider
				 */
				var owlPost = $('.our-p-slider');
				owlPost.owlCarousel({
					items: 7,
					loop: true,
					responsiveClass: true,
					responsive: {
						0: {
							items: 2
						},
						1024: {
							items: 2,
							navigation: true,
							center: true,
							singleItem: true
						},
						1280: {
							items: 7,
							center: true,
							singleItem: true
						}
					}
				});

				_d.on('click', '.op-prev', function(event) {
					owlPost.trigger('prev.owl.carousel');
				});

				_d.on('click', '.op-next', function(event) {
					owlPost.trigger('next.owl.carousel');
				});
			});
		},

		RangeSlider: function() {
			var testField = function(field) {
				field.val(field.val().replace(/[^0-9]/g, ''));
			};


			if ($('#range_cat_slider').length) {
				var _min, _max, _from, _to;

				_min = $('.catf--range').data('min');
				_max = $('.catf--range').data('max');
				_from = $('.catf--range').data('from');
				_to = $('.catf--range').data('to');


				var _range_from, _range_to;

				_range_from = $('#range_from');
				_range_to = $('#range_to');


				$('#range_cat_slider').ionRangeSlider({
					type: "double",
					min: _min,
					max: _max,
					from: _from,
					to: _to,
					hide_min_max: true,
					hide_from_to: true,
					onChange: function(data) {
						_range_from.val(data.from);
						_range_to.val(data.to);
					}
				});

				var _slider = $("#range_cat_slider").data("ionRangeSlider");
				_range_from.on('change', function(event) {
					var _this = $(this);
					testField(_this);

					var _val = parseInt(_this.val());
					if (isNaN(_val)) return;
					_slider.update({
						from: _val,
					});
				});

				_range_to.on('change', function(event) {
					var _this = $(this);
					testField(_this);
					var _val = parseInt(_this.val());
					if (isNaN(_val)) return;
					_slider.update({
						to: _val,
					});
				});

			}
		},

		Selects: function() {
			$('.tov-select, .tov-p-select select, .des-reg--cover select, .catalog--text-filter select').select2({
				minimumResultsForSearch: -1
			});


			$('.cat-sub-item select').select2({
				minimumResultsForSearch: -1,
				placeholder: $(this).data('placeholder')
			});
		},

		tovTabs: function() {
			$('.tt-heading li').on('click', function(event) {
				var _th = $(this);
				if (_th.hasClass('active')) return;
				_th.siblings().removeClass('active');
				_th.addClass('active');

				var _href = _th.data('href');

				$('.tt-content').children().removeClass('active');
				$('.tt-content').children(_href).addClass('active');

			});
		},

		tovSlider: function() {
			_d.on('click', '.sm-img', function(event) {
				var _this = $(this);

				_this.siblings().removeClass('active');
				_this.addClass('active');

				var $big_img_elem = $('.tov-sl-img');
				var $old_img = $big_img_elem.attr('style');

				var $new_img = _this.data('big-img');

				/*
				 * replacement
				 */
				$big_img_elem.fadeOut('fast').removeAttr('style').attr('style', 'background-image:url(' + $new_img + ')').fadeIn('fast');
			});
		},

		mobileMenu: function() {
			_d.on('click', '.mob-row .mob-sm-btn', function(event) {
				event.preventDefault();
				var _this = $(this);
				$('.mob-sm-menu-sm').toggleClass('vis');

				_this.toggleClass('active');
			});


			_d.on('click', '.mobile-sm-menu2 > li.has-sub', function(event) {
				var _this = $(this);
				_this.find('.second-menu--lvl2').stop().slideToggle('fast');
				_this.siblings('li').find('.second-menu--lvl2').stop().hide('fast');
			});

		},

		mobileSearch: function() {
			_d.on('click', '.mob-row .mob-search', function(event) {
				$('.mob-form').stop().toggleClass('is-visible');
			});

			_d.on('click', '.tablet-search', function(event) {
				$('.table-search-form').stop().toggleClass('is-visible');
			});

			$('body').on('click', function(event) {
				/*
				 * hide mobile search
				 */
				var et = $(event.target);
				if ($('.mob-form').hasClass('is-visible') || $('.table-search-form').hasClass('is-visible')) {
					if (et.is('.mob-form') || et.is('#inputSearch') || et.is('#inputSearch2') || et.is('.form-search-sub')) {

					} else

					{
						/*
						 * clean errors block
						 */
						$('.help-block.with-errors').empty();
						$('.mob-form').removeClass('is-visible');
						$('.table-search-form').removeClass('is-visible');
					}


				}
			});
		},

		enterForms: function() {
			var _fc = $('.forg-cover');
			var _pc = $('.po-cover');
			var _rc = $('.reg-cover');


			_d.on('click', '.forg_pswd', function(event) {
				event.preventDefault();
				_fc.removeClass('modal_form');
				_pc.addClass('modal_form');
				_rc.addClass('modal_form');
			});

			_d.on('click', '.back__link_reg', function(event) {
				event.preventDefault();
				_fc.addClass('modal_form');
				_rc.addClass('modal_form');
				_pc.removeClass('modal_form');
			});

			_d.on('click', '.back__link_pswd', function(event) {
				event.preventDefault();

				_fc.addClass('modal_form');
				_rc.addClass('modal_form');
				_pc.removeClass('modal_form');
			});


			_d.on('click', '.forg_reg', function(event) {
				event.preventDefault();
				_fc.addClass('modal_form');
				_pc.addClass('modal_form');
				_rc.removeClass('modal_form');
			});
		},

		formClose: function() {
			_d.on('click', '.fg-go-next', function(event) {
				$.magnificPopup.close();
			});
		},

		/*
		 * resize method here
		 */
		mobileMenuInit: function() {
			var resizeMenu = function() {
				var _el = $('#mmOrig').children('li');
				var _mm = $('.menu-mob-single').children('ul');

				_el.each(function(index, el) {
					var _this = $(el);
					if (_this.offset().top > 171) {
						_this.addClass('no-vis').removeClass('vis');
					} else {
						_this.addClass('vis').removeClass('no-vis');
					}
				});

				/*
				 * add items based on class
				 */
				var _tems = [];
				var _items = $('#mmOrig').clone().children('li.no-vis');
				$('.menu-mob-single').children('ul').children('li').remove();
				$('.menu-mob-single').children('ul').children('a').remove();
				for (var i = _items.length - 1; i >= 0; i--) {
					$('.menu-mob-single').children('ul').append(_items[i]);
				}

				var _act = $('.second-menu-act').clone().find('a');
				$('.menu-mob-single').children('ul').append(_act);

			};
			_w.on('resize', function(event) {

				setTimeout(resizeMenu, 500);
				setTimeout(mobileWrap, 300);
			});

			resizeMenu();
		},

		stars: function() {
			if ($('.rateYo_comm').length) {
				$('.rateYo_comm').each(function(index, el) {
					var _ratio = $(this).data('ratio');
					$(this).rateYo({
						starWidth: "13px",
						rating: _ratio,
						readOnly: true
					});
				});
			}
		},

		viewFix: function() {
			var _cvb = $('.catalog-item');
			if (_cvb.length < 3) {
				_cvb.closest('.catalog-items--cover').addClass('itemsL3');
			}

			var _contactTextH = $('.dost-cover').children('.dost-item:first').innerHeight();

			if ($('.frame-cover iframe').length) {
				if (_contactTextH < 200) return;
				$('.frame-cover iframe').removeAttr('height').css('height', _contactTextH);
			}
		},

		formSearchEnter: function() {

		},

		clicks: function() {
			_d.on('click', '.no-touch .mobile-sm-menu2 .has-sub > a', function(event) {
				event.preventDefault();
			});
		}

	};

	/*
	 * Run
	 */

	main.tovTabs();
	main.tovSlider();
	main.jsEffects();
	main.mobileSearch();
	main.enterForms();
	main.formClose();
	main.mobileMenuInit();
	main.stars();
	main.viewFix();
	main.clicks();

	_w.on('load', function(event) {
		main.Sliders();
		main.RangeSlider();
		main.Selects();
		main.mobileMenu();
		mobileWrap();
		// main.socialLikes();
	});



});