<?php
class ModelArticlesComments extends Model {
	public function addReview($articles_id, $data) {
		$this->event->trigger('pre.review.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "comment SET
		author = '" . $this->db->escape($data['name']) . "',
		customer_id = '" . (int)$this->customer->getId() . "',
		articles_id = '" . (int)$articles_id . "',
		parent_id = '" . (isset($data['parent_id']) ? (int)$data['parent_id'] : 0 ). "',
		ip = '".(isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1')."',
		text = '" . $this->db->escape($data['text']) . "',
		date_added = NOW()");

		$comment_id = $this->db->getLastId();

		$this->db->query("INSERT INTO " . DB_PREFIX . "articles_rating
			SET comment_id = '" . (int)$comment_id . "',
			articles_id = '" . (int)$articles_id . "',
			ip = '".(isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1')."',
			rating = '".(int)$data['rating']."',
			date_added = NOW()
		");

		$this->event->trigger('post.review.add', $comment_id);
	}

	public function getReviewsByProductId($articles_id, $start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}

		$query = $this->db->query("
				SELECT
					r.comment_id,
					r.author,
					(SELECT rating FROM " . DB_PREFIX . "articles_rating r1 WHERE r1.comment_id = r.comment_id LIMIT 1) as rating,
					(SELECT COUNT(*) FROM " . DB_PREFIX . "comment_rating rw2 WHERE rw2.comment_id = r.comment_id AND rw2.rating = 1) as rating_pluses,
					(SELECT COUNT(*) FROM " . DB_PREFIX . "comment_rating rw3 WHERE rw3.comment_id = r.comment_id AND rw3.rating < 0) as rating_minuses,
					(SELECT SUM(rw4.rating) FROM " . DB_PREFIX . "comment_rating rw4 WHERE rw4.comment_id = r.comment_id) as rating_review,
					r.text,
					r.answer,
					p.articles_id,
					pd.title,
					p.image,
					r.date_added,
					r.parent_id,
					r.date_modified
				FROM " . DB_PREFIX . "comment r
				LEFT JOIN " . DB_PREFIX . "articles p
				ON (r.articles_id = p.articles_id)
				LEFT JOIN " . DB_PREFIX . "articles_description pd
				ON (p.articles_id = pd.articles_id)
				WHERE p.articles_id = '" . (int)$articles_id . "'
				AND p.added_date <= NOW() AND p.status = '1'
				AND r.status = '1'
				AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
				ORDER BY r.date_added
				DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getReviewsByCommentId($articles_id, $parent_id, $start = 0, $limit = 2) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 2;
		}

		$query = $this->db->query("
				SELECT
					r.comment_id,
					r.author,
					(SELECT rating FROM " . DB_PREFIX . "articles_rating r1 WHERE r1.comment_id = r.comment_id LIMIT 1) as rating,
					(SELECT COUNT(*) FROM " . DB_PREFIX . "comment_rating rw2 WHERE rw2.comment_id = r.comment_id AND rw2.rating = 1) as rating_pluses,
					(SELECT COUNT(*) FROM " . DB_PREFIX . "comment_rating rw3 WHERE rw3.comment_id = r.comment_id AND rw3.rating < 0) as rating_minuses,
					(SELECT SUM(rw4.rating) FROM " . DB_PREFIX . "comment_rating rw4 WHERE rw4.comment_id = r.comment_id) as rating_review,
					r.text,
					r.answer,
					r.articles_id,
					r.date_added,
					r.parent_id,
					r.date_modified
				FROM " . DB_PREFIX . "comment r
				WHERE  r.articles_id = '" . (int)$articles_id . "'
                AND r.parent_id = '" . (int)$parent_id . "'
				AND r.status = '1'
				ORDER BY r.date_added DESC
				LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getReviewsByIp($ip, $articles_id) {

		$query = $this->db->query("
				SELECT
					r.comment_id,
					r.author,
					(SELECT rating FROM " . DB_PREFIX . "articles_rating r1 WHERE r1.comment_id = r.comment_id LIMIT 1) as rating,
					(SELECT COUNT(*) FROM " . DB_PREFIX . "comment_rating rw WHERE rw.comment_id = r.comment_id AND rw.rating = 1) as rating_pluses,
					(SELECT COUNT(*) FROM " . DB_PREFIX . "comment_rating rw2 WHERE rw2.comment_id = r.comment_id AND rw2.rating < 0) as rating_minuses,
					(SELECT SUM(rw3.rating) FROM " . DB_PREFIX . "comment_rating rw3 WHERE rw3.comment_id = r.comment_id) as rating_review,
					r.text,
					r.answer,
					r.date_added,
					r.parent_id,
					r.date_modified
				FROM " . DB_PREFIX . "comment r
				WHERE r.articles_id = '" . (int)$articles_id . "'
				AND r.ip = '" . $ip . "'
				AND r.date_added > '".date("Y-m-d H:i:s", time()-86400)."'
				AND r.status = '0'
				ORDER BY r.date_added
				DESC"
		);

		return $query->rows;
	}

	public function getTotalByIp($articles_id) {
		$query = $this->db->query("SELECT COUNT(*) as count
		FROM " . DB_PREFIX . "comment
		WHERE ip = '".(isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1')."'
		AND articles_id = {$articles_id}
		AND date_added > '".date("Y-m-d H:i:s", time()-60)."'");

		return $query->row;
	}

	public function getTotalByText($text) {
		$query = $this->db->query("SELECT COUNT(*) as count
		FROM " . DB_PREFIX . "comment
		WHERE ip = '".(isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1')."'
		AND text = '{$text}'
		AND date_added > '".date("Y-m-d H:i:s", time()-300)."'");

		return $query->row;
	}

	public function getTotalReviewsByProductId($articles_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total
		FROM " . DB_PREFIX . "comment r
		LEFT JOIN " . DB_PREFIX . "articles p
		ON (r.articles_id = p.articles_id)
		LEFT JOIN " . DB_PREFIX . "articles_description pd
		ON (p.articles_id = pd.articles_id)
		WHERE p.articles_id = '" . (int)$articles_id . "'
		AND p.added_date <= NOW()
		AND p.status = '1'
		AND r.status = '1'
		AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row['total'];
	}

	// public function addReviewRating( $comment_id, $value ) {
	//
	// 	$this->db->query("
	// 	INSERT INTO " . DB_PREFIX . "comment_rating
	// 	SET ip = '".(isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1')."',
	// 	comment_id = {$comment_id},
	// 	rating = {$value}
	// 	");
	//
	// }

	public function deleteMyReview($comment_id) {
		$result = $this->db->query("
			DELETE
			FROM " . DB_PREFIX . "comment
			WHERE comment_id = {$comment_id}
			AND status = '0'
			AND ip = '" . $_SERVER['REMOTE_ADDR'] . "'
			AND date_added > '".date("Y-m-d H:i:s", time()-86400)."'
		");

		if($result) {

			$this->db->query("
				DELETE
				FROM " . DB_PREFIX . "comment_rating
				WHERE comment_id = {$comment_id}
				AND ip = '" . $_SERVER['REMOTE_ADDR'] . "'
			");

			$this->db->query("
				DELETE
				FROM " . DB_PREFIX . "articles_rating
				WHERE comment_id = {$comment_id}
				AND ip = '" . $_SERVER['REMOTE_ADDR'] . "'
			");
		}
	}

	public function addReviewRating( $comment_id, $value ) {
		$check = $this->db->query("
			SELECT COUNT(*) as count
			FROM " . DB_PREFIX . "comment_rating
			WHERE ip = '".(isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1')."'
			AND comment_id = '".(int)$comment_id."'
		");

		if(isset($check->row['count']) && $check->row['count']) {
			$this->db->query("
				UPDATE " . DB_PREFIX . "comment_rating
				SET rating = {$value}
			");
		} else {
			$this->db->query("
				INSERT INTO " . DB_PREFIX . "comment_rating
				SET ip = '".(isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1')."',
				comment_id = {$comment_id},
				rating = {$value},
				date_added = NOW()
			");
		}
	}
}
