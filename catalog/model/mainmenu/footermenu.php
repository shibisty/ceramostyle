<?php
class ModelMainmenuFootermenu  extends Model {

	public function getInformation($menu_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "footermenu WHERE menu_id = '" . (int)$menu_id . "'");

		return $query->row;
	}

	public function getInformationDescriptions($menu_id) {
		$articles_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "footermenu_description WHERE menu_id = '" . (int)$menu_id . "'");

		foreach ($query->rows as $result) {
			$articles_description_data[$result['language_id']] = array(
				'name'      => $result['name'],
				'link'      => $result['link'],
			);
		}

		return $articles_description_data;
	}

	public function getInformations($data = array()) {
		$sql = "SELECT * FROM ".DB_PREFIX."footermenu
		LEFT JOIN ".DB_PREFIX."footermenu_description
		ON ".DB_PREFIX."footermenu.menu_id = ".DB_PREFIX."footermenu_description.menu_id
		WHERE ".DB_PREFIX."footermenu_description.language_id = '{$this->config->get('config_language_id')}'
		GROUP BY ".DB_PREFIX."footermenu.menu_id
		ORDER BY ".DB_PREFIX."footermenu.sort_order ASC
		";
		$query = $this->db->query($sql);
        $result = $query->rows;
        $menu = array();

        foreach ($result as $item) {
            $menu[$item['parent_id']][] = $item;
        }

		return $menu;
	}

	public function getParents($menu_id = 0) {
		$sql =
		"
		SELECT * FROM ".DB_PREFIX."footermenu
		LEFT JOIN ".DB_PREFIX."footermenu_description
		ON ".DB_PREFIX."footermenu.menu_id = ".DB_PREFIX."footermenu_description.menu_id
		WHERE ".DB_PREFIX."footermenu_description.language_id = '{$this->config->get('config_language_id')}'
		AND ".DB_PREFIX."footermenu.parent_id = 0
		AND ".DB_PREFIX."footermenu.menu_id != {$menu_id}
		GROUP BY ".DB_PREFIX."footermenu.menu_id
		ORDER BY ".DB_PREFIX."footermenu.sort_order ASC
		";
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalInformations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "footermenu");
		return $query->row['total'];
	}
}
