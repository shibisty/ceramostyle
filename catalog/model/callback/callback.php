<?php
class ModelCallbackCallback  extends Model {
	public function addInformation($data) {
		$this->event->trigger('pre.admin.callback.edit', $data);

		$this->db->query(
			"INSERT INTO " . DB_PREFIX . "callback
			SET name = '{$data['name']}', phone = '{$data['telephone']}', question = '{$data['question']}'"
		);

		$this->event->trigger('post.admin.callback.edit', $articles_id);
	}

	public function getInformation($callback_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "callback WHERE callback_id = '" . (int)$callback_id . "'");

		return $query->row;
	}

	public function getInformations($data = array()) {
		$sql = "SELECT * FROM ".DB_PREFIX."callback";
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalInformations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "callback");
		return $query->row['total'];
	}
}
