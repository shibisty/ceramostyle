<?php
class ControllerModuleReviews extends Controller {
	public function index() {

		$data = array();

		$this->load->model('module/reviews');

		$data['reviews'] = array();

		$results = $this->model_module_reviews->getReviews();

		foreach ($results as $result) {
			$data['reviews'][] = array(
				'href' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				'thumb' =>  $this->model_tool_image->resize($result['image'], 100, 100),
				'author' => $result['author'],
				'text'	 => $this->limit_words($result['text'], 10),
			);
	  	}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/reviews.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/reviews.tpl', $data);
		}
	}

	private function limit_words($str, $limit = 100, $end_char = NULL) {
		$limit = (int) $limit;
		$end_char = ($end_char === NULL) ? '…' : $end_char;

		if (trim($str) === '')
			return $str;

		if ($limit <= 0)
			return $end_char;

		preg_match('/^\s*+(?:\S++\s*+){1,'.$limit.'}/u', strip_tags($str), $matches);

		return rtrim($matches[0]).((strlen($matches[0]) === strlen($str)) ? '' : $end_char);
	}
}
