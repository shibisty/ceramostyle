<?php
class ControllerModuleFilter extends Controller {
	public function index() {
		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		$category_id = end($parts);

		$this->load->model('catalog/category');

		$category_info = $this->model_catalog_category->getCategory($category_id);

		if ($category_info) {
			$this->load->language('module/filter');

			$data['heading_title'] = $this->language->get('heading_title');

			$data['button_filter'] = $this->language->get('button_filter');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['action'] = str_replace('&amp;', '&', $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url));

			if (isset($this->request->get['filter'])) {
				$data['filter_category'] = explode(',', $this->request->get['filter']);
			} else {
				$data['filter_category'] = array();
			}

			$this->load->model('catalog/product');

			$data['filter_groups'] = array();

			$filter_groups = $this->model_catalog_category->getCategoryFilters($category_id);

			if ($filter_groups) {
				foreach ($filter_groups as $filter_group) {
					$childen_data = array();

					foreach ($filter_group['filter'] as $filter) {
						$filter_data = array(
							'filter_category_id' => $category_id,
							'filter_filter'      => $filter['filter_id']
						);

						$childen_data[] = array(
							'filter_id' => $filter['filter_id'],
							'name'      => $filter['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : '')
						);
					}

					$data['filter_groups'][] = array(
						'filter_group_id' => $filter_group['filter_group_id'],
						'name'            => $filter_group['name'],
						'filter'          => $childen_data
					);
				}

				$this->load->model('catalog/category');
				$data['price'] = $this->model_catalog_category->getMaxMinPrice();
				$data['count'] = !empty($this->request->get['count'])
					&& $this->request->get['count'] === 'true' ? true : false;
				$data['show'] = !empty($this->request->get['show'])
					&& $this->request->get['show'] === 'true' ? true : false;

				$data['count_link_false'] = $this->generateLink(
					'count', 'false', $_SERVER['REQUEST_URI']
				);
				$data['show_link_false'] = $this->generateLink(
					'show', 'false', $_SERVER['REQUEST_URI']
				);

				$data['count_link_true'] = $this->generateLink(
					'count', 'true', $_SERVER['REQUEST_URI']
				);
				$data['show_link_true'] = $this->generateLink(
					'show', 'true', $_SERVER['REQUEST_URI']
				);
				
				$data['drop'] = $this->url->link('product/category', 'path='.$this->request->get['path']);
				$prices = isset($this->request->get['price']) ? explode(',', $this->request->get['price']) : [];
				$data['prices'] = $prices;

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/filter.tpl')) {
					return $this->load->view($this->config->get('config_template') . '/template/module/filter.tpl', $data);
				} else {
					return $this->load->view('default/template/module/filter.tpl', $data);
				}
			}
		}
	}

	private function generateLink( $key = null, $value = NULL, $fakeLink = NULL ) {
		$link = $fakeLink ? $fakeLink : $_SERVER['REQUEST_URI'];
		$uri = explode('?', $link);

		$__get = array();
		if(count($uri) > 1) {
			$arr  = explode('&', $uri[1]);
			foreach($arr AS $_a) {
				$g   = urldecode($_a);
				$g   = strip_tags($g);
				$g   = stripslashes($g);
				$g   = trim($g);
				$___get = explode('=', $g);
				$__get[$___get[0]] = $___get[1];
			}
		}

		if( $value === NULL ) {
			if( !isset($__get[$key]) ) {
				return $link;
			}
			$arr = explode('&', $uri[1]);
			$get = array();
			foreach( $arr AS $el ) {
				$h = explode( '=', $el );
				if( $key != $h[0] ) {
					$get[] = $h[0].'='.$h[1];
				}
			}
			$uri[1] = implode('&', $get);
			if( $uri[1] ) {
				return $uri[0].'?'.$uri[1];
			}
			return $uri[0];
		}

		if( !isset( $__get[$key] ) ) {
			if( isset($uri[1]) ) {
				return $uri[0].'?'.$uri[1].'&'.$key.'='.$value;
			}
			return $uri[0].'?'.$key.'='.$value;
		}
		if( isset($__get[$key]) && $__get[$key] == $value ) {
			return $link;
		}
		$arr = explode('&', $uri[1]);
		$get = array();
		foreach( $arr AS $el ) {
			$h = explode( '=', $el );
			if( $key == $h[0] ) {
				$get[] = $key.'='.$value;
			} else {
				$get[] = $h[0].'='.$h[1];
			}
		}

		$uri[1] = implode('&', $get);
		return $uri[0].'?'.$uri[1];
	}
}
