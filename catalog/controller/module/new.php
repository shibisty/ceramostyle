<?php
class ControllerModuleNew extends Controller {
	public function index() {
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$data['products'] = array();

		$products = $this->model_catalog_product->getLatestProducts(20);
		$productsIds = [];

		foreach ($products as $item) {
			$productsIds[] = $item['product_id'];
		}

		foreach ($productsIds as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);

			if ($product_info) {
				if ($product_info['image']) {
					$image = '/image/'.$product_info['image'];
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', 300, 300);
				}

				if (($this->config->get('config_customer_price')
				&& $this->customer->isLogged())
				|| !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $product_info['product_id'],
					'thumb'       => $image,
					'name'        => $product_info['name'],
					'category_name'        => $product_info['category_name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => str_replace(' р.','',$price),
					'cost_type'       => $product_info['cost_type'],
					'special'     => str_replace(' р.','',$special),
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
				);
			}
		}

		$data['catalog_link'] = $this->url->link('product/category', '');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/new.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/new.tpl', $data);
		} else {
			return '';
		}
	}
}
