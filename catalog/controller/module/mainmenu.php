<?php
class ControllerModuleMainmmenu extends Controller {

	public function index() {
		$data = array();

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/mainmenu.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/mainmenu.tpl', $data);
		} else {
			return '';
		}
	}
}
