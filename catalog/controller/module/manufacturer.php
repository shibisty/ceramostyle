<?php
class ControllerModuleManufacturer extends Controller {
	public function index($setting) {
		$data = array();

		$this->load->model('catalog/manufacturer');
		$this->load->model('tool/image');

		$manufactures = $this->model_catalog_manufacturer->getManufacturers(array(
			'start' => 0,
			'limit' => 20
		));

		//print_r($manufactures); die;

		foreach ($manufactures as $m) {
			$data['manufactures'][] = array(
				'title' => $m['name'],
				'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $m['manufacturer_id']),
				'image' => $this->model_tool_image->resize($m['image'], 130, 42)
			);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturer.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/manufacturer.tpl', $data);
		} else {
			return '';
		}
	}
}
