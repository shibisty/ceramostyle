<?php
class ControllerModuleBanner1 extends Controller {

	public function index() {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.transitions.css');
		$this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner(10);

		foreach ($results as $result) {
			//if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => explode('|', $result['title']),
					'link'  => $result['link'],
					'image' => '/image/'.$result['image']
				);
			//}
		}

		$data['module'] = $module++;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/banner1.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/banner1.tpl', $data);
		} else {
			return '';
		}
	}
}
