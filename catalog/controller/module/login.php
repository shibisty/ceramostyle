<?php
class ControllerModuleLogin extends Controller {

	public function index() {
        $data = array();
        
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/login.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/login.tpl', $data);
		} else {
			return '';
		}
	}

}
