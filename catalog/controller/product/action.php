<?php
class ControllerProductAction extends Controller {
	private $error = array();

	public function index() {
		$data = array();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Акции',
			'href' => $this->url->link('product/action', '')
		);

		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$this->document->setTitle(isset($meta['meta_title']) ? $meta['meta_title'] : 'Акции');
		$this->document->setDescription(isset($meta['meta_description']) ? $meta['meta_description'] : 'Акции');
		$this->document->setKeywords(isset($meta['meta_keyword']) ? $meta['meta_keyword'] : 'Акции');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner(13);

		foreach ($results as $result) {
			//if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => explode('|', $result['title']),
					'link'  => $result['link'],
					'thumb' => 'image/'.$result['image']
				);
			//}
		}

		//$data['module'] = $module++;

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/catalog/action.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/catalog/action.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/catalog/action.tpl', $data));
		}
	}
}
