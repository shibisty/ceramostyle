<?php
class ControllerArticlesArticles  extends Controller {

	public function index() {
		$this->load->model('articles/articles');
		$this->load->model('tool/image');
		//$this->load->model('seo/seo');

		//$meta = $this->model_seo_seo->getSeo('articles_l');

		$this->document->setTitle(isset($meta['meta_title']) ? $meta['meta_title'] : 'Статьи');
		$this->document->setDescription(isset($meta['meta_description']) ? $meta['meta_description'] : 'Статьи');
		$this->document->setKeywords(isset($meta['meta_keyword']) ? $meta['meta_keyword'] : 'Статьи');

		$page = isset($this->request->get['page']) ? $this->request->get['page'] : 1;
		$limit = isset($this->request->get['limit']) ? $this->request->get['limit'] : 2;
		$offset = ($page - 1) * $limit;

		$data = array();

		$this->load->language('articles/articles');
		$data['text_annotation'] = $this->language->get('text_annotation');
		$data['text_keywords'] = $this->language->get('text_keywords');
		$data['text_prices'] = $this->language->get('text_prices');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_author'] = $this->language->get('text_author');
		$data['text_show'] = $this->language->get('text_show');
		$data['text_publication_year'] = $this->language->get('text_publication_year');

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		//$data['h1'] = $meta['meta_h1'];

		$data['get_sort'] = isset($this->request->get['sort']) ? $this->request->get['sort'] : null;
		$data['get_year'] = isset($this->request->get['year']) ? $this->request->get['year'] : null;

		$items = $this->model_articles_articles->getInformations(array(
			'sort' => $data['get_sort'],
			'year' => $data['get_year'],
			'limit' => $limit,
			'start' => $offset,
		));

		$total = $this->model_articles_articles->getTotalInformations();
		$articles = [];

		$Month_r = array(
		"01" => "января",
		"02" => "февраля",
		"03" => "марта",
		"04" => "апреля",
		"05" => "майа",
		"06" => "июня",
		"07" => "июля",
		"08" => "августа",
		"09" => "сентября",
		"10" => "октября",
		"11" => "ноября",
		"12" => "декабря");

		foreach ($items as $item) {
			$moth = $Month_r[date('m', strtotime($item['added_date']))];

			$articles[] = array(
				'meta_description' => $item['meta_description'],
				'meta_keyword' => $item['meta_keyword'],
				'title' => $item['title'],
				'added_date' => date("d $moth Y", strtotime($item['added_date'])),
				'link' => $this->url->link('articles/articles/item', 'article_id='.$item['articles_id'], 'SSL'),
				'image' => $this->model_tool_image->resize($item['image'], 200, 200)
			);
		}

		$data['articles'] = $articles;
		$data['breadcrumbs'] = [
			array(
				'text' => 'Главная',
				'href' => $this->url->link('common/home', '', 'SSL'),
			),
			array(
				'text' => 'Статьи',
				'href' => $this->url->link('articles/articles', '', 'SSL')
			)
		];

		$pagination = new Pagination();
		$pagination->total = $total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('articles/articles', 'page={page}', 'SSL');

		$data['limits'] = array(
			[
				'url' => $this->generateLink('limit', 9),
				'limit' => 9,
				'name' => 9
			],
			[
				'url' => $this->generateLink('limit', 18),
				'limit' => 18,
				'name' => 18
			],
			[
				'url' => $this->generateLink('limit', 36),
				'limit' => 36,
				'name' => 36
			],
			[
				'url' => $this->generateLink('limit', $total),
				'limit' => $total,
				'name' => 'Все'
			],
		);

		$years = $this->model_articles_articles->getYears();

		if(count($years)) {
			foreach ($years as $year) {
				$data['years'][] = array(
					'url' => $this->generateLink('year', $year['year']),
					'name' => $year['year']
				);
			}
		}

		$data['sort'] = array(
			[
				'name' => $this->language->get('text_publish_date'),
				'url' => $this->generateLink('sort', 'date'),
				'value' => 'date'
			],
			[
				'name' => $this->language->get('text_publish_date'),
				'url' => $this->generateLink('sort', 'date_desc'),
				'value' => 'date_desc'
			],
			[
				'name' => $this->language->get('text_viewsw'),
				'url' => $this->generateLink('sort', 'views'),
				'value' => 'views'
			],
			[
				'name' => $this->language->get('text_socials_active'),
				'url' => $this->generateLink('sort', 'social'),
				'value' => 'social'
			],
			[
				'name' => $this->language->get('text_authors'),
				'url' => $this->generateLink('sort', 'author'),
				'value' => 'author'
			]
		);

		$data['pagination'] = $pagination->render();
		//$_GET['slider'] = 'articles';
		//$data['slider'] = $this->load->controller('common/slider');

		return $this->response->setOutput(
		$this->load->view($this->config->get('config_template') . '/template/articles/articles.tpl', $data));
	}

	private function generateLink( $key = null, $value = NULL, $fakeLink = NULL ) {
		$link = $fakeLink ? $fakeLink : $_SERVER['REQUEST_URI'];
		$uri = explode('?', $link);

		$__get = array();
		if(count($uri) > 1) {
			$arr  = explode('&', $uri[1]);
			foreach($arr AS $_a) {
				$g   = urldecode($_a);
				$g   = strip_tags($g);
				$g   = stripslashes($g);
				$g   = trim($g);
				$___get = explode('=', $g);
				$__get[$___get[0]] = $___get[1];
			}
		}

		if( $value === NULL ) {
			if( !isset($__get[$key]) ) {
				return $link;
			}
			$arr = explode('&', $uri[1]);
			$get = array();
			foreach( $arr AS $el ) {
				$h = explode( '=', $el );
				if( $key != $h[0] ) {
					$get[] = $h[0].'='.$h[1];
				}
			}
			$uri[1] = implode('&', $get);
			if( $uri[1] ) {
				return $uri[0].'?'.$uri[1];
			}
			return $uri[0];
		}

		if( !isset( $__get[$key] ) ) {
			if( isset($uri[1]) ) {
				return $uri[0].'?'.$uri[1].'&'.$key.'='.$value;
			}
			return $uri[0].'?'.$key.'='.$value;
		}
		if( isset($__get[$key]) && $__get[$key] == $value ) {
			return $link;
		}
		$arr = explode('&', $uri[1]);
		$get = array();
		foreach( $arr AS $el ) {
			$h = explode( '=', $el );
			if( $key == $h[0] ) {
				$get[] = $key.'='.$value;
			} else {
				$get[] = $h[0].'='.$h[1];
			}
		}

		$uri[1] = implode('&', $get);
		return $uri[0].'?'.$uri[1];
	}

	public function item() {
		$id = isset($this->request->get['article_id']) ? $this->request->get['article_id'] : 0;

		if(!$id) return ;

		$this->load->model('articles/articles');
		$this->load->model('tool/image');

		$data = array();

		$this->load->language('articles/articles');
		$data['text_annotation'] = $this->language->get('text_annotation');
		$data['text_keywords'] = $this->language->get('text_keywords');
		$data['text_prices'] = $this->language->get('text_prices');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_author'] = $this->language->get('text_author');
		$data['text_show'] = $this->language->get('text_show');
		$data['text_publication_year'] = $this->language->get('text_publication_year');
		$data['text_re'] = $this->language->get('text_re');
		$data['text_vk'] = $this->language->get('text_vk');
		$data['text_fb'] = $this->language->get('text_fb');
		$data['text_payde_rew'] = $this->language->get('text_payde_rew');
		$data['text_myname'] = $this->language->get('text_myname');
		$data['text_rating2'] = $this->language->get('text_rating2');
		$data['text_myreview'] = $this->language->get('text_myreview');

		$data['text_main_page']= $this->language->get('text_main_page');
		$data['text_not_found']= $this->language->get('text_not_found');
		$data['text_404']= $this->language->get('text_404');
		$data['text_try']= $this->language->get('text_try');
		$data['text_go_to']= $this->language->get('text_go_to');
		$data['text_to_main_page']= $this->language->get('text_to_main_page');
		$data['text_goback']= $this->language->get('text_goback');
		$data['text_way']= $this->language->get('text_way');
		$data['text_f']= $this->language->get('text_f');

		$description = $this->model_articles_articles->getInformationDescription($id);

		$Month_r = array(
		"01" => "января",
		"02" => "февраля",
		"03" => "марта",
		"04" => "апреля",
		"05" => "майа",
		"06" => "июня",
		"07" => "июля",
		"08" => "августа",
		"09" => "сентября",
		"10" => "октября",
		"11" => "ноября",
		"12" => "декабря");


		if($description) {

			$data['breadcrumbs'] = [
				array(
					'text' => 'Главная',
					'href' => $this->url->link('common/home', '', 'SSL'),
				),
				array(
					'text' => 'Статьи',
					'href' => $this->url->link('articles/articles', '', 'SSL')
				),
				array(
					'text' => $description['title'],
					'href' => $this->url->link('articles/articles/item', 'article_id='.$description['articles_id'], 'SSL')
				)
			];

			$this->document->setTitle($description['meta_title']);
			$this->document->setDescription($description['meta_description']);
			$this->document->setKeywords($description['meta_keyword']);

			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			$data['article'] = $this->model_articles_articles->getInformation($id);
			$other_articles = $this->model_articles_articles->getInformationDescriptionsNoId($id);

			foreach ($other_articles as $value) {
				$moth = $Month_r[date('m', strtotime($value['added_date']))];

				$data['other_articles'][] = array(
					'title' => $value['title'],
					'meta_description' => $value['meta_description'],
					'added_date' => date("d $moth Y", strtotime($value['added_date'])),
					'image' => $this->model_tool_image->resize($value['image'], 400, 400),
					'href' => $this->url->link('articles/articles/item', 'article_id='.$value['articles_id']),
				);
			}

			$this->model_articles_articles->incViews($id);
			$moth = $Month_r[date('m', strtotime($description['added_date']))];

			$data['article_des'] = array(
				'description' => htmlspecialchars_decode($description['description']),
				'title' => $description['title'],
				'meta_description' => $description['meta_description'],
				'meta_keyword' => $description['meta_keyword'],
				'articles_id' => $description['articles_id'],
				'added_date' => date("d $moth Y", strtotime($description['added_date'])),
				'image' => $this->model_tool_image->resize($description['image'], 400, 400)
			);

			return $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/articles/item.tpl', $data));

		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('articles/articles',  'article_id=' . $id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}
}
