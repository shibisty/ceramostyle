<?php
class ControllerCallbackCallback  extends Model {

	public function add() {
		$this->load->model('callback/callback');
		$post = $this->request->post;

		$this->load->language('common/bactosfera');

		if(count($post)) {
			$name = isset($post['name']) ? trim(htmlspecialchars(strip_tags($post['name']))) : null;
			if(!$name) {
				$this->response([ 'message' => 'Вы не ввели имя.', 'error' => 1 ]);
			}

			$telephone = isset($post['telephone']) ? trim(htmlspecialchars(strip_tags($post['telephone']))) : null;
			if(!$telephone || !preg_match('/(?:\d){9,11}/', $telephone)) {
				$this->response([ 'message' => 'Неверный формат телефона.', 'error' => 1 ]);
			}

			$this->model_callback_callback->addInformation([
				'name' => $name,
				'telephone' => $telephone,
				'question' => '',
			]);

			$this->response([ 'message' => 'Ожидайте, вскоре мы вам перезвоним.', 'error' => 0 ]);
		}
	}

	private function response($data = array()) {

		if(isset($data['error'])) {
			$_SESSION['message_status'] = ($data['error'] == 1 ? 'error' : 'success');
		}

		if(isset($data['message'])) {
			$_SESSION['message'] = $data['message'];
		}

		$this->response->redirect(
			isset($data['url'])
				? $data['url'] 
				: (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/')
		);
	}

}
