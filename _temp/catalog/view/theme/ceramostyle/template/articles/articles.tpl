<?php echo $header; ?>
<div class="liders-b sliders-block">
  <div class="container">
	<div class="row">
	  <div class="site_size">
          <ul class="breadcrumbs">
              <?php foreach ($breadcrumbs as $breadcrumb): ?>
                  <li>
                      <a <?php echo end($breadcrumbs) == $breadcrumb ? 'class="active"' : ''; ?> href="<?php echo $breadcrumb['href']; ?>">
                          <?php echo $breadcrumb['text']; ?>
                      </a>
                  </li>
              <?php endforeach; ?>
          </ul>
		<h1 class="mainH1">Статьи</h1>
		<div class="art-cover">
		   <?php foreach ($articles as $article): ?>
			   <div class="art-item">
                   <a href="<?php echo $article['link']; ?>">
                       <div class="art-img" style="background-image:url(<?php echo $article['image']; ?>)"></div>
                   </a>
     			<div class="art-date"><?php echo $article['added_date']; ?></div><a class="art-href" href="<?php echo $article['link']; ?>">
     			  <h2 class="art-title"><?php echo $article['title']; ?></h2></a>
     			<div class="art-text"><?php echo $article['meta_description']; ?></div>
     		  </div>
		   <?php endforeach; ?>
		</div>
		<div class="catalog-pagin">
			<?php echo $pagination; ?>
		  <!-- <div class="pagination">
			<ul>
			  <li class="active"><span>1</span></li>
			  <li><a href="#2">2</a></li>
			  <li><a href="#3">3</a></li>
			</ul>
		  </div> -->
		</div>
	  </div>
	</div>
  </div>
</div>
<?php echo $footer; ?>
