<?php
class ModelArticlesArticles  extends Model {

	public function getInformation($articles_id) {
		$query = $this->db->query("SELECT DISTINCT *,
			(
				SELECT keyword
				FROM " . DB_PREFIX . "url_alias
				WHERE query = 'articles_id=" . (int)$articles_id . "') AS keyword
			FROM " . DB_PREFIX . "articles as i WHERE i.articles_id = '" . (int)$articles_id . "'");
			//print_r($query->row); die;
		return $query->row;
	}

	public function getYears() {
		$query = $this->db->query("SELECT DISTINCT extract(year from added_date) as year FROM " . DB_PREFIX . "articles");

		return $query->rows;
	}

	public function getInformations($data = array()) {
		if ($data) {
			$sql = "SELECT
				i.* , id.* ";


			$sql .= "FROM " . DB_PREFIX . "articles i
                LEFT JOIN " . DB_PREFIX . "articles_description id
                ON (i.articles_id = id.articles_id)
                WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ";

				if (isset($data['year'])) {
					$sql .= "AND YEAR(added_date) = {$data['year']} ";
				}

				if(isset($data['sort'])) {

					if($data['sort'] === 'date') {
						$sql .= "ORDER BY i.added_date DESC ";
					}

					if($data['sort'] === 'date_desc') {
						$sql .= "ORDER BY i.added_date ASC ";
					}

				} else {
					$sql .= "ORDER BY i.added_date DESC ";
				}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$articles_data = $this->cache->get('articles.' . (int)$this->config->get('config_language_id'));

			if (!$articles_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "articles i
				LEFT JOIN " . DB_PREFIX . "articles_description id
				ON (i.articles_id = id.articles_id)
				WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'
				AND i.status = 1
				ORDER BY id.title");

				$articles_data = $query->rows;

				$this->cache->set('articles.' . (int)$this->config->get('config_language_id'), $articles_data);
			}

			return $articles_data;
		}
	}

	public function getInformationDescriptions($articles_id) {
		$articles_description_data = array();

		$query = $this->db->query("SELECT *
			FROM " . DB_PREFIX . "articles_description WHERE articles_id = '" . (int)$articles_id . "' AND status = 1");

		foreach ($query->rows as $result) {
			$articles_description_data[$result['language_id']] = array(
				'title'            => $result['title'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
				'meta_h1'          => $result['meta_h1'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword']
			);
		}

		return $articles_description_data;
	}

	public function getInformationDescriptionsNoId($articles_id, $data = array()) {
			$sql = "SELECT
				i.* , id.* ";


			$sql .= "FROM " . DB_PREFIX . "articles i
                LEFT JOIN " . DB_PREFIX . "articles_description id
                ON (i.articles_id = id.articles_id)
                WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'
				AND i.articles_id != ".((int)$articles_id)." ";

				if (isset($data['year'])) {
					$sql .= "AND YEAR(added_date) = {$data['year']} ";
				}

				if(isset($data['sort'])) {

					if($data['sort'] === 'date') {
						$sql .= "ORDER BY i.added_date DESC ";
					}

					if($data['sort'] === 'date_desc') {
						$sql .= "ORDER BY i.added_date ASC ";
					}

				} else {
					$sql .= "ORDER BY i.added_date DESC ";
				}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
	}

	public function getInformationDescription($articles_id) {
		$query = $this->db->query("SELECT *
			FROM " . DB_PREFIX . "articles_description d
			INNER JOIN " . DB_PREFIX . "articles a
			ON a.articles_id = d.articles_id
			WHERE d.articles_id = '" . (int)$articles_id . "'
			AND a.status = 1");

		return $query->row;
	}

	public function getAuthor($author_id) {
		$query = $this->db->query("SELECT username FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$author_id . "'");

		return $query->row;
	}

	public function getTotalInformations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "articles WHERE status = 1");
		return $query->row['total'];
	}

	public function incViews($id) {
		$query = $this->db->query("UPDATE " . DB_PREFIX . "articles SET views = views + 1 WHERE articles_id = {$id}");
	}

	public function addProductSocials($data) {
		if(!isset($data['articles_id'])) return;

		$articles_id = $data['articles_id'];

		$check = $this->db->query("
			SELECT COUNT(*) as count
			FROM " . DB_PREFIX . "articles_social
			WHERE articles_id = '".(int)$articles_id."'
		");

		if(isset($check->row['count']) && $check->row['count']) {
			$this->db->query("
				UPDATE " . DB_PREFIX . "articles_social
				SET vk_likes = '".$data['vk_likes']."',
					vk_comments = '".$data['vk_comments_count']."',
					fb_likes = '".$data['fb_likes']."',
					fb_comments = '".$data['fb_comments_count']."',
					gp_likes = '".$data['gp_likes']."',
					all_soc = '".$data['all']."'
				WHERE articles_id = '".(int)$articles_id."'
			");
		} else {
			$this->db->query("
				INSERT INTO " . DB_PREFIX . "articles_social
				SET vk_likes = '".$data['vk_likes']."',
					vk_comments = '".$data['vk_comments_count']."',
					fb_likes = '".$data['fb_likes']."',
					fb_comments = '".$data['fb_comments_count']."',
					gp_likes = '".$data['gp_likes']."',
					all_soc = '".$data['all']."',
					articles_id = '".(int)$articles_id."'
			");
		}

	}
}
