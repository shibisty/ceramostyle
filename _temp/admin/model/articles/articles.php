<?php
class ModelArticlesArticles  extends Model {
	public function addInformation($data) {
		$this->event->trigger('pre.admin.articles.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "articles
		SET
		bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "',
		author_id = '".(int)$this->session->data['user_id']."',
		image = '".$data['image']."',
		author = '".$data['author']."',
		added_date = NOW(),
		status = '" . (int)$data['status'] . "'");

		$articles_id = $this->db->getLastId();

		foreach ($data['articles_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "articles_description SET articles_id = '" . (int)$articles_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		// if (isset($data['articles_store'])) {
		// 	foreach ($data['articles_store'] as $store_id) {
		// 		$this->db->query("INSERT INTO " . DB_PREFIX . "articles_to_store SET articles_id = '" . (int)$articles_id . "', store_id = '" . (int)$store_id . "'");
		// 	}
		// }

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'article_id=" . (int)$articles_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('articles');

		$this->event->trigger('post.admin.articles.add', $articles_id);

		return $articles_id;
	}

	public function editInformation($articles_id, $data) {
		$this->event->trigger('pre.admin.articles.edit', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "articles
		SET bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "',
		image = '".$data['image']."',
		author = '".$data['author']."',
		status = '" . (int)$data['status'] . "' WHERE articles_id = '" . (int)$articles_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "articles_description WHERE articles_id = '" . (int)$articles_id . "'");

		foreach ($data['articles_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "articles_description SET articles_id = '" . (int)$articles_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		// $this->db->query("DELETE FROM " . DB_PREFIX . "articles_to_store WHERE articles_id = '" . (int)$articles_id . "'");
		//
		// if (isset($data['articles_store'])) {
		// 	foreach ($data['articles_store'] as $store_id) {
		// 		$this->db->query("INSERT INTO " . DB_PREFIX . "articles_to_store SET articles_id = '" . (int)$articles_id . "', store_id = '" . (int)$store_id . "'");
		// 	}
		// }

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'article_id=" . (int)$articles_id . "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'article_id=" . (int)$articles_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('articles');

		$this->event->trigger('post.admin.articles.edit', $articles_id);
	}

	public function deleteInformation($articles_id) {
		$this->event->trigger('pre.admin.articles.delete', $articles_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "articles WHERE articles_id = '" . (int)$articles_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "articles_description WHERE articles_id = '" . (int)$articles_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "articles_to_store WHERE articles_id = '" . (int)$articles_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'articles_id=" . (int)$articles_id . "'");

		$this->cache->delete('articles');

		$this->event->trigger('post.admin.articles.delete', $articles_id);
	}

	public function getInformation($articles_id) {
		$query = $this->db->query(
			"SELECT DISTINCT *,
				(
					SELECT keyword
					FROM " . DB_PREFIX . "url_alias
					WHERE query = 'article_id=" . (int)$articles_id . "'
				) AS keyword
			FROM " . DB_PREFIX . "articles
			WHERE articles_id = '" . (int)$articles_id . "'"
		);

		return $query->row;
	}

	public function getInformations($data = array()) {
		if ($data) {
			$sql = "SELECT *
				FROM " . DB_PREFIX . "articles i
				LEFT JOIN " . DB_PREFIX . "articles_description id
				ON (i.articles_id = id.articles_id)
				WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				'id.title',
				'i.sort_order'
			);

			if (isset($data['title'])) {
				$sql .= " AND id.title LIKE '%".$data['title']."%'";
			}

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY id.title";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$articles_data = $this->cache->get('articles.' . (int)$this->config->get('config_language_id'));

			if (!$articles_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "articles i LEFT JOIN " . DB_PREFIX . "articles_description id ON (i.articles_id = id.articles_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title");

				$articles_data = $query->rows;

				$this->cache->set('articles.' . (int)$this->config->get('config_language_id'), $articles_data);
			}

			return $articles_data;
		}
	}

	public function getInformationDescriptions($articles_id) {
		$articles_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "articles_description WHERE articles_id = '" . (int)$articles_id . "'");

		foreach ($query->rows as $result) {
			$articles_description_data[$result['language_id']] = array(
				'title'            => $result['title'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
				'meta_h1'          => $result['meta_h1'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword']
			);
		}

		return $articles_description_data;
	}

	public function getInformationDescription($articles_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "articles_description WHERE articles_id = '" . (int)$articles_id . "'");

		return $query->row;
	}

	public function getInformationStores($articles_id) {
		$articles_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "articles_to_store WHERE articles_id = '" . (int)$articles_id . "'");

		foreach ($query->rows as $result) {
			$articles_store_data[] = $result['store_id'];
		}

		return $articles_store_data;
	}

	public function getTotalInformations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "articles");
		return $query->row['total'];
	}
}
