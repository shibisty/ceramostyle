<?php
class ModelArticlesComments extends Model {

	public function editReview($comment_id, $data) {
		$this->event->trigger('pre.admin.comment.edit', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "comment
		SET author = '" . $this->db->escape($data['author']) . "',
		articles_id = '" . (int)$data['articles_id'] . "',
		text = '" . $this->db->escape(strip_tags($data['text'])) . "',
		answer = '" . $this->db->escape(strip_tags($data['answer'])) . "',
		status = '" . (int)$data['status'] . "',
		date_modified = NOW() WHERE comment_id = '" . (int)$comment_id . "'");

		// if(isset($data['rating'])) {
		// 	$this->db->query("UPDATE " . DB_PREFIX . "articles_rating
		// 		SET
		// 		rating = '".(int)$data['rating']."'
		// 		WHERE comment_id = '" . (int)$comment_id . "'
		// 	");
		// }

		if(isset($data['rating']) && $this->db->query("SELECT * FROM " . DB_PREFIX . "articles_rating WHERE comment_id = '" . (int)$comment_id . "' LIMIT 1")->row) {
			$this->db->query("UPDATE " . DB_PREFIX . "articles_rating
				SET
				rating = '".(int)$data['rating']."'
				WHERE comment_id = '" . (int)$comment_id . "'
			");
		} else if(isset($data['rating'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "articles_rating
				SET
				rating = '".(int)$data['rating']."',
				comment_id = '" . (int)$review_id . "',
				articles_id = '" . (int)$data['articles_id'] . "',
				date_added = NOW()
			");
		}

		$this->cache->delete('product');

		$this->event->trigger('post.admin.comment.edit', $comment_id);
	}

	public function deleteReview($comment_id) {
		$this->event->trigger('pre.admin.comment.delete', $comment_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "comment WHERE comment_id = '" . (int)$comment_id . "'");

		$this->cache->delete('product');

		$this->event->trigger('post.admin.comment.delete', $comment_id);
	}

	public function getReview($comment_id) {
		$query = $this->db->query("
			SELECT DISTINCT *,
			 (
				 SELECT pd.title FROM " . DB_PREFIX . "articles_description pd
			 	WHERE pd.articles_id = r.articles_id
			 	AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
			 ) AS product,
			 (
			 	SELECT rating FROM " . DB_PREFIX . "articles_rating r1 WHERE r1.comment_id = r.comment_id LIMIT 1
			 ) as rating
			 FROM " . DB_PREFIX . "comment r WHERE r.comment_id = '" . (int)$comment_id . "'");

		return $query->row;
	}

	public function getReviews($data = array()) {
		$sql = "SELECT r.comment_id, r.articles_id,
			pd.title,
			r.author,
			(SELECT rating FROM " . DB_PREFIX . "articles_rating r1 WHERE r1.comment_id = r.comment_id LIMIT 1) as rating,
			r.status,
			r.date_added
			FROM " . DB_PREFIX . "comment r LEFT JOIN " . DB_PREFIX . "articles_description pd
			ON (r.articles_id = pd.articles_id)
			WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.title LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}

		if (!empty($data['filter_author'])) {
			$sql .= " AND r.author LIKE '" . $this->db->escape($data['filter_author']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$sort_data = array(
			'pd.title',
			'r.author',
			'r1.rating',
			'r.status',
			'r.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY r.date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalReviews($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "comment r LEFT JOIN " . DB_PREFIX . "articles_description pd ON (r.articles_id = pd.articles_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.title LIKE '" . $this->db->escape($data['filter_product']) . "%'";
		}

		if (!empty($data['filter_author'])) {
			$sql .= " AND r.author LIKE '" . $this->db->escape($data['filter_author']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalReviewsAwaitingApproval() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "comment WHERE status = '0'");

		return $query->row['total'];
	}
}
