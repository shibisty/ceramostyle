<?php
// HTTP
define('HTTP_SERVER', 'http://ceramostyl/admin/');
define('HTTP_CATALOG', 'http://ceramostyl/');

// HTTPS
define('HTTPS_SERVER', 'http://ceramostyl/admin/');
define('HTTPS_CATALOG', 'http://ceramostyl/');

// DIR
define('DIR_APPLICATION', 'C:\OpenServer\domains\ceramostyl/admin/');
define('DIR_SYSTEM', 'C:\OpenServer\domains\ceramostyl/system/');
define('DIR_LANGUAGE', 'C:\OpenServer\domains\ceramostyl/admin/language/');
define('DIR_TEMPLATE', 'C:\OpenServer\domains\ceramostyl/admin/view/template/');
define('DIR_CONFIG', 'C:\OpenServer\domains\ceramostyl/system/config/');
define('DIR_IMAGE', 'C:\OpenServer\domains\ceramostyl/image/');
define('DIR_CACHE', 'C:\OpenServer\domains\ceramostyl/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:\OpenServer\domains\ceramostyl/system/storage/download/');
define('DIR_LOGS', 'C:\OpenServer\domains\ceramostyl/system/storage/logs/');
define('DIR_MODIFICATION', 'C:\OpenServer\domains\ceramostyl/system/storage/modification/');
define('DIR_UPLOAD', 'C:\OpenServer\domains\ceramostyl/system/storage/upload/');
define('DIR_CATALOG', 'C:\OpenServer\domains\ceramostyl/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'ceramostyl');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
