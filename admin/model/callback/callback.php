<?php
class ModelCallbackCallback  extends Model {
	public function editInformation($callback_id, $data) {
		$this->event->trigger('pre.admin.callback.edit', $data);

		$this->db->query(
			"UPDATE " . DB_PREFIX . "callback
			SET status = '" . (int)$data['status'] . "' WHERE callback_id = '". (int)$callback_id ."'"
		);

		$this->cache->delete('callback');
		$this->event->trigger('post.admin.callback.edit', $articles_id);
	}

	public function deleteInformation($callback_id) {
		$this->event->trigger('pre.admin.articles.delete', $callback_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "callback WHERE callback_id = '" . (int)$callback_id . "'");

		$this->cache->delete('callback');

		$this->event->trigger('post.admin.articles.delete', $callback_id);
	}

	public function getInformation($callback_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "callback WHERE callback_id = '" . (int)$callback_id . "'");

		return $query->row;
	}

	public function getInformations($data = array()) {
		$sql = "SELECT * FROM ".DB_PREFIX."callback ORDER BY callback_id DESC";
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalInformations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "callback");
		return $query->row['total'];
	}

	public function getNewTotalInformations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "callback WHERE status = 0");
		return $query->row['total'];
	}
}
