<?php
class ModelMainmenuFootermenu  extends Model {
	public function addInformation($data) {
		$this->event->trigger('pre.admin.footermenu.add', $data);

		$this->db->query(
			"INSERT INTO " . DB_PREFIX . "footermenu
			SET sort_order = '" . (int)$data['sort_order'] . "',
			parent_id = '" . (int)$data['parent_id'] . "',
			status = '" . (int)$data['status'] . "'"
		);

		$menu_id = $this->db->getLastId();

		foreach ($data['articles_description'] as $language_id => $value) {
			$this->db->query(
				"INSERT INTO " . DB_PREFIX . "footermenu_description
				SET menu_id = '". (int)$menu_id ."',
				name = '" . $this->db->escape($value['name']) . "',
				link = '" . $this->db->escape($value['link']) . "',
				language_id = '" . (int)$language_id . "'"
			);
		}

		$this->cache->delete('footermenu');

		$this->event->trigger('post.admin.footermenu.add', $menu_id);

		return $menu_id;
	}

	public function editInformation($menu_id, $data) {
		$this->event->trigger('pre.admin.footermenu.edit', $data);

		$this->db->query(
			"UPDATE " . DB_PREFIX . "footermenu
			SET sort_order = '" . (int)$data['sort_order'] . "',
			parent_id = '" . (int)$data['parent_id'] . "',
			status = '" . (int)$data['status'] . "' WHERE menu_id = '". (int)$menu_id ."'"
		);

		foreach ($data['articles_description'] as $language_id => $value) {
			$this->db->query(
				"UPDATE " . DB_PREFIX . "footermenu_description
				SET name = '" . $this->db->escape($value['name']) . "',
				link = '" . $this->db->escape($value['link']) . "'
				WHERE language_id = '" . (int)$language_id . "' AND menu_id = '". (int)$menu_id ."'"
			);
		}

		$this->cache->delete('articles');
		$this->event->trigger('post.admin.footermenu.edit', $articles_id);
	}

	public function deleteInformation($menu_id) {
		$this->event->trigger('pre.admin.articles.delete', $menu_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "footermenu WHERE menu_id = '" . (int)$menu_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "footermenu_description WHERE menu_id = '" . (int)$menu_id . "'");

		$this->cache->delete('articles');

		$this->event->trigger('post.admin.articles.delete', $menu_id);
	}

	public function getInformation($menu_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "footermenu WHERE menu_id = '" . (int)$menu_id . "'");

		return $query->row;
	}

	public function getInformationDescriptions($menu_id) {
		$articles_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "footermenu_description WHERE menu_id = '" . (int)$menu_id . "'");

		foreach ($query->rows as $result) {
			$articles_description_data[$result['language_id']] = array(
				'name'      => $result['name'],
				'link'      => $result['link'],
			);
		}

		return $articles_description_data;
	}

	public function getInformations($data = array()) {
		$sql = "SELECT * FROM ".DB_PREFIX."footermenu
		LEFT JOIN ".DB_PREFIX."footermenu_description
		ON ".DB_PREFIX."footermenu.menu_id = ".DB_PREFIX."footermenu_description.menu_id
		WHERE ".DB_PREFIX."footermenu_description.language_id = '{$this->config->get('config_language_id')}'
		GROUP BY ".DB_PREFIX."footermenu.menu_id
		ORDER BY ".DB_PREFIX."footermenu.sort_order
		";
		$query = $this->db->query($sql);
		$query = $query->rows;
		$result = array();

		foreach ($query as $item) {
			$result[$item['parent_id']][] = $item;
		}
		//echo '<pre>';
		//print_r($result);
		return $result;
	}

	public function getParents($menu_id = 0) {
		$sql =
		"
		SELECT * FROM ".DB_PREFIX."footermenu
		LEFT JOIN ".DB_PREFIX."footermenu_description
		ON ".DB_PREFIX."footermenu.menu_id = ".DB_PREFIX."footermenu_description.menu_id
		WHERE ".DB_PREFIX."footermenu_description.language_id = '{$this->config->get('config_language_id')}'
		AND ".DB_PREFIX."footermenu.parent_id = 0
		AND ".DB_PREFIX."footermenu.menu_id != {$menu_id}
		GROUP BY ".DB_PREFIX."footermenu.menu_id
		ORDER BY ".DB_PREFIX."footermenu.sort_order
		";
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalInformations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "footermenu ORDER BY sort_order ASC");
		return $query->row['total'];
	}

	public function getChilds($parent_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "footermenu WHERE parent_id = '".(int)$parent_id."' ORDER BY sort_order ASC");
		return $query->rows;
	}
}
