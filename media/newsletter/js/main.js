window.AdvancedNewsletter = function(params){
    this.init(params);
};

AdvancedNewsletter.prototype.init = function(params){
    var self = this;
    $(document).ready(function(){
        self.container_box      = $(params.container_id);
        self.input_email        = self.container_box.find(params.input_id);
        self.button_subbmit     = self.container_box.find(params.submit_id);
        console.log(self.container_box);
        console.log(self.input_email);
        console.log(self.button_subbmit);
        self.subscribe();

        if (typeof params.display_as != 'undefined'){
            self.container_box.subscribeBetter({
                animation: "flyInUp"
            });
        }
    });
}

AdvancedNewsletter.prototype.subscribe = function(){
    var self = this;
    this.button_subbmit.bind("click", function(){
        var selfClick = $(this);

        if(!self.input_email.val()){
            alert('Please enter your email!');
            return true;
        }

        if(!( /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/.test(self.input_email.val()) )) {
            //console.log('e');
            return;
        }

        selfClick.attr('disabled', true).addClass('disable');
        $.ajax({
            type:   "POST",
            url:    "index.php?route=module/newsletter/index",
            data:   "email="+self.input_email.val(),
            dataType: 'json',
            success: function(data){
                if(data.error && data.msg != 'Thanks for submitted to get newsletter of our store.') {
                    var Message = noty({
        				text: "Не удалось оформить подписку",
        				type: "error",
        				layout: "center",
        				timeout:1500,
        				animation: {
        					open: {
        						height: 'toggle'
        					},
        					close: {
        						height: 'toggle'
        					},
        					easing: 'swing',
        					speed: 500
        				}
        			});
                } else {
                    var Message = noty({
        				text: "Подписка оформлена",
        				type: "success",
        				layout: "center",
        				timeout:1500,
        				animation: {
        					open: {
        						height: 'toggle'
        					},
        					close: {
        						height: 'toggle'
        					},
        					easing: 'swing',
        					speed: 500
        				}
        			});
                }

                setTimeout(function(){
                    document.location.reload();
                },1000);
                selfClick.attr('disabled', false).removeClass('disable');
            }
        });
    });
}
