
				<?php
				class ControllerCommonHome extends Controller {
					public function index() {
						$this->document->setTitle($this->config->get('config_meta_title'));
						$this->document->setDescription($this->config->get('config_meta_description'));
						$this->document->setKeywords($this->config->get('config_meta_keyword'));

						if (isset($this->request->get['route'])) {
							$this->document->addLink(HTTP_SERVER, 'canonical');
						}

						$data['column_left'] = $this->load->controller('common/column_left');
						$data['column_right'] = $this->load->controller('common/column_right');
						$data['content_top'] = $this->load->controller('common/content_top');
						$data['content_bottom'] = $this->load->controller('common/content_bottom');
						$data['footer'] = $this->load->controller('common/footer');
						$data['header'] = $this->load->controller('common/header');

						$this->load->model('design/layout');
						$modules = $this->model_design_layout->getLayoutModules(1, 'content_top');

						$data['slideshow'] = '';
						$data['featured'] = '';

						foreach ($modules as $module) {
							// $part = explode('.', $module['code']);
							//
							// if (isset($part[0]) && $this->config->get($part[0] . '_status')) {
							// 	$data['modules'][] = $this->load->controller('module/' . $part[0]);
							// }
							//
							// if (isset($part[1])) {
							// 	$setting_info = $this->model_extension_module->getModule($part[1]);
							//
							// 	if ($setting_info && $setting_info['status']) {
							// 		$data['modules'][] = $this->load->controller('module/' . $part[0], $setting_info);
							// 	}
							// }
							$part = explode('.', $module['code']);
							if (isset($part[1])) {
								$setting_info = $this->model_extension_module->getModule($part[1]);

								if ($part[0] === "slideshow" && $setting_info && $setting_info['status']) {
									$data['slideshow'] = $this->load->controller('module/slideshow', $setting_info);
								}

								if ($part[0] === "featured" && $setting_info && $setting_info['status']) {
									$data['featured'] = $this->load->controller('module/featured', $setting_info);
								}
							}
						}


						$data['banner1'] = $this->load->controller('module/banner1');
						$data['manufacturer'] = $this->load->controller('module/manufacturer');
						$data['new'] = $this->load->controller('module/new');
						$data['plitka'] = $this->load->controller('module/plitka');
						$data['plitka2'] = $this->load->controller('module/plitka2');
						$data['review'] = $this->load->controller('module/review');
						$data['top'] = $this->load->controller('module/top');

						if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
							$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
						} else {
							$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
						}
					}
				}
			