
				<?php
				class ControllerCommonContentTop extends Controller {
					public function index() {
						$this->load->model('design/layout');

						if (isset($this->request->get['route'])) {
							$route = (string)$this->request->get['route'];
						} else {
							$route = 'common/home';
						}

						$layout_id = 0;

						if ($route == 'product/category' && isset($this->request->get['path'])) {
							$this->load->model('catalog/category');

							$path = explode('_', (string)$this->request->get['path']);

							$layout_id = $this->model_catalog_category->getCategoryLayoutId(end($path));
						}

						if ($route == 'product/product' && isset($this->request->get['product_id'])) {
							$this->load->model('catalog/product');

							$layout_id = $this->model_catalog_product->getProductLayoutId($this->request->get['product_id']);
						}

						if ($route == 'information/information' && isset($this->request->get['information_id'])) {
							$this->load->model('catalog/information');

							$layout_id = $this->model_catalog_information->getInformationLayoutId($this->request->get['information_id']);
						}

						if (!$layout_id) {
							$layout_id = $this->model_design_layout->getLayout($route);
						}

						if (!$layout_id) {
							$layout_id = $this->config->get('config_layout_id');
						}

						$this->load->model('extension/module');

						$data['modules'] = array();

						$modules = $this->model_design_layout->getLayoutModules($layout_id, 'content_top');
						//die($layout_id);
						//$data['slideshow'] = '';
						//$data['featured'] = '';

						foreach ($modules as $module) {
							$part = explode('.', $module['code']);

							if (isset($part[0]) && $this->config->get($part[0] . '_status')) {
								$data['modules'][] = $this->load->controller('module/' . $part[0]);
							}

							if (isset($part[1])) {
								$setting_info = $this->model_extension_module->getModule($part[1]);

								if ($setting_info && $setting_info['status']) {
									$data['modules'][] = $this->load->controller('module/' . $part[0], $setting_info);
								}
							}
							// $part = explode('.', $module['code']);
							// if (isset($part[1])) {
							// 	$setting_info = $this->model_extension_module->getModule($part[1]);
							//
							// 	if ($part[0] === "slideshow" && $setting_info && $setting_info['status']) {
							// 		$data['slideshow'] = $this->load->controller('module/slideshow', $setting_info);
							// 	}
							//
							// 	if ($part[0] === "featured" && $setting_info && $setting_info['status']) {
							// 		$data['featured'] = $this->load->controller('module/featured', $setting_info);
							// 	}
							// }
						}


						// $data['banner1'] = $this->load->controller('module/banner1');
						// $data['manufacturer'] = $this->load->controller('module/manufacturer');
						// $data['new'] = $this->load->controller('module/new');
						// $data['plitka'] = $this->load->controller('module/plitka');
						// $data['plitka2'] = $this->load->controller('module/plitka2');
						// $data['review'] = $this->load->controller('module/review');
						// $data['top'] = $this->load->controller('module/top');

						if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/content_top.tpl')) {
							return $this->load->view($this->config->get('config_template') . '/template/common/content_top.tpl', $data);
						} else {
							return $this->load->view('default/template/common/content_top.tpl', $data);
						}
					}
				}
			